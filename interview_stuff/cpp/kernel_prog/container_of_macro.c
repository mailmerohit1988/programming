/*
http://codinghighway.com/2013/08/10/the-magical-container-of-macro-and-its-use-in-the-linux-kernel/
*/

#include <stdio.h>
#include <stdlib.h>

#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)

#define container_of(ptr, type, member) ({ \
const typeof( ((type *)0)->member ) *__mptr = (ptr); \
(type *)( (char *)__mptr - offsetof(type,member) );})

struct list_head {
        struct list_head *next;
        struct list_head *prev;
};

struct small_data {
        int size;
        struct list_head list;
};

struct temp {
        int temp1;
};

int main()
{
        struct temp obj = {
                .temp1 = 1,
        }; 

        printf("%d\n", obj.temp1);

        struct small_data sd1;
        sd1.size = 2;
        
        struct small_data sd2;
        sd2.size = 3;

        struct small_data sd3;
        sd3.size = 4;

        printf("Address of sd1.list %x\n", &sd1.list);
        printf("Address of sd2.list %x\n", &sd2.list);
        printf("Address of sd3.list %x\n", &sd3.list);

        sd1.list.prev = NULL;
        sd1.list.next = &sd2.list;

        sd2.list.prev = &sd1.list;
        sd2.list.next = &sd3.list;

        sd3.list.prev = &sd2.list;
        sd3.list.next = NULL;

        printf("%x\n", &sd1); 
        printf("%x\n", &sd1.size); 
        printf("%x\n", &sd1.list);
        printf("%d\n", &((struct small_data*)0)->list);
        printf("%x\n", &sd1.list);

        struct list_head *obj1 = &sd1.list;
        struct small_data *ptrsd1;
        ptrsd1 = container_of(obj1, struct small_data, list);

        printf("size of sd1 %d\n", ptrsd1->size);
        return 0;
} 

/*#define SD_DATA(sd) \
(void *)((char *)sd + sizeof(*sd) + (sd->name_size-sizeof(sd->name)))*/
