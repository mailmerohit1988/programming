//: C09:MacroSideEffects.cpp 
#include "../require.h" 
#include <fstream> 
#include <iostream> 
using namespace std; 

#define BAND(x) (((x)>5 && (x)<10) ? (x) : 0) 

#define BAND1(x) (((x)>5 && (x)<10) ? (x) : 0) 

int main() { 
	ofstream out("macro.out"); // writes output to a file macro.out
	assure(out, "macro.out"); 
	for(int i = 4; i < 11; i++) { 
		int a = i; 
		out << "a = " << a << endl << '\t'; 
		out << "BAND(++a)=" << BAND(++a) << endl; 
		out << "\t a = " << a << endl; 
	} 
	
	for(int i = 4; i < 11; i++) { 
		int a = i; 
		cout << "a = " << a << endl << '\t'; 
		cout << "BAND(++a)=" << BAND(++a) << endl; 
		cout << "\t a = " << a << endl; 
	} 
	cout << "\n\n***\n\n";
	for(int i = 4; i < 11; i++) { 
		int a = i; 
		cout << "a = " << a << endl << '\t'; 
		cout << "BAND1(a)=" << BAND1(a) << endl; 
		a++;
		cout << "\t a = " << a << endl ; 
	} 
	
} ///:~ 
