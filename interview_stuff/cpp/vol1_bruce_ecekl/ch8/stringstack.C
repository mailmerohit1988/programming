// compile-time constant inside a class 
#include <cstring> 
#include <iostream> 
using namespace std; 

class StringStack { 
	static const int size = 100; 
	const string* stack[size]; 
	int index; 
	public: 
		StringStack(); 
		void push(const string* s); 
		const string* pop(); 
}; 

StringStack::StringStack() : index(0) { 
	cout << "Contructor called " << sizeof(string*) << endl;
	memset(stack, 0, size * sizeof(string*)); 
} 

void StringStack::push(const string* s) { 
	if(index < size) 
		stack[index++] = s; 
} 

const string* StringStack::pop() { 
	if(index > 0) { 
		const string* rv = stack[--index]; 
		stack[index] = 0; 
		return rv; 
	} 
	return 0; 
} 

string iceCream[] = { 
"pralines & cream", 
"fudge ripple", 
"jamocha almond fudge", 
"wild mountain blackberry", 
"raspberry sorbet", 
"lemon swirl", 
"rocky road", 
"deep chocolate fudge" 
}; 

const int iCsz = sizeof iceCream / sizeof *iceCream; // or const int iCsz = sizeof iceCream / sizeof iceCream[0]; 

int main() { 
	StringStack ss; 
	for(int i = 0; i < iCsz; i++) 
		ss.push(&iceCream[i]); 
	const string* cp; 
	while((cp = ss.pop()) != 0) 
		cout << *cp << endl; 
	
	iceCream[0]="my custom value";
	cout << "Thats it" << iceCream[0] << endl; 
	const char* s2="hell";
	cout << s2 << endl; // Will print hell
	const string* s1=new string("hello");
	cout << s1<< endl; // Will print the address
} ///:~
