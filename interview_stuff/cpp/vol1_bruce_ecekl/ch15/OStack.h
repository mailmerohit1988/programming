//: C15:OStack.h 
// Using a singly-rooted hierarchy 
#ifndef OSTACK_H 
#define OSTACK_H 

class Object { 
	public: 
		virtual ~Object() = 0; 
}; 

// Required definition: 
inline Object::~Object() {} 

class Stack { 
	struct Link { 
		Object* data; 
		Link* next; // actually this next is its previous
		Link(Object* dat, Link* nxt) : data(dat), next(nxt) {} 
	}* head; 

	public: 
		Stack() : head(0) {} 
		
		~Stack(){ 
			while(head) 
				delete pop(); // will use late binding because of virtual function call mechanism and this call will call ~MyString() 
		} 
		
		void push(Object* dat) { 
			head = new Link(dat, head); // head has its size embedded into it so when we call delete head, compiler automatically determines the size of head
		} 
		
		Object* peek() const { 
			return head ? head->data : 0; 
		} 
		
		Object* pop() { 
			if(head == 0) return 0; 
			Object* result = head->data; 
			Link* oldHead = head; 
			head = head->next; 
			delete oldHead; 
			return result; 
		} 
}; 
#endif // OSTACK_H ///:~
