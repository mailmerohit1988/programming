//: C11:ReferenceToPointer.cpp 
#include <iostream> 
using namespace std; 

void increment(int*& i) { i++; } 

int main() { 
	int* i = 0; 
	cout << "i = " << i << endl; 
	increment(i); 
	cout << "i = " << i << endl; 
	int j=10;
	int* k = &j;
	cout << "k = " << k << endl; 
	increment(k); 
	cout << "k = " << k << endl; 
} ///:~ 

/*
Address of dynamic variables are not known for the expected reason, as they are allocated dynamically from memory pool.
Address of local variables are not known, because they reside on "stack" memory region. Stack winding-unwinding of a program may defer based on runtime conditions of the code flow.

http://www.tenouk.com/Bufferoverflowc/Bufferoverflow2a.html
*/

/*
 * 
 * 
 * 
 * So far, so good. There’s a workable process for passing and 
returning large simple structures. But notice that all you have is a 
way to copy the bits from one place to another, which certainly 
works fine for the primitive way that C looks at variables. But in 
C++ objects can be much more sophisticated than a patch of bits; 
they have meaning. This meaning may not respond well to having 
its bits copied.
*/
