//: C13:NewAndDelete.cpp 
// Simple demo of new & delete 
#include "tree.h" 

using namespace std; 

int main() { 
	Tree* t = new Tree(40); 
	cout << t; 
	delete t; 
	operator<<(cout,t);// global function and not a class member function
	
} ///:~ 
