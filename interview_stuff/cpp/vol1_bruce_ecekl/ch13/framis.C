//: C13:Framis.cpp 
// Local overloaded new & delete 
#include <cstddef> // Size_t 
#include <fstream> 
#include <iostream> 
#include <new> 
using namespace std; 

ofstream out("Framis.out"); 

class Framis { 
	enum { sz = 10 }; // to have constants inside classes
	char c[sz]; // To take up space, not used 
	static unsigned char pool[]; 
	static bool alloc_map[]; 
	public: 
		enum { psize = 100 }; // frami allowed 
		Framis() { out << "Framis()\n"; }
		~Framis() { out << "~Framis() ... "; } 
		void* operator new(size_t) throw(bad_alloc); 
		void operator delete(void*); 
}; 

unsigned char Framis::pool[psize * sizeof(Framis)]; 

bool Framis::alloc_map[psize] = {false}; 

// Size is ignored -- assume a Framis object 
void* Framis::operator new(size_t) throw(bad_alloc) { // new will actually allocate memory
	out << "inside new , size of framis" << sizeof(Framis) << endl; //sizeof(Framis) = 10, because of 10 characters c[10]
	for(int i = 0; i < psize; i++) 
		if(!alloc_map[i]) { 
			out << "using block " << i << " ... "; 
			alloc_map[i] = true; // Mark it used 
			return pool + (i * sizeof(Framis)); // returns void*
		} 
	out << "out of memory" << endl; 
	throw bad_alloc(); 
} 

void Framis::operator delete(void* m) { 
	if(!m) {out << "m is null" << m << endl;return;} // Check for null pointer 
	// Assume it was created in the pool 
	// Calculate which block number it is: 
	unsigned long block = (unsigned long)m - (unsigned long)pool; 
	block /= sizeof(Framis);
	out << "freeing block " << block << endl;
	// Mark it free: 
	alloc_map[block] = false; 
	//cout << "exiting delete" << m << endl;
} 

int main() { 
	Framis* f[Framis::psize]; 
	try { 
		for(int i = 0; i < Framis::psize; i++) 
			f[i] = new Framis; 
		new Framis; // Out of memory 
	} catch(bad_alloc) { 
		cerr << "Out of memory and in catch block!" << endl; 
	} 

	delete f[10];
	f[10] = 0; 
	// Use released memory: 
	Framis* x = new Framis; 
	delete x; //does not make x=0
	for(int j = 0; j < Framis::psize; j++){
		if(j == 10){
			out << "deleting object for 10th object"<<endl;
		}
		delete f[j]; // f[10] has already been made zero in line 64, so this is like delete 0;
		if(j == 10){
			out << "deleted 10th object"<<endl;
		}
	}
	
	Framis* g=new Framis[10]; // 10 cons calls will be made but global new will be called and not our overloaded new
	delete []g; // 10 destructor calls are made
} ///:~
	
