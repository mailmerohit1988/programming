//: C13:Tree.h 
#ifndef TREE_H 
#define TREE_H 
#include <iostream> 

class Tree { 
	int height; 
	
	friend std::ostream& operator<<(std::ostream& os, const Tree* t) { //cout is an object and is collected by reference here
			return os << "Tree height is: " << t->height << std::endl; 
	} 
	// FRIEND FUNCTIONS PRIVATE OR PUBLIC DOESNT MAKE ANY DIFFERENCE
	public: 
		Tree(int treeHeight) : height(treeHeight) {} 
	
		~Tree() { std::cout << "*" << std::endl; } 
	
		
}; 

#endif // TREE_H ///:~ 

