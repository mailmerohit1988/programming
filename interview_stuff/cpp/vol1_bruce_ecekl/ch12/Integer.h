//: C12:Integer.h 
// Non-member overloaded operators 
#ifndef INTEGER_H 
#define INTEGER_H 
#include <iostream> 

// Non-member functions:
class Integer { 
	long i; 
	public: 
		Integer(long ll = 0) : i(ll) {} 
		// Operators that create new, modified value: 
		friend const Integer operator+(const Integer& left, const Integer& right); 
		friend const Integer operator-(const Integer& left, const Integer& right); 
		friend const Integer operator*(const Integer& left, const Integer& right); 
		friend const Integer operator/(const Integer& left, const Integer& right); 
		friend const Integer operator%(const Integer& left, const Integer& right); 
		friend const Integer operator^(const Integer& left, 
};

void h(Integer& c1, Integer& c2) { 
	// A complex expression: 
	c1 += c1 * c2 + c2 % c1; 
	#define TRY(OP) \ 
	out << "c1 = "; c1.print(out); \ 
	out << ", c2 = "; c2.print(out); \ 
	out << "; c1 " #OP " c2 produces "; \ 
	(c1 OP c2).print(out); \ 
	out << endl; 
	TRY(+) TRY(-) TRY(*) TRY(/) 
	TRY(%) TRY(^) TRY(&) TRY(|) 
	TRY(<<) TRY(>>) TRY(+=) TRY(-=) 
	TRY(*=) TRY(/=) TRY(%=) TRY(^=) 
	TRY(&=) TRY(|=) TRY(>>=) TRY(<<=) 
	// Conditionals: 
	#define TRYC(OP) \ 
	out << "c1 = "; c1.print(out); \ 
	out << ", c2 = "; c2.print(out); \ 
	out << "; c1 " #OP " c2 produces "; \ 
	out << (c1 OP c2); \ 
	out << endl; 
	TRYC(<) TRYC(>) TRYC(==) TRYC(!=) TRYC(<=) 
	TRYC(>=) TRYC(&&) TRYC(||) 
} 
Byte& operator=(const Byte& right) { 
	// Handle self-assignment: 
	if(this == &right) return *this; 
	b = right.b; 
	return *this; 
}




Byte& operator+=(const Byte& right) { 
	if(this == &right) {/* self-assignment */} 
	b += right.b; 
	return *this; 
}


int main() { 
	cout << "friend functions" << endl; 
	Integer c1(47), c2(9); 
	h(c1, c2); 
} ///:~ 

//: C12:Byte.h 
// Member overloaded operators 
#ifndef BYTE_H 
#define BYTE_H 
