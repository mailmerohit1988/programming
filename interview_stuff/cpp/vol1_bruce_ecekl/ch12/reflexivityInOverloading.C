//: C12:ReflexivityInOverloading.cpp 
#include <iostream>
using namespace std;

class Number { 
	int i; 
	public: 
		Number(int ii = 0) : i(ii) {cout << "NUMBER cons() called" << endl;} 
		
		const Number operator+(const Number& n) const { 
			cout << i << n.i << endl;
			return Number(i + n.i); 
		} 
		
		friend const Number operator-(const Number&, const Number&); // not called with objects ,  global overloaded operators
}; 

const Number operator-(const Number& n1, const Number& n2) { 
		return Number(n1.i - n2.i); 
} 

int main() { 
	Number a(47), b(11); 
	a + b; // OK // a.operator+(b); 
	cout << "******" << endl;
	a + 1; // 2nd arg converted to Number //a.operator+(1); // CALLS Number(int ii = 0) : i(ii) {}  BECAUSE WE ARE PASSING INT AND IT ACCEPTS A NUMBER THIS IS CALLED AS CONSTRUCTOR CONVERSION
	cout << "******" << endl;
	//! 1 + a; // Wrong! 1st arg not of type Number  
	a - b; // OK 
	a - 1; // 2nd arg converted to Number 
	1 - a; // 1st arg converted to Number 
} ///:~
