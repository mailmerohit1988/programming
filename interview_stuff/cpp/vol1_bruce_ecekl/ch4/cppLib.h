//: C04:CppLib.h 
// C-like library converted to C++ 

#include "cLib.h"  //Lib.h:11:3: error: ‘CStash’ has a previous declaration as ‘typedef struct CStashTag CStash’

#ifndef STASH
#define STASH
struct Stash { 
	int size; // Size of each space 
	int quantity; // Number of storage spaces 
	int next; // Next empty space 
	// Dynamically allocated array of bytes: 
	unsigned char* storage; 
	// Functions! 
	void initialize(int size); 
	void cleanup(); 
	int add(const void* element); 
	void* fetch(int index); 
	int count(); 
	void inflate(int increase); 
}; ///:~ 
#endif //STASH

