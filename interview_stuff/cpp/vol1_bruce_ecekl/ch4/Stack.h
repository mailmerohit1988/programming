//: C04:Stack.h 
// Nested struct in linked list 
#ifndef STACK_H 
#define STACK_H 

struct Stack { 
	struct Link { 
		void* data; // STACK is holding void data
		Link* next; // This next actually means the previous Link
		void initialize(void* dat, Link* nxt); 
	}* head; 
	void initialize(); 
	void push(void* dat); 
	void* peek(); 
	void* pop(); 
	void cleanup(); 
}; 

#endif // STACK_H ///:~ 
