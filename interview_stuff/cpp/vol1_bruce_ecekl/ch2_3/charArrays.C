//: C03:CommandLineArgs.cpp 
#include <iostream> 
using namespace std; 

int main(int argc, char* argv[]) { 
	cout << "argc = " << argc << endl; 
	for(int i = 0; i < argc; i++) 
		cout << "argv[" << i << "] = " 
		<< argv[i] << endl; 
	
	char c = 's';
	//cout << static_cast<void *>(&c)<<endl;
	char str[1] ;
	str[0]= c;

	char* arr[2]; // it says that its individual element will be char* types i.e char array addresses
	char c1[] = "abc";
	char c2[] = "def";
	arr[0]=c1;
	arr[1]=c2;
	cout << arr[0] << endl; //cout sees char* and prints out the string
	cout << arr[1] << endl;
	

} ///:~ 

// abc def ghi 
// These are char arrays and argv[] stores pointers to these char arrays
