//: C16:ValueStack.h 
// Holding objects by value in a Stack 
#ifndef VALUESTACK_H 
#define VALUESTACK_H 
#include "../require.h" 
#include <iostream> 
using namespace std; 

template<class T, int ssize = 100> 
class Stack { 
	
	// Default constructor performs object 
	// initialization for each element in array: 
	T stack[ssize]; 
	int top; 

	public: 
		Stack() : top(0) {} 
	
		// Copy-constructor copies object into array: 
		void push(const T& x) { 
			cout << "Entering push() " << endl;
			require(top < ssize, "Too many push()es"); 
			stack[top++] = x; 
			cout << "Exiting push() " << endl;
		} 
		
		T peek() const { return stack[top]; } //being passed by value so CC will be called
		
		// Object still exists when you pop it; 
		// it just isn't available anymore: 
		T pop() { 
			require(top > 0, "Too many pop()s"); 
			return stack[--top]; //being passed by value so CC will be called
		} 
}; 
#endif // VALUESTACK_H ///:~
