//: C06:Stack3.h 
// With constructors/destructors 
#ifndef STACK3_H 
#define STACK3_H 

class Stack { 
	struct Link { 
		void* data; 
		Link* next; // This next is actually the previous or the last link*
		Link(void* dat, Link* nxt); 
		~Link(); 
	}* head; 
	public: 
		Stack(); 
		~Stack(); 
		void push(void* dat); 
		void* peek(); 
		void* pop(); 
}; 
#endif // STACK3_H ///:~ 

