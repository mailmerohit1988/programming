// string::find_last_of
#include <iostream>       // std::cout
#include <string>         // std::string

void SplitFilename (const std::string& str)
{
  std::cout << "Splitting: " << str << '\n';
  unsigned found = str.find_last_of("/\\");
  std::cout << found << std::endl;
  std::cout << " path: " << str.substr(0,found) << '\n';
  std::cout << " file: " << str.substr(found+1,std::string::npos) << '\n';
}

int main ()
{
  std::string str1 ("/usr/bin/man");
  std::string str2 ("c:\\windows\\winhelp.exe");

  SplitFilename (str1);
  SplitFilename (str2);

  std::cout << str1 << str2 << std::endl;
  
  return 0;
}
