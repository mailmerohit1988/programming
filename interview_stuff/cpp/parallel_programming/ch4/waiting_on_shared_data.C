#include <iostream>
#include <thread>
#include <mutex>
using namespace std;

int global_data;
std::mutex m1;

void some_func(int i) {
        cout << "inside some_func" << i << endl;
        while (true) {
                std::unique_lock<std::mutex> lk1(m1);
                cout << "Waiting for  global_data" << global_data << endl;
                if (global_data == 40) {
                        lk1.unlock();
                        break;
                }
                lk1.unlock();
        }
        if (global_data == 40) {
                cout << "Got global_data" << global_data << endl;
        }
}

void some_other_func(int i) {
        cout << "inside some_other func" << i << endl;
        for (int j = 0; j< 200; j++) {
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                if (j == 100) {
                        std::unique_lock<std::mutex> lk2(m1);
                        cout << "setting up global_data"  << endl;
                        global_data = 40;
                        lk2.unlock();
                }
                if (j == 150) {
                        cout << "j set to 150" << endl;
                }
        }
}

int  main() 
{
        cout << "Inside main starting two threads" << endl;
        std::thread t1(some_func, 10);
        std::thread t2(some_other_func, 20);        
        t1.join();
        t2.join();

        return 0;
}
