//auto_ptr : Transfer of Ownership

#include <iostream>
#include <memory>
using namespace std;

template <class T>
ostream& operator << (ostream& ostrm, const auto_ptr<T>& ap) {
 if(ap.get()==NULL) /*Returns NULL if auto_ptr does not own an object, else returns the address of object owned */
  ostrm << "NULL";
 else   ostrm << *ap;
 return ostrm;
}

int main() {
 auto_ptr<int> apio(new int(10));
 auto_ptr<int> apit;
 cout << apio << " " << apit << endl; //10 NULL
 apit=apio; // Ownership transferred to apit
 cout << apio << " " << apit << endl; // NULL 10
 *apit *=5;  apio=apit; //Ownership transferred again
 cout << apio << " " << apit << endl; // 50 NULL
 return 0;
}

