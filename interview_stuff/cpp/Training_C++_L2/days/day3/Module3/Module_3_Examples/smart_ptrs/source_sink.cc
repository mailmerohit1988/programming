#include<iostream>
#include<memory>
using namespace std;

auto_ptr<int> Source(int value){
  return auto_ptr<int>(new int(value));
}

void Sink(auto_ptr<int> api) {;}

int main() {
  auto_ptr<int> api;
  api=Source(100);

  cout<<*api<<endl;//100
  Sink(api);//ownership transferred, api set to NULL

  cout<<*api<<endl; //error

}


