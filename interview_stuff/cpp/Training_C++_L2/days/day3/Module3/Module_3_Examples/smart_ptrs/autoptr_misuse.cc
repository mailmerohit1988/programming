auto_ptr<int> apio(new int (50));
auto_ptr<int> apit(apio.get()); /*apit and apio refer to same object! Dangling !! */

Better use this approach:
 auto_ptr<int> apit(apio.release()); /*Releases the ownership of object apio refers to, sets apio to NULL and returns the address of object apio was referring to earlier (before release) */

Sometimes, we can use reset() to reinitialize the auto_ptr
auto_ptr<int> apio(new int (50));
apio.reset(new int (100)); /*deletes the object ato_ptr refers to and then reinitializes the auto_ptr */

Another Example:
void test() {
  int *pi=new int(50);
  auto_ptr<int> apio(pi); // initialize auto_ptr
  *apio=100;
  int* pit = apio.release();
  delete pit; //since no other auto_ptr owns this object
}

