//const auto_ptr 

#include <iostream>
#include <memory>
using namespace std;

void ownershiptest(auto_ptr<int> api) {
 if(api.get()==NULL) 
  cout << "NULL";
 else
  cout << *api;
}

int main() {
 auto_ptr<int> apio(new int);
 *apio=50;
 ownershiptest(apio); // 50
 //cout << *apio; //Segmentation fault
 ownershiptest(apio); //NULL

const auto_ptr<int> capio(new int); *capio=50;
 //ownershiptest(capio); // Compile Error
 cout << *capio; // 50
 return 0;
}

