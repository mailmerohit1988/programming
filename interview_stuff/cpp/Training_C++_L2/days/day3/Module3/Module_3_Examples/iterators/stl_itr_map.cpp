//STL: using iterator to access map

//Using iterator on map
#include <iostream>
#include <string>
#include <map>
using namespace std;

int main(){
  map <string,string> gk;
  gk.insert( make_pair("Apple","Fruit"));
  gk.insert( make_pair("Potato","Vegetable"));
  gk.insert( make_pair("Orange","Fruit"));
  gk.insert( make_pair("Tomato","Vegetable"));
  gk.insert( make_pair("Potato","Fruit"));

  map<string,string>::iterator it;

  for (it=gk.begin(); it != gk.end(); ++it)
    cout << it->first <<" is a "<< it->second<< endl;
  return 0;
}

