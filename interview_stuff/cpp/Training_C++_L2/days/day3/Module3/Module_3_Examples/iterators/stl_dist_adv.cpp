//STL: Iterator : distance() and advance()

#include <iostream>
#include <list>
using namespace std;

int main() {
   list<int> num;
   for(int i=1; i<=5; ++i)
	   num.push_back(i*i); // 1 4 9 16 25

   list<int>::iterator it = num.begin(); // Points to beginning
   cout << *it << endl; // 1
   advance(it, 4); // Advance by 4 positions
   cout << *it << endl; //25
   advance(it, -1); // Step one element backward
   cout << *it << endl; // 16
   cout <<"Distance between begin and current: ";
   cout << distance(num.begin(), it); // 3

   return 0;
}

