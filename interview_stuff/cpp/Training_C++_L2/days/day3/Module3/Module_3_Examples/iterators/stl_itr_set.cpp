//STL: using iterator to access set

//Using iterator on set
#include <iostream>
#include <set>
using namespace std;

int main(){
  set<char> names;
  // no push_back for associative containers
  names.insert('T');
  names.insert('I');
  names.insert('T');
  names.insert('A');
  names.insert('N');

  set<char>::const_iterator it;
  for (it = names.begin(); it != names.end(); ++it)
    cout << *it; // AINT
  return 0;
}

