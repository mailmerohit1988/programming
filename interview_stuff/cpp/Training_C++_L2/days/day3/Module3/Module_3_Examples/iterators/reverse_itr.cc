#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
void fnPrint(int iVal) {  cout << iVal << ' '; }
int main(){
 vector<int> oNum;
 for(int iLoop=1; iLoop<=6; ++iLoop) oNum.push_back(iLoop);
 vector<int>::iterator it = find(oNum.begin(),oNum.end(),4);
 cout << *it << endl; // 4 
 vector<int>::reverse_iterator rit(it);
 cout << *rit << endl; // 3
 vector<int>::iterator ito=find(oNum.begin(), oNum.end(), 2);
 vector<int>::iterator itt=find(oNum.begin(), oNum.end(), 5);
 for_each(ito, itt, fnPrint); cout << endl; // [2,5) -> 2 3 4

 vector<int>::reverse_iterator rito(ito);
 vector<int>::reverse_iterator ritt(itt);
 for_each(ritt, rito, fnPrint); cout << endl; // [2,5) in reverse -> 4 3 2
 return 0;
}

