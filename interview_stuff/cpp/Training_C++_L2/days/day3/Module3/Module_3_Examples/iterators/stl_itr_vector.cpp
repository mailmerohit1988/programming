//STL: using iterator to access vector

//Using iterator on vector
#include <iostream>
#include <vector>
#include <cctype>
using namespace std;

int main(){
 vector<char> names;
 for(char c='a';c<='z';++c) 
      names.push_back(c);

 vector<char>::iterator it;
 for (it=names.begin(); it!=names.end(); ++it )
   cout << *it; // abc...xyz
 cout << endl;

 for (it=names.begin(); it!=names.end();++it )
   *it=toupper(*it);

 for (it =names.begin(); it!=names.end();++it )
   cout << *it; // ABC...XYZ
 return 0;
}

