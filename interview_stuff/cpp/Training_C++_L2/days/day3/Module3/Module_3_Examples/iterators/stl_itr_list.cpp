//STL: using iterator to access list

//Using iterator on list
#include <iostream>
#include <list>
using namespace std;

int main(){
 list<char> names;
 names.push_back( 'T' );
 names.push_back( 'I' );
 names.push_back( 'T' );
 names.push_back( 'A' );
 names.push_back( 'N' );

 list<char>::iterator it;
 for (it =names.begin(); it!=names.end();++it)
   cout << *it; // TITAN
 return 0;
}


