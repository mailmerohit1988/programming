//STL: iterator adapter

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void print(int val) {  cout << val << ' '; }

int main(){
 vector<int> num;
 for(int i=1; i<=6; ++i) num.push_back(i);
 vector<int>::iterator it = find(num.begin(),num.end(),4);
 cout << *it << endl; // 4 

 vector<int>::reverse_iterator rit(it);
 cout << *rit << endl; // 3
 vector<int>::iterator ito=find(num.begin(), num.end(), 2);
 vector<int>::iterator itt=find(num.begin(), num.end(), 5);
 for_each(ito, itt, print); cout << endl; // [2,5) -> 2 3 4

 vector<int>::reverse_iterator rito(ito);
 vector<int>::reverse_iterator ritt(itt);
 for_each(ritt, rito, print); cout << endl; //[2,5) in reverse -> 4 3 2
 return 0;
}

