//STL: List

#include <iostream>
#include <list>
using namespace std;

void PrintList(list<int> nums) {
 while(!nums.empty()) {
   cout << nums.front() << ' '; // display 1st element
   nums.pop_front(); // remove 1st element
  }
 cout << endl;
}

int main(){
 list<int> nums;
 for (int i=1; i<=5; ++i) nums.push_back(i*i);
 PrintList(nums); // 1 4 9 16 25
 nums.reverse(); PrintList(nums); // 25 16 9 4 1
 nums.sort(); PrintList(nums); // 1 4 9 16 25
 return 0;
 }

