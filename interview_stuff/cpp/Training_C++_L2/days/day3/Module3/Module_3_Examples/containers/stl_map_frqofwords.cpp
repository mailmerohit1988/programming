//STL: using map to count frequency of words

#include <iostream>
#include <map> 
#include <string>

using namespace std; 
 
int main() {   
 map<string,int> wordcount;
 map<string,int>::iterator it;

 string str;
 cout <<"Enter group of words (type . to end)? ";
 while (cin >> str, str!=".")
   wordcount[str]++;

 cout <<"Unique words: "<< wordcount.size() <<endl; 

 for (it = wordcount.begin(); it != wordcount.end(); ++it)
  cout << it->first << " : " << it->second << endl;

 return 0;
}

