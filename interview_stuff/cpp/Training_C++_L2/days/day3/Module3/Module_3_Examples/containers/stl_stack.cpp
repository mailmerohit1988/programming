//STL: Stack


#include <iostream>
#include <stack>
using namespace std;

int main()
{
  int aiData[] = {45, 34, 56, 27, 71, 50, 62};
  stack<int> s;
  cout << "The stack size is now " << s.size() << endl;

  cout << "Pushing 4 elements " << endl;
  for (int i = 0; i < 4; ++i)
      s.push(aiData [i]);
      
  cout << "The stack size is now " << s.size() << endl;

  cout << "Popping 3 elements " << endl;
  for (int j = 0; j < 3; ++j) {
    cout << s.top() << endl;
    s.pop();
  }
  cout << "The stack size is now " << s.size() << endl;
  
  return 0;
}

