//STL: Deque

#include <iostream>
#include <deque>
using namespace std;

int main(){
 deque<int> nums;
 for (int i=1; i<=3; ++i)  nums.push_back(i*i);
 for (int i=4; i<=6; ++i)  nums.push_front(i*i);
 cout << "1st : " << nums.front() << endl; // 36
 cout << "Last: " << nums.back() << endl; // 9
 nums.pop_back(); nums.pop_front(); // Remove 1st and last element
 for (int i=0; i<nums.size(); ++i)
  cout << nums[i] << ' '; cout << endl;
 return 0;
}

