//STL: Vector

#include<iostream>
#include <vector>
using namespace std;

int main() {
 vector<int> nums;
 for (int i=1; i<=5; ++i)
   nums.push_back(i*i); //append elements
 for (int i=0; i<nums.size(); ++i)
   cout << nums[ i ] << ' '; cout << endl;
   
 nums.erase( nums.begin() ); //remove first element
 nums.pop_back(); //remove last element
 for (int i=0; i<nums.size(); ++i)
   cout << nums[ i ] << ' '; cout << endl;
}

