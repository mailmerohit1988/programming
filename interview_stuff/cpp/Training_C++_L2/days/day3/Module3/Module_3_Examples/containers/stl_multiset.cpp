//STL: multiset

#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>
using namespace std;

int main()
{
    multiset<int> ms;

    // insert elements in random order
    ms.insert(4);
    ms.insert(3);
    ms.insert(5);
    ms.insert(1);
    ms.insert(6);
    ms.insert(2);
    // inserting duplicate entry with value 5
    ms.insert(5);

    // iterate over all elements and print them
    multiset<int>::iterator pos;
    for (pos = ms.begin(); pos != ms.end(); ++pos) {
        cout << *pos << ' ';
    }
    cout << endl;

}

