//STL: set

#include <iostream>
#include <set>
#include <string>

using namespace std;

int main( ) {

   set<string> setStr;
   string sValue = "B";

   setStr.insert(sValue);
   sValue = "S";
   setStr.insert(sValue);
   sValue = "R";
   setStr.insert(sValue);
   // Inserting duplicate entry for .R.
   sValue = "R";
   setStr.insert(sValue);
   sValue = "H";
   setStr.insert(sValue);
   // Inserting duplicate entry for .H.
   sValue = "H";
   setStr.insert(sValue);


   for (set<string>::const_iterator p = setStr.begin( );p != setStr.end( ); ++p)
      cout << *p << endl;
}

