#include <iostream>
using namespace std;

template<class T> void fun( T, T ) {
  cout <<"G";
} 

void fun( int, double ){
	cout <<"D";
} 

int main() { 
    int  i; 
    double c;    
    fun( i, c ); 
    return 0;
}

