//STL: Algorithms: Predefined Functors and Adapters

#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <list>
#include <functional>

using namespace std;
template <class T>
inline void PrintElements( const T& seq) {
	typename T::const_iterator cit;

	for(cit=seq.begin(); cit!=seq.end(); ++cit)
		cout << *cit << ' ';
	cout << endl;
}  

int main() {
	vector<int>v;
	for(int i=1; i<8; ++i)  v.push_back(i*i);

    PrintElements(v); // 1 4 9 16 25 36 49

	int num = count_if(v.begin(), v.end(), bind2nd(greater<int>(),5));
	cout << num << endl; // 5 , Count of elements >5

	num = count_if(v.begin(), v.end(), bind2nd(modulus<int>(),2));
	cout << num << endl; // 4 , count of elements not divisible by 2

	num = count_if(v.begin(), v.end(), not1(bind2nd(modulus<int>(),2)));
	cout << num << endl;  // 3, count of elements divisible by 2

	transform(v.begin(), v.end(), v.begin(), negate<int>());
	PrintElements(v); // -1 -4 -9 -16 -25 -36 -49

	transform(v.begin(), v.end(), v.begin(), bind2nd(multiplies<int>(), -2));
	PrintElements(v); //2 8 18 32 50 72 98

	random_shuffle(v.begin(), v.end());
	PrintElements(v); // elements randomly shuffled

	sort(v.begin(), v.end()); // Sort in ascending
	PrintElements(v); //2 8 18 32 50 72 98

	sort(v.begin(), v.end(), greater<int>()); // Sort in descending
	PrintElements(v); //98 72 50 32 18 8 2

	random_shuffle(v.begin(), v.end());
	PrintElements(v); // elements randomly shuffled

	partial_sort(v.begin(), v.begin()+3, v.end(), greater<int>());
	PrintElements(v); // Find top 3 elements in descending order, rest of list is unsorted

	nth_element(v.begin(), v.begin()+3, v.end());
	PrintElements(v); // Find 3 smallest elements (not necessarily in order)

	sort(v.begin(), v.end());
	if ( binary_search(v.begin(), v.end(), 50) )
		cout << "50 found in list!" << endl; // 50 found in list!
		else
		 cout << "50 not found in list!" << endl;

	return 0;
}

