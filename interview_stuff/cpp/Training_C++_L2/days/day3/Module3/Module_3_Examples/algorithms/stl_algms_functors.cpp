//STL: Algorithms : Functors

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Bounds {
    int fval, cval, ofb;
  public:
    Bounds(int fv=0, int cv=0) :fval(fv), cval(cv), ofb(0) { }
    void operator() (int num)  {   if (num<fval || num>cval) ++ ofb; }
    int get_ofb() const { return ofb; }
 };
int main() {
  vector<int> v;
   for(int i=1; i<=10; ++i)  v.push_back(i);   //1 2 3 4 5 6 7 8 9 10
   Bounds bo=for_each(v.begin(), v.end(), Bounds(4,9));
   Bounds bt=for_each(v.begin(), v.end(), Bounds(2,5));
   cout << "Out of Bounds(4,9): " << bo.get_ofb() << endl; // 4
   cout << "Out of Bounds(2,5): " << bt.get_ofb() << endl; // 6
   return 0;
}


