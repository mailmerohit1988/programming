//STL: Algorithms

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iterator>
using namespace std;

int main(){
 vector<int> v;
 vector<int> ::iterator pos;
 for(int i=1; i<=9; ++i)
     v.push_back((i*i)/3);

 copy(v.begin(), v.end(), ostream_iterator<int>(cout," "));
 cout << endl; //0 1 3 5 8 12 16 21 27

 cout << count(v.begin(), v.end(), 5) << endl; // 1
 pos=min_element(v.begin(), v.end()); cout << *pos << endl; // 1
 pos=max_element(v.begin(), v.end()); cout << *pos << endl; // 27

 replace(v.begin(), v.end(), 12, 50); // replace all 12s with 50
 copy(v.begin(), v.end(), ostream_iterator<int>(cout," "));
 cout << endl; // 0 1 3 5 8 50 16 21 27

 remove(v.begin(), v.end(), 0); // remove all 0s
 copy(v.begin(), v.end(), ostream_iterator<int>(cout," ")); 
cout << endl; // 1 3 5 8 50 16 21 27 27

 reverse(v.begin(), v.end()); // reverse the order of elements
 copy(v.begin(), v.end(), ostream_iterator<int>(cout," "));
 cout << endl; // 27 27 21 16 50 8 5 3 1

 rotate(v.begin(),v.begin()+2, v.end()); // rotate 2 elements left
 copy(v.begin(), v.end(), ostream_iterator<int>(cout," "));
 cout << endl; // 21 16 50 8 5 3 1 27 27

 sort(v.begin(),v.end()); // sort
 copy(v.begin(), v.end(), ostream_iterator<int>(cout," "));
 cout << endl; // 1 3 5 8 16 21 27 27 50

 v.erase(remove(v.begin(), v.end(),27), v.end()); // erase all 27s
 copy(v.begin(), v.end(), ostream_iterator<int>(cout," "));
 cout << endl; // 1 3 5 8 16 21 50

 cout << binary_search(v.begin(), v.end(), 16) << endl; // 1
 cout << accumulate(v.begin(),v.end(),0) << endl;
 // 104 :Add all elements, initial sum=0 
 return 0;
}

