//STL: Algorithms : Functions/Predicates

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void Print(int val) {	cout << '-' << val << '-'; }
void Cube(int& val) { 	val *=val*val;}
bool isOdd(int val) { 
	if ((val%2)==0) return false;
	else return true;
}
int main(){
 vector<int> v;
 for(int i=1; i<=5; ++i)
	 v.push_back(i);	 //1 2 3 4 5

 for_each(v.begin(), v.end(), Print);
 cout << endl; // -1--2--3--4--5-

 for_each(v.begin(), v.end(), Cube); 
 for_each(v.begin(), v.end(), Print);
 cout << endl; // -1--8--27--64--125-

 cout << count_if(v.begin(), v.end(), isOdd); // 3
 return 0;
}

