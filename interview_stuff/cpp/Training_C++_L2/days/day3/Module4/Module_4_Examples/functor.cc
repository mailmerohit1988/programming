#include <iostream>
using namespace std;

void my_print(int a[], int n)
{
    for(int i=0; i<n; ++i) cout << a[i] << " ";
	cout << endl;
}

void my_swap(int &a,int &b)
{
    int temp(a);
    a=b;
    b=temp;
}

bool my_less(const int& a,const int& b)
{
    return a < b;
}

bool my_greater(const int& a,const int& b)
{
    return a > b;
}

void my_bubble_sort( int a[], int n, bool(*comp)(const int&,const int&))
{
	for(int i=0; i<n-1; i++)
		for(int j=0; j<n-1; j++)
			if(comp(a[j+1], a[j]))    swap(a[j+1], a[j]);
}

class my_class_less 
{
public:
	bool operator()(int a, int b)
	{
	    return a < b;
	}
};

class my_class_greater
{
public:
      bool operator()(int a, int b)
      {
           return a > b;
      }
};

void my_bubble_sort( int a[], int n, my_class_less comp)
{
	for(int i=0; i<n-1; i++)
		for(int j=0; j<n-1; j++)
			if(comp(a[j+1], a[j]))    swap(a[j+1], a[j]);
}

int main()
{
    int array[]={3,5,2,1,4};
	my_print(array, 5);
	
	typedef bool (*fp)(const int&,const int&);
	fp fptr = my_less; my_bubble_sort(array, 5, fptr); my_print(array, 5);
	fptr = my_greater; my_bubble_sort(array, 5, fptr); my_print(array, 5);
	
	my_bubble_sort(array, 5, my_class_less()); my_print(array, 5);
    return 0;
}

