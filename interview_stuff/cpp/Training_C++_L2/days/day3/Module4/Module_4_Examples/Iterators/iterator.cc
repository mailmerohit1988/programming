#include <iostream>
#include<stdexcept>
using namespace std;

class ArrayException:public logic_error {
	public:
		ArrayException(const char *s):logic_error(s){}
};
class array {
	int * arr;
	int size;
public:
	array(int s=0)
	{
		size=s;
		if(size)
		{
		arr=new int[size];
		}
		else
			arr=0;
	}
	int& operator[](int i)
	{
		if(i>=0 && i<size)
			return arr[i]; 
		else
		{
			throw ArrayException("Array index out of range");
		}
	}
	int operator[](int i)const 
	{
		if(i>=0 && i < size)
		
			return arr[i];
		else
		{
			throw ArrayException("Array index out of range");
		}
	}

	void resize(int s)
	{
		
		int *temp =new int[s];
		for(int i=0;i<size && i < s;i++)
			temp[i]=arr[i];

		delete []arr;
		arr=temp;
		size=s;
	}
	array(const array& o);
	array& operator=(const array& o);

	~array(){delete [] arr;}

	class iterator { // nested class iterator
		array *a;
		int index;
	public:
		iterator(array *s=0,int index =0);
		iterator operator++();
		iterator operator ++(int );
		int &operator *();
		bool operator!=(iterator it);
		bool operator==(iterator it);
		

	}; // end of nested class iterator
	iterator begin()
	{
		return iterator(this,0);
	}
	iterator end()
	{
		return iterator(this,size);
	}
	//friend array::iterator;
};
array::array(const array& o)
{
	size=o.size;
	arr=new int[size];
	for(int i=0;i<size;i++)
		arr[i]=o.arr[i];
}

array& array::operator=(const array& o)
{
	if(this!=&o)
	{
		delete []arr;
		size=o.size;
		arr=new int[size];
		for(int i=0;i<size;i++)
			arr[i]=o.arr[i];
	}
	return *this;
}
			
array::iterator::iterator(array *s,int in ){			
	a = s;
	if(a)
		index=in;
	else 
		index=-1;
}
		
array::iterator array::iterator::operator++()
{
	if (index < a->size)
		index++;
	return iterator(a,index);
}

array::iterator array::iterator::operator ++(int )
		{
			
			if (index < a->size)
				index++;
			return iterator(a,index-1);
		}
int & array::iterator::operator *()
		{
			static int i=0;
			if(index >= 0)
				return a->arr[index];
			else
				return i;
		}
bool array::iterator::operator!=(iterator it)
		{
			if((a!=it.a) || (index!=it.index))
				return true;
			else
				return false;
		}
bool array::iterator::operator==(iterator it)
		{
			
				return (!(*this!=it));
		}


int main()
{
	array a1(5);
	array::iterator iter;
	int i;
	
	for(iter=a1.begin();iter!=a1.end();iter++)
		cout << *iter<<endl;
	for( i=0,iter=a1.begin();iter!=a1.end();iter++,i++)
		*iter=i;
	for(iter=a1.begin();iter!=a1.end();iter++)
		cout << *iter<<endl;

}


