#include <iostream>
using namespace std;
class array
{
  int *a;
  int size;

public:
   typedef int * iterator;
   array(int s = 0) {
          size = s;
          if(size)
          {
             a = new int[size];
          }
          else
            a = 0;
   } 
   int& operator[](int i)
   {
        if(i>=0 && i < size)
          return a[i];
        else
          cout << "index out of range" << endl;
   }
   iterator begin() {return a;}
   iterator end() {return a + size;}
};

int main()
{
   array ob(5);
   ob[0] = 0;
   ob[1] = 1;
   ob[2] = 2;
   ob[3] = 3;
   ob[4] = 4; 
  
   array::iterator iter;
   for (iter = ob.begin(); iter != ob.end(); ++iter)
       std::cout << *iter << std::endl;
}
