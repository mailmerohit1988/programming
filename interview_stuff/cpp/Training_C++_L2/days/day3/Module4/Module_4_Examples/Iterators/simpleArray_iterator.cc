#include <iostream>
using namespace std;
class array
{
  int a[5];

public:
   typedef int * iterator;

   int& operator[](int i)
   {
        if(i>=0 && i < 5)
          return a[i];
        else
          cout << "index out of range" << endl;
   }
   iterator begin() {return &a[0];}
   iterator end() {return &a[5];}
};

int main()
{
   array ob;
   ob[0] = 0;
   ob[1] = 1;
   ob[2] = 2;
   ob[3] = 3;
   ob[4] = 4; 
  
   array::iterator iter;
   for (iter = ob.begin(); iter != ob.end(); ++iter)
       std::cout << *iter << std::endl;
}
