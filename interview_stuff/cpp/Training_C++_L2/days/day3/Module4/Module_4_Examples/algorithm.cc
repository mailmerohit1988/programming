#include <iostream>
using namespace std;

void my_print(int a[], int n)
{
    for(int i=0; i<n; ++i) cout << a[i] << " ";
	cout << endl;
}

class my_class 
{
public:
    my_class(int v=2) : val(v) { }
	int operator()(int a)
	{
	    return val * a;
	}
private:
    int val;
};

void my_algorithm(int a[], int n, my_class fo)
{
    for(int i=0; i<n; ++i)
    	a[i]=fo(a[i]);
}

int main()
{
    int array[]={1,2,3,4,5};
	my_print(array, 5);
	
	my_algorithm(array, 5, my_class()); my_print(array, 5);
	
	my_algorithm(array, 5, my_class(10)); my_print(array, 5);
	
	my_class mc(5);
	my_algorithm(array, 5, mc); my_print(array, 5);
	
    return 0;
}

