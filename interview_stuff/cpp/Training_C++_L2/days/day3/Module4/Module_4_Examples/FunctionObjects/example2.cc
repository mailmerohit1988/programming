#include <iostream>
//using namespace std;


class c1 {
	int val;

public:
	c1(int x=0):val(x) {}
	bool operator()(int a,int b)
	{
		return a<b;
	}
	void setval(int x){val=x;}
	int operator()(int i)
	{
		return val * i;
	}
};

void swap(int &a,int &b)
{
	int temp(a);
	a=b;
	b=temp;
}

void bub( int a[],int n,c1 comp )
{
	int i,j;
	for(i=0;i<n-1;i++)
		for(j=0;j<n-1;j++)
			if(comp(a[j+1],a[j]))
				swap(a[j+1],a[j]);
}


int main()
{
	int arr[]={3,1,2,5,0};
	c1 ob(10);
	bub(arr,5,ob);
	int i;
	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
	std::cout << "\n";
		
	
}
