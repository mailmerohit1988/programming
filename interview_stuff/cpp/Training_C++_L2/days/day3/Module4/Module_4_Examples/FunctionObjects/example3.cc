#include <iostream>
//using namespace std;

class c1 {
	int val;

public:
	c1(int x=0):val(x) {}
	bool operator()(int a,int b)// functional behavior 1
	{
		return a<b;
	}
	void setval(int x){val=x;}
	int operator()(int i) //functional behavior 2
	{
		return val * i;
	}
};
class c2 {
	int val;

public:
	c2(int x=0):val(x) {}
	
	void setval(int x){val=x;}
	int operator()(int i)
	{
		return val + i;
	}
};

void transform(int a[],int n,c1 func)
{
	int i;
	for(i=0;i<n;++i)
		a[i]=func(a[i]);
}




int main()
{
	int arr[]={3,1,2,5,0};
	int i;
	c1 ob(10);//function objects can be initialized before you call them
	transform(arr,5,ob); // function object with state

	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";

	ob.setval(20);
	transform(arr,5,ob);// same function object where internal state is changed,same code

	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";

	c1 ob2(5);

	transform(arr,5,ob2); // different function object,different state, same type,same code, different behavior

	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";



	
}
/* ob and ob2 coexist and provide different states for the same operator ()member function at the same time*/ 
/*transform cannot be called with an object of type c2 since there would be type mismatch*/ 
