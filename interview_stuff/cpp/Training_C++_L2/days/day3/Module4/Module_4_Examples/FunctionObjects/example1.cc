/* function pointer as function object*/
#include <iostream>
//using namespace std;


void swap(int &a,int &b)
{
	int temp(a);
	a=b;
	b=temp;
}

void bub( int a[],int n,bool(*comp)(const int&,const int&) )
{
	int i,j;
	for(i=0;i<n-1;i++)
		for(j=0;j<n-1;j++)
			if(comp(a[j+1],a[j]))
				swap(a[j+1],a[j]);
}
bool less(const int& a,const int& b)
{
	return a<b;
}
bool greater(const int& a,const int& b)
{
	return a > b;
}


int main()
{
	int arr[]={3,1,2,5,0};
	typedef bool (*fp)(const int&,const int&);
	fp fptr=less;
  // since the comparison function is less , sorting occurs in ascending order
	bub(arr,5,fptr);
	int i;
	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";
		
// since the comparison function is greater,sorting occurs in descending order
	bub(arr,5,greater);

	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";

}
