#include <iostream>
void swap(int & a, int & b)
{
	int temp =a;
	a=b;
	b=temp;
}
void bub( int* beg , int* end ,bool (*comp)(const int&,const int&)) /* algorithm manipulates and/or modifies elements of the container passed to it. It operates on the elements of the container via iterators passed to it. It expects the iterators to support ++,--,test for inequality and *(dereferencing). It uses a function object to perform the required operation on the elements of the container*/
{
	int* init(beg);
	int* last(end);
	--end;
	
	while(init != last)
	{
		int*  beg1(beg);
		while(beg1 != end)
		{
			int* temp=beg1;
			++beg1;
		 if( comp(*beg1,*temp))
		   	  swap(*beg1,*temp);
		}
		++init;
	}
		
	
}

	bool greater(const int&a ,const int& b)
	{
		return a<b;
	}


int main()
{
int i;
	int arr[]={3,1,2,5,0};
	
	bub(&arr[0],&arr[5],greater );
	
	for(i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";
}
