#include <iostream>
#include <cstring>
#include <stdexcept>
using namespace std;
char buf[80];
class MyException:public logic_error {
int erno;
public:
	
	
	MyException(const char *s, int e) : logic_error(s)
	{
		erno=e;
	}
	const char * what() const throw()
	{
		sprintf(buf,"%s %d",logic_error::what(),erno);
		return buf;
	
	}
		
};
class String {
    char *string;
public:
	String()
	{
			string=new char[1];
			*string = '\0';
	}
    String(const char* txt )
    {
		
		
			int len = strlen(txt);
			string = new char[len+1];
			strcpy(string,txt);
		
    }
   void display()const
    {
		if(*string)
			cout << string << endl;
    }
String(const String& s);
String& operator=(const String& t);
char & operator [](int i);
char operator [](int i) const;

	~String(){cout << "destructor" << endl;delete [] string;}
	friend std::ostream& operator <<(std::ostream& os,const String& s);
typedef char * iterator;
iterator begin()
{
	return string;
}
iterator end()
{
	return (string + strlen(string));
}
};
String::String(const String& s)
{
	
			int len = strlen(s.string);
			string = new char[len+1];
			strcpy(string,s.string);
		
}
String& String::operator=(const String& t)
{
    if (&t != this)
    {
		delete string;
		
			int len = strlen(t.string);
			string = new char[len+1];
			strcpy(string,t.string);
		
	}

		return *this;
}
char & String::operator [](int i)
{
	static char ch='\0';
	if(i>=0 && i < strlen(string))
		return string[i];
	else
		throw MyException("Invalid Index " , i);
}
char  String::operator [](int i)const
{
	char ch='\0';
	if(i>=0 && i < strlen(string))
		return string[i];
	else
		throw MyException("Invalid Index " , i);
}

std::ostream& operator <<(std::ostream& os,const String& s)
{
	os << s.string;
	return os;
}

int main()
{
	int i;
	try
	{    	
		String s1("Spencer");
	   	String::iterator iter;
	
	
	for(iter=s1.begin();iter!=s1.end();++iter)
		cout << *iter<<endl;
	for( i=0,iter=s1.begin();iter!=s1.end();++iter,++i)
		*iter='a'+ i;
	for(iter=s1.begin();iter!=s1.end();++iter)
		cout << *iter<<endl;
	}
	catch(exception& ex)
	{
		cout << ex.what() << endl;
	}
		
	return 0;
}
