#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

class Dllist
{
   private:
       typedef struct Node
       {
          string name;
          Node* next;
          Node* prev;
       };
       Node* head;
       Node* last;
   public:
       Dllist()
       {
         head = 0;
         last = 0;
       }
       bool empty() const { return head==0; }
       friend ostream& operator<<(ostream& ,const Dllist& );
       void Insert(const string& );
       void Remove(const string& );
       Dllist(const Dllist& ob)
	{
		copylist(ob);
	}
	Dllist& operator = (const Dllist& ob)
	{
		if(this != &ob)
		{
			deallocatelist();
			copylist(ob);
		}
		return *this;
	}
	private:
		void copylist(const Dllist & src)
		{
			Node *temp=src.head;
			if ( src.head != 0)
			{
				Node *curr = new Node;
				curr->name = temp->name;
				curr->next= 0;
				curr->prev=(Node*)-1;
				head=curr;
				temp = head->next;
				while (temp!=0)
				{
					Node * newnode = new Node;
					newnode->name = temp->name;
					curr->next = newnode;
					newnode->next = 0;
					newnode->prev = curr;
					curr = newnode;
					temp=temp->next;
				}
			
					last = curr;
			}
			else
			{
					head=0;
					last=0;
			}
		}

		void deallocatelist()
		{
			Node *temp=head;
			while(temp!=0)
			{
				Node *curr = temp;
				temp=temp->next;
				delete curr;
			}
			head=0;
			last=0;
		}
			
		
	 
	public:					
	class iterator {
	 
	public:
	Dllist * cont;
	 Node *cur;
	iterator(Dllist * c= 0,Node *p=0) {cont=c;cur=p;}
	string& operator *()
	{
		return cur->name;
	}
	iterator operator ++ ()
	{
		if( cont != 0)
		{		
			if(cur == (Node*)-1)
				cur = cont->head;
			else
				
			cur = cur->next;
		}
		return *this;
	}
	iterator operator++(int)
	{
		Node*temp=cur;
		if( cont != 0)
		{		
			if(cur == (Node*)-1)
				cur = cont->head;
			else
				
			cur = cur->next;
		}
		return iterator(cont,temp);
	}
	iterator operator -- ()
	{
		
		if( cont != 0)
		{		
			if(cur == 0)
				cur = cont->last;
			else
				
			cur = cur->prev;
		}
		return *this;
	}
	iterator operator--(int)
	{
		Node *temp=cur;
		if( cont != 0)
		{		
			if(cur == 0)
				cur = cont->last;
			else
				
			cur = cur->prev;
		}
		return iterator(cont,temp);
	}
	bool operator != (const iterator& it)
	{
		if((cont!=it.cont) || (cur != it.cur))
			return true;
		else
			return false;
	}
	bool operator == (const iterator& it)
	{
		return (!(*this!=it));
	}
    };
	iterator begin()
	{
		return iterator(this,head);	
	}
	iterator end()
	{
		return iterator(this);
	
	}
	~Dllist()
		{
			deallocatelist();
		}
};

void Dllist::Insert(const string& s)
{
    // Insertion into an Empty List.
    if(empty())
    {
       Node* temp = new Node;
       head = temp;
       last = temp;
       temp->prev = (Node*)-1;
       temp->next = 0;
       temp->name = s;
    }
    else
    {
       Node* curr;
       curr = head;
       while( s>curr->name && curr->next != last->next) curr = curr->next;

       if(curr == head)
       {
         Node* temp = new Node;
         temp->name = s;
         temp->prev = curr;
         temp->next = 0;
         head->next = temp;
         last = temp;
      //  cout<<" Inserted "<<s<<" After "<<curr->name<<endl;
       }
       else
       {
       if(curr == last) //&& s>last->name)
       {
         last->next = new Node;
         (last->next)->prev = last;
         last = last->next;
         last->next = 0;
         last->name = s;
      //  cout<<" Added "<<s<<" at the end "<<endl;
       }
       else
       {
         Node* temp = new Node;
         temp->name = s;
         temp->next = curr;
         (curr->prev)->next = temp;
         temp->prev = curr->prev;
         curr->prev = temp;
      //  cout<<" Inserted "<<s<<" Before "<<curr->name<<endl;
       }
      }
    }
}

ostream& operator<<(ostream& ostr, const Dllist& dl )
{
    if(dl.empty()) ostr<<" The list is empty. "<<endl;
    else
    {
        Dllist::Node* curr;
        for(curr = dl.head; curr != dl.last->next; curr=curr->next)
          ostr<<curr->name<<" ";
        ostr<<endl;
        ostr<<endl;
        return ostr;
    }
}

void Dllist::Remove(const string& s)
{
    bool found = false;
    if(empty())
    {
      cout<<" This is an empty list! "<<endl;
      return;
    }
    else
    {
      Node* curr;
      for(curr = head; curr != last->next; curr = curr->next)
      {
          if(curr->name == s)
          {
             found = true;
             break;
          }
      }
      if(found == false)
      {
       cout<<" The list does not contain specified Node"<<endl;
       return;
      }
      else
      {
         // Curr points to the node to be removed.
         if (curr == head && found)
         {
           if(curr->next != 0)
           {
            head = curr->next;
            delete curr;
            return;
           }
           else
           {
            delete curr;
            head = 0;
            last = 0;
            return;
           }
         }
        if (curr == last && found)
        {
         last = curr->prev;
         delete curr;
         return;
        }
       (curr->prev)->next = curr->next;
       (curr->next)->prev = curr->prev;
        delete curr;
     }
  }
}

void Swap(string & a, string & b)
{
	string temp =a;
	a=b;
	b=temp;
}
void bub( Dllist::iterator beg , Dllist::iterator end ,bool (*comp)( const string&,const string &)) /* algorithm manipulates and/or modifies elements of the container passed to it. It operates on the elements of the container via iterators passed to it. It expects the iterators to support ++,--,test for inequality and *(dereferencing). It uses a function object to perform the required operation on the elements of the container*/
{
	Dllist::iterator init(beg);
	
	Dllist::iterator last(end);
	
	--end;
	
	
	while(init != last)
	{
		
		Dllist::iterator beg1(beg);
		
		while(beg1 != end)
		{
			Dllist::iterator temp=beg1;
			++beg1;
			
		 if( comp(*beg1,*temp))
		{			
			
		   	  Swap(*beg1,*temp);
		}
	
		}
		++init;
	
	}
		
	
}

	bool Greater(const string&a ,const string& b)
	{
		return a<b;
	}


int main()
{
int i;
	Dllist d1;
	Dllist::iterator iter;
	
	char ch ='y';	
	string temp;
	do
	{

		cout<<" Enter Name to be inserted : ";
                  cin>>temp;
                  d1.Insert(temp);
		cout << endl << " Insert another ? y/n : ";
		cin >> ch;
	}	
	while(ch == 'y');

	for(iter=d1.begin();iter!=d1.end();++iter)
		cout << *iter<<endl;
	
	bub(d1.begin(),d1.end(),Greater );
	
	for(iter=d1.begin();iter!=d1.end();++iter)
		cout << *iter<<endl;
}
