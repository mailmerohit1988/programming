#include <iostream>
#include <stdexcept>
using namespace std;

class ArrayException : public logic_error {
	public :
		ArrayException(char* msg):logic_error(msg){}
};

class array {
	
	int size;
	int* arr;

	public :
		array(int s=0){
			size=s;
			if (size)
				arr = new int [size];
			else
				arr=0;
		}
		array( const array &obj){
			size=obj.size;
			arr= new int[size];
			for(int i=0;i<size;i++)
				arr[i]=obj.arr[i];
		}
		
		array& operator=(const array& o);
		
		int& operator[](int i) {
			if(i>=0 && i < size)
				return arr[i];
			else{
				throw ArrayException("Array index out of range");
			}
        }
        int operator[](int i)const{
			if(i>=0 && i < size)
				return arr[i];
			else{
				throw ArrayException("Array index out of range");
			}
        }
        ~array() {
			delete []arr;
		}
		void resize(int s){
			int* newarr=new int[s];
			for(int i=0;i<size;i++)
				newarr[i]=arr[i];
			delete []arr;
			arr=newarr;
			size=s;
		}
		int fnsize(){
			return size;
		}

};

array& array::operator=(const array& o) {
	if(this!=&o){
		delete []arr;
		size=o.size;
		arr=new int[size];
		for(int i=0;i<size;i++)
			arr[i]=o.arr[i];
	}
	return *this;
}

int main()
{
	int i;
	try
	{
	array ob;	
	array ob2(5);
	cout << "ob2" << endl;
	for( i=0;i<5;i++)
		cout << ob2[i]<<endl;
	ob2[1]=2;
	cout << "ob2" << endl;
	
	for( i=0;i<5;i++)
		cout << ob2[i]<<endl;
	int x=ob2[1];
	cout << "x= " << x<<endl;
	//x=ob[0];
//	x=ob2[6];
	ob2.resize(10);
	cout << "ob2" << endl;
	for( i=0;i<10;i++)
		cout << ob2[i]<<endl;

	ob2.resize(13);
	cout << "ob2" << endl;
	for( i=0;i<3;i++)
		cout << ob2[i]<<endl;

	array ob3(ob2);	
	cout << "ob3" << endl;
	for( i=0;i<3;i++)
		cout << ob3[i]<<endl;
	ob3[1]=5;
	cout << "ob2[1] = " << ob2[1] << endl;
	ob2=ob3;
	cout << "ob2[1] = " << ob2[1] << endl;
	ob3[1]=-1;
	cout << "ob2[1] = " << ob2[1] << endl;

	}
	catch(exception& e)
	{
		cout << e.what() << endl;
	}
}


