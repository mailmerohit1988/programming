#ifndef DATE_H
#define DATE_H

#include <iostream>

class Date  {
protected:
    static const int dys[];
    long int ndays; // days inclusive since 
	                // Jan 1,1 (1/1/1 == 1)
public:
    // --- default and initializing constructor
    Date(int mo = 0, int da = 0, int yr = 0)
        { SetDate(mo, da, yr); }
    // --- copy constructor
    Date(const Date& dt)
        { *this = dt; }
    // --- destructor
    virtual ~Date() {}
    // --- overloaded assignment operator
    Date& operator=(const Date& dt)
        { ndays = dt.ndays; return *this; }
    // --- overloaded arithmetic operators
    Date  operator+(int n) const
        { Date dt(*this); dt += n; return dt; }
    Date  operator-(int n) const
        { Date dt(*this); dt -= n; return dt; }
    Date& operator+=(int n)
        { ndays += n; return *this; }
    Date& operator-=(int n)
        { ndays -= n; return *this; }
    Date& operator++()	   // prefix
        { ++ndays; return *this; }
    Date  operator++(int)  // postfix
        { Date dt(*this); dt.ndays++; return dt; }
    Date& operator--()	   // prefix
        { --ndays; return *this; }
    Date  operator--(int)  // postfix
        { Date dt(*this); dt.ndays--; return dt; }
    long int operator-(const Date& dt) const
        { return ndays-dt.ndays; }
    // --- overloaded relational operators
    bool operator==(const Date& dt) const
        { return ndays == dt.ndays; }
    bool operator!=(const Date& dt) const
        { return ndays != dt.ndays; }
    bool operator< (const Date& dt) const
        { return ndays <  dt.ndays; }
    bool operator> (const Date& dt) const
        { return ndays >  dt.ndays; }
    bool operator<=(const Date& dt) const
        { return ndays <= dt.ndays; }
    bool operator>=(const Date& dt) const
        { return ndays >= dt.ndays; }
    // --- getter and setter functions
    void SetDate(int mo, int da, int yr);
    void SetMonth(int mo)
        { *this = Date(mo, GetDay(), GetYear()); }
    void SetDay(int da)
        { *this = Date(GetMonth(), da, GetYear()); }
    void SetYear(int yr)
        { *this = Date(GetMonth(), GetDay(), yr); }
    void GetDate(int& mo, int& da, int& yr) const;
    int  GetMonth() const;
    int  GetDay()   const;
    int  GetYear()  const;
    // --- test for leap year
    bool IsLeapYear(int yr) const
        { return ((yr % 4)==0 && (yr % 1000)!=0); }
    bool IsLeapYear() const
        { return IsLeapYear(GetYear()); }
	void Display(std::ostream& os);
};

std::ostream& operator<<(std::ostream& os, const Date& dt);
std::istream& operator>>(std::istream& is, Date& dt);

inline void Date::Display(std::ostream& os)
{
    os << "Date: "<< *this;
}
		
// ----- overloaded (int + Date)
inline Date operator+(int n, const Date& dt)
{
    return dt + n;
}


const int Date::dys[]={31,28,31,30,31,30,31,31,30,31,30,31};

void Date::SetDate(int mo, int da, int yr)
{
    if (mo < 1 || mo > 12 || yr < 1)  {
        ndays = 0;  // invalid month or year or null date
        return;
    }
    // compute days thru last year
    ndays = (yr-1) * 365 + (yr-1) / 4 - (yr-1) / 1000;
    for (int i = 0; i < mo; i++)  {
        int dy = dys[i];
        if (i == 1 && IsLeapYear(yr))
            dy++;          // make Feb's days 29 if leap year
        if (i < mo-1)      // add in all but this month's days
            ndays += dy;
        else if (da > dy)  {
            ndays = 0;     // invalid day
            return;
        }
    }
    ndays += da;  // add in this month's days
}

void Date::GetDate(int& mo, int& da, int& yr) const
{
    da = ndays;
    if (ndays == 0)  {
        yr = mo = 0;
        return;
    }
    for (yr = 1;; yr++)  {
        int daysthisyear = IsLeapYear(yr) ? 366 : 365;
        if (da <= daysthisyear)
            break;
        da -= daysthisyear;
    }
    for (mo = 1; mo < 13; mo++)	{
        int dy = dys[mo-1];
        if (mo == 2 && IsLeapYear(yr))
            dy++;   // make Feb's days 29 if leap year
        if (da <= dy)
            break;
        da -= dy;
    }
}
int Date::GetMonth() const
{
    int mo, da, yr;
    GetDate(mo, da, yr);
    return mo;
}
int Date::GetDay()   const
{
    int mo, da, yr;
    GetDate(mo, da, yr);
    return da;
}
int Date::GetYear()   const
{
    int mo, da, yr;
    GetDate(mo, da, yr);
    return yr;
}

std::ostream& operator<<(std::ostream& os, const Date& dt)
{
    os << dt.GetMonth() << '/' 
	   << dt.GetDay()   << '/' 
	   << dt.GetYear();
    return os;
}
std::istream& operator>>(std::istream& is, Date& dt)
{
    int mo, da, yr;
    is >> mo >> da >> yr;
    dt.SetDate(mo, da, yr);
    return is;
}
#endif
