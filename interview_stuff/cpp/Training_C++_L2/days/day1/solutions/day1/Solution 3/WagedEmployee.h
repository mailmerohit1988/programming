#ifndef WAGEDEMPLOYEE_H
#define WAGEDEMPLOYEE_H

#include "Employee.h"


class WagedEmployee : public Employee  {
    double hourlywage;
public:
    WagedEmployee(const std::string& nm = "") : Employee(nm)
        { }
    ~WagedEmployee() { }
    double GetHourlyWage() const
        { return hourlywage; }
    void SetHourlyWage(double wage)
        { hourlywage = wage; }
    void FormattedDisplay(std::ostream& os);
};

void WagedEmployee::FormattedDisplay(std::ostream& os)
{
    os << "----Waged Employee----\n";
    Employee::FormattedDisplay(os);
    os << "Hourly wage:    " << hourlywage << '\n';
}

std::ostream& operator<<(std::ostream& os, WagedEmployee& cntr)
{
    os << (static_cast<Employee&>(cntr)) << '\n'
       << cntr.GetHourlyWage() << '\n';
    return os;
}



#endif

