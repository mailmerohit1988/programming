#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>
#include "Date.h"


class Person {
public:
    enum Sex {unknown, male, female};
    enum MaritalStatus {single, married, divorced, widowed};
protected:
    std::string name;
    std::string address;
    std::string phone;
    unsigned ssn;
    Date dob;
    Sex sex;
    MaritalStatus mstatus;
public:
    Person(const std::string& nm = "")
           : name(nm), sex(unknown), mstatus(single)
        { }
    virtual ~Person() { }
    // --- setter functions
    void SetName(const std::string& nm)
        { name = nm; }
    void SetAddress(const std::string& addr)
        { address = addr; }
    void SetPhone(const std::string& phon)
        { phone = phon; }
    void SetSSN(unsigned sn)
        { ssn = sn; }
    void SetDob(const Date& dtb)
        { dob = dtb; }
    void SetSex(Sex sx)
        { sex = sx; }
    void SetMaritalStatus(MaritalStatus st)
        { mstatus = st; }
    // --- getter functions
    const std::string& GetName() const
        { return name; }
    const std::string& GetAddress() const
        { return address; }
    const std::string& GetPhone() const
        { return phone; }
    unsigned GetSSN() const
        { return ssn; }
    const Date& GetDob() const
        { return dob; }
    Sex GetSex() const
        { return sex; }
    MaritalStatus GetMaritalStatus() const
        { return mstatus; }
    virtual void FormattedDisplay(std::ostream& os)=0;
};

void Person::FormattedDisplay(std::ostream& os)
{
    os << "Name:           " << name    << '\n';
    os << "Address:        " << address << '\n';
    os << "Phone:          " << phone   << '\n';
    os << "SSN:            " << ssn     << '\n';
    os << "Date of birth:  " << dob     << '\n';
    os << "Sex:            ";
    switch (sex)  {
        case Person::male:
            os << "male" << '\n';
            break;
        case Person::female:
            os << "female" << '\n';
            break;
        default:
            os << "unknown" << '\n';
            break;
    }
    os << "Marital status: ";
    switch (GetMaritalStatus())  {
        case Person::single:
            os << "single" << '\n';
            break;
        case Person::married:
            os << "married" << '\n';
            break;
        case Person::divorced:
            os << "divorced" << '\n';
            break;
        case Person::widowed:
            os << "widowed" << '\n';
            break;
        default:
            os << "unknown" << '\n';
            break;
    }
}

std::ostream& operator<<(std::ostream& os, const Person& person)
{
    os << person.GetName() << '\n'
       << person.GetAddress() << '\n'
       << person.GetPhone() << '\n'
       << person.GetSSN() << '\n'
       << person.GetDob() << '\n'
       << person.GetSex() << '\n'
       << person.GetMaritalStatus() << '\n';
    return os;
}



#endif

