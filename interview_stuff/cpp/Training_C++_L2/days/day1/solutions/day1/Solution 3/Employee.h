#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include "Person.h"

class Employee : public Person  {
protected:
    Date datehired;
public:
    Employee(const std::string& nm = "") : Person(nm)
        { }
    ~Employee() { }
    Date GetDateHired() const
        { return datehired; }
    void SetDateHired(Date date)
        { datehired = date; }
    void FormattedDisplay(std::ostream& os);
};

void Employee::FormattedDisplay(std::ostream& os)
{
    Person::FormattedDisplay(os);
    os << "Date hired:     " << datehired << '\n';
}

std::ostream& operator<<(std::ostream& os, Employee& empl)
{
    os << ((Person&)empl) << '\n';
    os << empl.GetDateHired() << '\n';
    return os;
}

#endif

