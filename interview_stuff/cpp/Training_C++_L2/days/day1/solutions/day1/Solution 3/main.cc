#include "WagedEmployee.h"
#include "SalariedEmployee.h"
#include "Contractor.h"

std::string ReadString(const std::string& prompt)
{
    std::string str;
    std::cout << prompt << ": ";
    std::getline(std::cin, str);
    return str;
}
Date ReadDate(const std::string& prompt)
{
    Date dt;
    std::cout << prompt << " (mm dd yyyy): ";
    std::cin >> dt;
    return dt;
}
double ReadMoney(const std::string& prompt)
{
    double mn;
    std::cout << prompt << ": ";
    std::cin >> mn;
    return mn;
}
void PersonInput(Person* pPerson)
{
    static std::string str;
    std::getline(std::cin, str); // flush the input buffer
    pPerson->SetName(ReadString("Name"));
    pPerson->SetAddress(ReadString("Address"));
    pPerson->SetPhone(ReadString("Phone"));
    pPerson->SetDob(ReadDate("Date of birth"));
    unsigned int ssn;
    std::cout << "SSN: ";
    std::cin >> ssn;
    pPerson->SetSSN(ssn);
    char sx;
    do {
        std::cout << "Sex (m/f) ";
        std::cin >> sx;
    } while (sx != 'm' && sx != 'f');
    pPerson->SetSex(sx == 'm' ? 
                    Person::male : Person::female);
}
void EmployeeInput(Employee* pEmployee)
{
    pEmployee->SetDateHired(ReadDate("Date hired"));
}
void WagedEmployeeInput(WagedEmployee* pWagedEmployee)
{
    pWagedEmployee->SetHourlyWage(ReadMoney("Hourly wage"));
}
void SalariedEmployeeInput(SalariedEmployee* 
                                         pSalariedEmployee)
{
    pSalariedEmployee->SetSalary(ReadMoney("Salary"));
}
void ContractorInput(Contractor* pContractor)
{
    pContractor->SetStartDate(ReadDate("Start date"));
    pContractor->SetEndDate(ReadDate("End date"));
    pContractor->SetHourlyRate(ReadMoney("Hourly rate"));
}
int main()
{
    Person* pPerson = 0;
    std::cout << "1 = Salaried employee" << std::endl
              << "2 = Waged employee"    << std::endl
              << "3 = Contractor"        << std::endl
              << "Enter selection: ";
    int sel;
    std::cin >> sel;
    switch (sel) {
        case 1:
            pPerson = new SalariedEmployee;
            PersonInput(pPerson);
            EmployeeInput((Employee*)pPerson);
            SalariedEmployeeInput((SalariedEmployee*)pPerson);
            break;
        case 2:
            pPerson = new WagedEmployee;
            PersonInput(pPerson);
            EmployeeInput((Employee*)pPerson);
            WagedEmployeeInput((WagedEmployee*)pPerson);
            break;
        case 3:
            pPerson = new Contractor;
            PersonInput(pPerson);
            ContractorInput((Contractor*)pPerson);
            break;
        default:
            std::cout << "\aIncorrect entry";
            break;
    }
    if (pPerson != 0) {
        pPerson->FormattedDisplay(std::cout);
        delete pPerson;
    }
    return 0;
}

