#include <iostream>
using namespace std;
class Pet {
public:
Pet() {cout <<"Pet"<<endl;}
virtual ~Pet() = 0;
};
Pet::~Pet() {
cout << "~Pet()" << endl;
}
class Dog : public Pet {
public:
Dog() {cout<<"Dog"<<endl;}
~Dog() {
cout << "~Dog()" << endl;
}
};
int main() {
Pet* p = new Dog; // Upcast
delete p; // Virtual destructor call
}
