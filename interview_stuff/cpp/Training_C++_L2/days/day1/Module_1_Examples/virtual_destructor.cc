#include <iostream>
using namespace std;

class OperatingSystem
{
public:
	virtual void drawCircle() = 0;
	virtual ~OperatingSystem() = 0;
};

OperatingSystem::~OperatingSystem() {cout <<"OperatingSystem:destructor()" << endl; }

class Windows : public OperatingSystem
{
public:
	virtual void drawCircle(){ cout << "OperatingSystem:Windows:drawCircle()" << endl; }
	virtual ~Windows(){ cout <<"OperatingSystem:Windows:destructor()" << endl; }
};

class Unix : public OperatingSystem
{
public:
	virtual void drawCircle(){ cout << "OperatingSystem:Unix:drawCircle()" << endl; }
	virtual ~Unix(){ cout <<"OperatingSystem:Unix:destructor()" << endl; }
};

class Shape
{
public:
	virtual void draw() = 0;
	virtual ~Shape() = 0;
};

Shape::~Shape() {cout <<"Shape:destructor()" << endl; }

class Circle : public Shape
{
	OperatingSystem* pOS;
	int radius;
public:
	Circle(int radius, OperatingSystem* rhs):radius(radius), pOS(rhs) {cout << "Shape:Circle:Constructor called" << endl;}
	virtual void draw()
	{
		cout << "Shape:Circle:draw() called" << endl;
		pOS->drawCircle();
	}
	virtual ~Circle(){cout << "Shape:Circle:Destructor called" << endl;}
};

int main()
{
	OperatingSystem* pOS = new Windows;
	Shape* pS = new Circle(10, pOS);
	
	pS->draw();
        delete pS;
        delete pOS;
	return 0;
}
