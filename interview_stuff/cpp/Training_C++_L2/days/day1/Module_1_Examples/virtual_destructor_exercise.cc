#include <iostream>
class AbstractBase {
 public:
  AbstractBase() { std::cout << "Base Ctor" << "\n"; }
  virtual ~AbstractBase() = 0;
};
class Derived : public AbstractBase {
 public:
  Derived() { std::cout << "Derived Ctor" << "\n"; }
  ~Derived() { std::cout << "Derived Dtor" << "\n"; }
};

int main() {     Derived d; } 

