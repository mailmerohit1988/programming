#include <iostream>
using namespace std;

class C{
public: 
int i;
};

int main()
{
const C* a = new C;
//a->i = 5; compiler error , trying to modify a const obj

C *b = const_cast<C*>(a);
b->i = 5;

cout << "a->i : " << a->i << endl;
cout << "b->i : " << b->i << endl;
}


