#include <typeinfo>
#include <iostream>
using namespace std;

class CPolygon{
protected:
   int width;
   int height;

public: 
   void set_values(int a,int b){ width =a; height = b;}
   virtual int area() { return 0;}
};

class CRectangle: public CPolygon{
public:
   int area() {return width * height;}
};

class CTriangle: public CPolygon{
public:
   int area() {return (width * height)/2;}
};

int main()
{
   CPolygon poly;
   CRectangle rect;
   CTriangle tri;
   CPolygon* poly1 = &rect;
   CPolygon* poly2 = &tri;
   CPolygon* poly3 = &poly;
   poly1->set_values(4,5);
   poly2->set_values(4,5);
   poly3->set_values(4,5);

   cout << poly1->area() << endl;
   cout << poly2->area() << endl;
   cout << poly3->area() << endl;


   CPolygon* polyArray[2] = {poly1,poly2};
   CRectangle* downCastRect1 = dynamic_cast<CRectangle *>(polyArray[0]);
   CRectangle* downCastRect2 = dynamic_cast<CRectangle *>(polyArray[1]);
   if(downCastRect1 != 0)
      cout << "polyArray[0] IS A Rectangle object" << endl;
   else 
      cout << "polyArray[0] IS NOT a Rectangle object" << endl;

   if(downCastRect2 != 0)
      cout << "polyArray[1] IS A Rectangle object" << endl;
   else 
      cout << "polyArray[1] IS NOT a Rectangle object" << endl;


   cout << "demonstrating the use of typeid and the name member fn" << endl;
   // name is statically known at compile time
   cout << " Name known statically " << endl;
   for (int i = 0 ; i < 2; i ++)
       cout << typeid(polyArray[i]).name() << endl;

   // name is dynamically looked up at run time 
   // because it is dereferencing the pointer to a polymorphic class
   cout << " Name determined dynamically " << endl;
   for (int i = 0 ; i < 2; i ++)
       cout << typeid(*polyArray[i]).name() << endl;
   return 0;
}
