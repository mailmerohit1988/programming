#include<iostream>
using namespace std;

class Base {
 public: 
  Base() { cout << "Base::Base() " << this << endl;   }
  ~Base() { cout << "Base::~Base() " << this << endl; }
};

class Derived : public Base {
 public:
  Derived() { cout <<"Derived::Derived()" << this << endl;}
  ~Derived(){ cout <<"Derived::~Derived()" << this<< endl;}
};

int main() {
 cout << "In main() " << endl;
 Base b;
 { cout << "In Block1 " << endl; Base b; }
 { cout << "In Block2 " << endl; Derived d; }
 cout << "In main() " << endl;
}

