/*In this example there are no virtual functions in the 
base class TwoNumbers and derived class ThreeNumbers.
Hence all function calls  are resolved at compile time - static binding
This occurs irrespective of whether the class member 
function is called using an object (tw.sum(),th.sum()) or 
pointers to objects (twptr->sum(),thptr->sum()*/

#include <iostream>
using namespace std;
class TwoNumbers
{
protected:
	int a,b;
public:
   void setA(int x) { a=x; }
   void setB(int x) { b=x; }
   int getA() { return a; }
   int getB() { return b;}
   int sum() 
   {
     cout << "	in base sum	" << a+b << endl;
     return a+b;
   }
};

class ThreeNumbers:public TwoNumbers
{
protected:
	int c;
public:

   void setC(int x) { c=x; }
   int difference() 
   {
     cout << " in derived difference" << a-(b-c) << endl;
     return a-(b-c);
   }
   int sum()
   {
     cout << " in derived sum	"	 << a+b+c <<endl;
     return a+b+c;
   }
};

int main()
{
        cout << sizeof(TwoNumbers) << endl;// output: 8
        cout << sizeof(ThreeNumbers) << endl;// output : 12

	TwoNumbers tw;
	tw.setA(10);
	tw.setB(20);
	tw.sum();
	//tw.setC(30);// compiler error: not a member of class TwoNumbers
	//tw.difference();// compiler error: not a member of class TwoNumbers

	ThreeNumbers th;
	th.setA(50);
	th.setB(60);
	th.setC(30);
	th.sum();
	th.difference();

	TwoNumbers *twptr=&tw;
	twptr->setA(100);
	twptr->setB(200);
	twptr->sum();
	//twptr->setC(300);// compiler error: not a member of class TwoNumbers
	//twptr->difference();// compiler error: not a member of class TwoNumbers


	ThreeNumbers *thptr=&th;
	thptr->setA(500);
	thptr->setB(600);
	thptr->setC(400);
	thptr->sum();
	thptr->difference();

      // compiler error:compiler does not convert base class (class TwoNumbers) pointer 
      // to derived class (class ThreeNumbers) pointer

	//ThreeNumbers *thptr2=&tw;


      // compiler converts derived class (class ThreeNumbers)pointer
      // to base class (class TwoNumbers) pointer 
	TwoNumbers *twptr2 = &th;
	twptr2->setA(5);
	twptr2->setB(6);
	twptr2->sum();
	//twptr2->setC(7);// compiler error: not a member of class TwoNumbers
	//twptr2->difference();//compiler error: not a member of class TwoNumbers

}
