#include <iostream>
using namespace std;
void fnTwoTimes(const int *pciNum) { //Takes const argument
	int *piTemp = const_cast<int *>(pciNum);
	*piTemp *=2;
}
int main() {
  int iNum=5;
  cout << iNum; // 5
  fnTwoTimes(&iNum);
  cout << iNum; // 10
  return 0;
}

