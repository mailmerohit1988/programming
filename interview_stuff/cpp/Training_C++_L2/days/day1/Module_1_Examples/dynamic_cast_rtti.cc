#include <iostream>
#include <typeinfo>
using namespace std;
class B1 {
 public:
  virtual void Test() {}; };
class B2 {
 public:
 virtual void Retest() {} };
class D1: public B1, public B2 { };
class D2: public D1 { };
int main() {
 B2* pB2 = new D2;
 D2* pD2 = dynamic_cast<D2*>(pB2);
 if(pD2) cout << "1OK " << endl;
 D1* pD1 = dynamic_cast<D1*>(pB2);
 if(pD1) cout << "2OK " << endl;
 B1* pB1 = dynamic_cast<B1*>(pB2); // Cross Cast
 if(pB1) cout << "3OK " << endl;
 if(typeid(pB2)==typeid(D2*)) cout << "4OK" << endl; //RTTI unable to figure out
 if(typeid(pB2)==typeid(B2*)) cout << "5OK" << endl;
}

