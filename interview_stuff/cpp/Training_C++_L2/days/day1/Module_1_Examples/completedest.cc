#include <iostream>
using namespace std;

class base{
public:
base(){cout << "base's constructor\n";}
virtual void detailsdisp()=0;
virtual ~base(){cout << "base's destructor\n";}
};

void base::detailsdisp()
{ cout << "Here special facts about base are displayed\n";}

class derived1:public base{
public:
derived1(){cout << "derived 1 constructor\n";}
virtual ~derived1(){cout << "derived 1 destructor\n";}
void detailsdisp() 
{ 
   base::detailsdisp();
   cout << "Details of derived 1 are displayed\n";
}
};

class derived2 :public base{
public:
derived2(){cout << "derived 2 constructor\n";}
// In the following, the destructor is virtual even without 
// the use of the virtual keyword, because the base class 
// destructor is virtual 
~derived2(){cout << "derived 2 destructor\n";} 
virtual void detailsdisp() 
{ 
  base::detailsdisp();
  cout << "Details of derived 2 are displayed\n";
}

};
int main()
{
	base *p;
	int ch;
while(1)
{
	cout << "1. base\n";
	cout << "2.derived 1\n";
	cout << "3.derived 2\n";
	cout << "4.Exit\n";
	cout << endl << "Enter your choice : ";
	cin >> ch;
	p=0;
	switch(ch)
	{
		/*case 1:p=new base; // cannot do this becasue base is an abstract class 
		       p->detailsdisp();
		       break; */   

		case 2:p=new derived1;
	               p->detailsdisp();
     		         break;

		case 3:p=new derived2;
                         p->detailsdisp();
                         break;

		case 4:return 0;

		default:cout << "Invalid Choice\n";
	}
	delete p;
        //if not deleted each time  a new choice is made old object 
        //'memory not returned to os and new memory taken to create a new object
} // end of while
} // end of main
