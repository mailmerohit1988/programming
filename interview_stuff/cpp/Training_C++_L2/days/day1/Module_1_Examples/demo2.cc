/*In this example  the base class TwoNumbers has 2 virtual functions 
sum() and mul() and the derived class ThreeNumbers inherits these 2 
virtual functions and has one virtual function of its own namely difference().
Function calls to sum() and mul() via TwoNumbers pointer( twptr->sum(),twptr->mul()) 
and calls to sum(),mul() and difference() via ThreeNumbers pointer
( thptr->sum(),thptr->mul(),thptr->difference()) are resolved at run time 
this is dynamic binding.All function calls ( virtual or non-virtula) 
made via class objects are resolved at compile time, that is static binding occurs. 
All non-virtual function calls made via pointers to the classes are also resolved 
at compile time(static binding).*/

#include <iostream>
using namespace std;
class TwoNumbers  // polymorphic class
{
protected:
	int a;int b;
public:
   void setA(int x) { a=x; }
   void setB(int x) { b=x; }
   int getA() 
   {
       cout << "getA " ;
      /* the following is compiled as this->sum() . 
         Since virtual member function is being invoked via pointer to polymorphic class 
        (data type of this is TwoNumbers*)
        dynamic binding takes place */

      sum();
      return a;
    }

   int getB() { return b;}
   virtual int sum() // virtual function
   {
      cout << "	in base sum	" << a+b << endl;
      return a+b;
   }
   virtual int mul()// virtual function
   {
      cout << "in base mul      " << a*b << endl;
      return a*b;
   }
};

class ThreeNumbers:public TwoNumbers  // polymorphic class
{

protected:
	int c;
public:
   void setC(int x) { c=x; }
   virtual int difference() // virtual function
   {
    /* the following is compiled as this->sum() . 
       Since virtual member function is being invoked via pointer 
       to polymorphic class (data type of this is ThreeoNumbers*) 
       dynamic binding takes place */

       sum();

       cout << " in derived difference" << a-(b-c) << endl;
       return a-(b-c);
   }

   /* overriding base class virtual function, 
      still a virtual without virtual keyword */
   int sum()
   {
      cout << " in derived sum	"	 << a+b+c <<endl;
      return a+b+c;
   }
};

int main()
{
	cout << sizeof(TwoNumbers) << endl;
	cout << sizeof(ThreeNumbers) << endl;

	TwoNumbers tw;
	tw.setA(10);//static binding
	tw.setB(20);// static binding
	tw.sum();// static binding
	tw.getA();// static binding
        // compiler error: The functions are not a member of class TwoNumbers
	//tw.setC(30);
	//tw.difference();

	ThreeNumbers th;
	th.setA(50);// static binding
	th.setB(60);// static binding
	th.setC(30);// static binding
	th.sum();// static binding
        th.getA();// static binding
	th.difference();//static binding


        
	TwoNumbers *twptr=&tw;
	twptr->setA(100);// static binding
	twptr->setB(200);// static binding
	twptr->sum();// dynamic binding
	twptr->mul();//dynamic binding
        // compiler error: The functions are not a member of class TwoNumbers


	//twptr->setC(300);
	//twptr->difference();

	ThreeNumbers *thptr=&th;
	thptr->setA(500);// static binding
	thptr->setB(600);// static binding
	thptr->setC(400);// static binding
	thptr->sum();// dynamic binding
	thptr->difference();//dynamic binding
	thptr->mul();//dynamic binding

        /* In the following the compiler converts derived class (class ThreeNumbers) 
           pointer to base class (class TwoNumbers) pointer and assigns to twptr. 
           So now twptr(pointer to TwoNumbers) contains the address of th 
           which is an object of class ThreeNumbers  */
	twptr=thptr;
	twptr->setA(5);
	twptr->setB(6);
	twptr->sum();
	twptr->mul();
        // compiler error: The functions are not a member of class TwoNumbers
	//twptr->setC(7);
	//twptr->difference();

        /* The following is a compiler error:compiler does not convert base class 
           (class TwoNumbers) pointer to derived class (class ThreeNumbers) pointer */
	//thptr=twptr;
	
}
