#include <iostream>
using namespace std;

class base{
public:
base(){cout << "base's constructor\n";}
virtual ~base(){cout << "base's destructor\n";}

virtual void interface_inheritance()=0; 
virtual void default_implementation_inheritance() 
{ cout << "base::default virtual" << endl;} 
void invariant() { cout << "base::invariant" << endl;} 
};

class derived1:public base{
public:
derived1(){cout << "derived 1 constructor\n";}
virtual ~derived1(){cout << "derived 1 destructor\n";}

// mandatory implementation needed
void interface_inheritance() 
{ 
   cout << "Details of derived 1 are displayed\n";
}

// override the default implementation
virtual void default_implementation_inheritance() 
{ cout << "derived1::overriding default virtual" << endl;} 

// override the invariant implementation
// has no effect  
void invariant() { cout << "derived1::overriding invariant" << endl;} 
};


class derived2 :public base{
public:
derived2(){cout << "derived 2 constructor\n";}
~derived2(){cout << "derived 2 destructor\n";} 

// Mandatory implementation required
virtual void interface_inheritance () 
{ 
  cout << "Details of derived 2 are displayed\n";
}

// use the default implementation for 
// default_implementation_inheritance() 

// override the invariant implementation
// has no effect  
void invariant() { cout << "derived2::overriding invariant" << endl;} 

};




int main()
{
	base *p;
	int ch;
while(1)
{
	cout << "1. base\n";
	cout << "2.derived 1\n";
	cout << "3.derived 2\n";
	cout << "4.Exit\n";
	cout << endl << "Enter your choice : ";
	cin >> ch;
	p=0;
	switch(ch)
	{
               // cannot do this becasue base is an abstract class 
		/* case 1:p=new base; 
		       break; */   

		case 2:p=new derived1;
		         break;

		case 3:p=new derived2;
                         break;

		case 4:return 0;

		default:cout << "Invalid Choice\n";
	}

        if(p)
        { 
	   p->interface_inheritance();
           p->default_implementation_inheritance();
           p->invariant();
	   delete p;
        }

        //if not deleted each time  a new choice is made old object 
        //'memory not returned to os and new memory taken to create a new object
} // end of while
} // end of main
