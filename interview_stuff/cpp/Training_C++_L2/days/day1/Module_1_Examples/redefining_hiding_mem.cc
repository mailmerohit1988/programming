#include<iostream>
using namespace std;

class Base {
 public:  
  void Display() {  cout << "Base::Display() " << endl;  }	
  void setVal(int v){ cout << "Base::setVal(int) " << v; }	
  void setVal(double v){ cout<<"Base::setVal(double)"<<v;}	
};

class Derived : public Base {
 public:
  void Display(){cout << "Derived::Display() " << endl;  }	
  void setVal(char v){cout << "Derived::setVal(char)"<<v <<endl;}	
};

int main() {
 Derived d;
 char c = 'A';
 d.setVal(c);     // Ok! Derived::setVal(char) A
 d.setVal(65);    // Oops, Derived::setVal(char) A
 d.setVal(67.25); // Oops, Derived::setVal(char) C
}

