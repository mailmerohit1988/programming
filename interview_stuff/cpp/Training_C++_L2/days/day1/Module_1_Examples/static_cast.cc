#include <exception>
#include <iostream>
using namespace std;

class CBase {};
class CDerived:public CBase {};
class SomeClass{};

int main(){
CBase *pb = new CBase; 
CDerived* pd = new CDerived;
SomeClass* ps = new SomeClass;


// No Casting
CBase* pb3 = pd;
//CDerived* pd3 = pb; // invalid and compiler error!
//CDerived* ps3 = ps; // compiler error !

// c-style casting
CBase* pb1 = (CBase*)(pd); // valid, d "is-a" CBase
CDerived *pd1 = (CDerived*)(pb); // b not "is-a" CDerived but no compiler error! 
CDerived* ps3 = (CDerived*)(ps); // no compiler error , no run time error in casting unrelated classes!

// Static Cast
CBase* pb2 = static_cast<CBase*>(pd); // valid, d "is-a" CBase, no casting needed, implicitly cast by compiler 
CDerived *pd2 = static_cast<CDerived*>(pb); // invalid, b not "is-a" CDerived but not a compiler error! 
//no safety check is performed during runtime to check if the object being converted is in fact 
//a full object of the destination type. Therefore, it is up to the programmer to ensure that 
//the conversion is safe. The overhead of the type-safety checks of dynamic_cast is avoided. 

CDerived *pd4 = static_cast<CDerived*>(ps); // compiler error , unrelated classes

void *voidptr1 = new CDerived;
CDerived *pd5 = static_cast<CDerived*>(voidptr1);//safe, voidptr1 has CDerived Object at run time 

void *voidptr2 = new SomeClass;
CDerived *pd6 = static_cast<CDerived*>(voidptr2);//No compiler/runtime error but unsafe, voidptr2 has SomeClass(unrelated object) at runtime 

}



