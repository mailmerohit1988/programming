#include <iostream>
#include <string>

void println(std::string const& msg) { std::cout << msg << '\n'; }

class Base {
public:
  Base()              { println("Base::Base()");  virt(); }
  virtual void virt() { println("Base::virt()"); }
};

class Derived : public Base {
public:
  Derived()           { println("Derived::Derived()");  virt(); }
  virtual void virt() { println("Derived::virt()"); }
};
int main(){
  Derived d;
}

