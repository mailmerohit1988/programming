#include<iostream>
using namespace std;

class A {
 public:
	void Know_me() { cout << "A";  }
};
class B: virtual public A { };
class C: virtual public A { };
class D: public B, public C { };    
int main() {
	D oD; oD.Know_me();//OK
         oD.A::Know_me(); // OK
         oD.B::Know_me(); //OK
         oD.C::Know_me(); //OK
  return 0;
}

