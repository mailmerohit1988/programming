#include <iostream>
using namespace std;
class WindowA {
 public:
 virtual ~WindowA() { }
  virtual void SetColor(int iColor) {;}//Sets the border color
};
class WindowB {
 public:
  virtual ~WindowB() { }
  virtual void SetColor(int iColor) {;}//Sets the menu color
};
class AuxWindowA : public WindowA {
 void SetColor(int iColor) { //override inherited one
   SetColorA(iColor);
 }
 public:
  virtual void SetColorA(int iColor)=0;
};
class AuxWindowB : public WindowB{
 void SetColor(int iColor) { //override inherited one
   SetColorB(iColor);
 }
 public:
  virtual void SetColorB(int iColor)=0;
};
class MyWindow :public AuxWindowA, public AuxWindowB {
public:
 void SetColorA(int iColor) {   cout << "set border color in style" << endl;  }
 void SetColorB(int iColor) {   cout << "set menu color in style " << endl;  }
};
int main() {
 MyWindow *pmw =new MyWindow;
 WindowA *pwa=pmw;
 WindowB *pwb=pmw;
 pwa->SetColor(1); // set border color in style
 pwb->SetColor(2); // set menu color in style
 delete pmw;
 return 0;
}
