#include<iostream>
using namespace std;

class A {
 public:
        A() {cout<<"A ctor"<<endl;}
};
class B {
 public:
        B() {cout<<"B ctor"<<endl;}
};
class C {
 public:
        C() {cout<<"C ctor"<<endl;}
};
class D: public A,public virtual B,public C { public: D() {cout<<"D ctor"<<endl;} };
class E: public virtual B { public: E() {cout<<"E ctor"<<endl;} };
class F: public D, public E { public: F() {cout<<"F ctor"<<endl;} };    

int main() {
	F oF; 
  return 0;
}

