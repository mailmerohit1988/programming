#include<iostream>
using std::cout;
class B { public : virtual ~B(){} };
class D: public B { public : virtual ~D() {} };
 
int main() {
  B *b = new D;
  D *d = dynamic_cast<D*>(b);
  if(d != NULL)
    cout<<"works";
  else
    cout<<"cannot cast B* to D*";
  //getchar();
  return 0;
}

