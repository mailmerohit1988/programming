#include<iostream>
using namespace std;

class A {
 public:
	void Know_me() { cout << "A";  }
};
class B: public A { };
class C: public A { };
class D: public B, public C { };    
int main() {
	D oD; oD.Know_me();//Ambiguous
         oD.A::Know_me(); // Ambiguous
         oD.B::Know_me(); //OK
         oD.C::Know_me(); //OK
  return 0;
}

