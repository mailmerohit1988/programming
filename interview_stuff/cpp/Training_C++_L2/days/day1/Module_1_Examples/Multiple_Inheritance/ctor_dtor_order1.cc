#include<iostream>
using namespace std;

class A {
 public:
        A() {cout<<"A ctor"<<endl;}
	void Know_me() { cout << "A";  }
};
class B: virtual public A { public: B() {cout<<"B ctor"<<endl;} };
class C: virtual public A { public: C() {cout<<"C ctor"<<endl;} };
class D: public B, public C { public: D() {cout<<"D ctor"<<endl;} };    
int main() {
	D oD; oD.Know_me();//OK
         oD.A::Know_me(); // OK
         oD.B::Know_me(); //OK
         oD.C::Know_me(); //OK
  return 0;
}

