#include <iostream>
using namespace std;
class Shape {
 public:
  virtual ~Shape() { }
  virtual void Rotate()=0;
  virtual Shape* Clone() const =0;
  virtual Shape* Create() const =0;
};
class Circle : public Shape {
  public:
   void Rotate() { cout <<"Circle Rotated" << endl; }
   Circle* Clone () const { return new Circle(*this); }
   Circle* Create() const { return new Circle; }
};
class Triangle : public Shape {
  public:
   void Rotate() { cout <<"Triangle Rotated" << endl; }
   Triangle* Clone () const { return new Triangle(*this); }
   Triangle* Create() const { return new Triangle; }
};
void fnUserWantsFun(Shape& oS) {
  cout <<"Original Object: "<< &oS <<endl;
  Shape *ps=oS.Clone(); //ps points to a copy of object. This can be used without affecting oS.
  cout <<"Cloned Object: " << ps <<endl;
  ps->Rotate();
}
int main() {
 Shape *ps;
 if (rand()%2 ==0) ps=new Circle;
  else
    ps=new Triangle;
 fnUserWantsFun(*ps);
 return 0;
}

