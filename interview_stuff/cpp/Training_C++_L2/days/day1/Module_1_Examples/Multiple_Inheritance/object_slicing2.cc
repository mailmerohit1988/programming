class A {
   int foo;
};

class B : public A {
   int bar;
};

B b;

A a = b;
