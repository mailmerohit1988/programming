#include<iostream>
using namespace std;

class A {
 public:
  virtual void Know_me()  {
     cout <<"A";
  }
};

class B: public virtual A {
 public:
   void Know_me() {cout <<"B"; }
};

class C: public virtual A { };
class D: public B, public C { };    
int main() {
   A *pa=new D;
   pa->Know_me(); // Ok or Ambiguous???
 return 0;
}

