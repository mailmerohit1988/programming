#include <iostream>
using namespace std;
 
class Book {
 public:
     virtual void Info() {
		 cout << "Book" << endl;
	 }
 }; 
 class PaperBack : public Book {
 public:
     void Info() {
		 cout << "PaperBack of "; Book::Info(); cout << endl;
	 }
 };
 void fnGetBookDetails(Book oB) { //Slices Derived to Base
	 oB.Info(); 
 }
 
 void fnBrowseLibrary(Book &oB) { // OK! pass by reference, no slicing
	 fnGetBookDetails(oB); 
 } 

 int main() {
     PaperBack oPB;
     fnBrowseLibrary(oPB); // Displays Book
   return 0;
 }

