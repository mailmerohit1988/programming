#include<iostream>
using namespace std;

class SomeClass {
public:
   virtual void pure_virtual() = 0;  // a pure virtual function
   // note that there is no function body	 
};

/*This is an implementation of the pure_virtual function
    which is declared as a pure virtual function.
    This is perfectly legal:
*/
void SomeClass::pure_virtual() {
    cout<<"This is a test"<<endl;
}

class MyClass:public SomeClass{
public:
  void pure_virtual() {
    SomeClass::pure_virtual();cout<<"This is a test1"<<endl;
  }
};

int main(){
 MyClass m;
 m.pure_virtual();
}
