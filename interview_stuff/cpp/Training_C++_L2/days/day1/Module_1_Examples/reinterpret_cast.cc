#include <iostream>
using namespace std;


class CBase {};
class CDerived : public CBase {};
class SomeClass{};

int main()
{
   CBase* pb = new CBase();
   CDerived* pd = new CDerived();
   SomeClass* ps = new SomeClass();

  // SomeClass* ps1 = static_cast<SomeClass*>(pb); // compiler error 
   SomeClass* ps2 = reinterpret_cast<SomeClass*>(pb); // no compiler error
}



