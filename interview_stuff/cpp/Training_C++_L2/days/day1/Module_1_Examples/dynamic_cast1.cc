#include <exception>
#include <iostream>
using namespace std;

class CBase {};
class CDerived:public CBase {};
class SomeClass {};

class VBase{virtual void dummy(){}};
class VDerived: public VBase{ int a;};

int main(){
CBase b; CBase *pb;
CDerived d; CDerived* pd;


// c-style casting
pb = (CBase*)(&d); // valid, d "is-a" CBase
pd = (CDerived*)(&b); // invalid, b not "is-a" CDerived but not a compiler error !

pb = dynamic_cast<CBase*>(&d); // valid, safer, run-time overhead
//pd = dynamic_cast<CDerived*>(&b); // invalid, compiler error.
// Not a compiler error if polymorphic,but still safer as shown below 


VBase *pba = new VDerived;
VBase *pbb = new VBase;
VDerived *pd1;

// For polymorphic types, conversion of pointer to base object to a Derived Type (1)
// as well as conversion of derived object to Derived type is not a compiler error. 
// For (1) invalid conversion, a null pointer is produced, no runtime issue.
try
{

pd1 = dynamic_cast<VDerived*>(pba);
//above  Not a compiler error . pba points to an object of VDerived type at run time. 

// below, not a compiler error. but pbb points to an object of VBase type at run time
// result null pointer , no run-time issue
pd1 = dynamic_cast<VDerived*>(pbb);
if (pd1 == 0)
  cout << " Null ptr returned from converting an object of Base type to Derived Type using dynamic casting" << endl;

// unsafe C-style casting for the above 
// No compiler error !
// Not returning a Null pointer for this incomplete conversion!

pd1 = (VDerived*)(pbb);
if (pd1 == 0)
   cout << " Null ptr returned from converting an object of Base type to Derived Type using c-style casting" << endl;

} catch(exception& e) 
{
   cout << "Exception in dynamic casting" << e.what();
}
}



