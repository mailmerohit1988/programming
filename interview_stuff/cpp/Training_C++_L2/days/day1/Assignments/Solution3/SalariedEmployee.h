#ifndef SALARIEDEMPLOYEE_H
#define SALARIEDEMPLOYEE_H

#include "Employee.h"


class SalariedEmployee : public Employee  {
    double salary;
public:
    SalariedEmployee(const std::string& nm = "") : Employee(nm)
        { }
    ~SalariedEmployee() { }
    double GetSalary() const
        { return salary; }
    void SetSalary(double sal)
        { salary = sal; }
    void FormattedDisplay(std::ostream& os);
};
void SalariedEmployee::FormattedDisplay(std::ostream& os)
{
    os << "----Salaried Employee----\n";
    Employee::FormattedDisplay(os);
    os << "Salary:         " << salary << '\n';
}

std::ostream& operator<<(std::ostream& os, SalariedEmployee& empl)
{
    os << ((Employee&)empl) << '\n' << empl.GetSalary() << '\n';
    return os;
}

#endif

