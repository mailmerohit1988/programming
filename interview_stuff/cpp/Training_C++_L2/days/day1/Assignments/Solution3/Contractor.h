#ifndef CONTRACTOR_H
#define CONTRACTOR_H

#include "Person.h"
#include "Date.h"

class Contractor : public Person  {
    Date startdate;
    Date enddate;
    double hourlyrate;
public:
    Contractor(const std::string& nm = "") : Person(nm)
        { }
     ~Contractor() { }
    const Date& GetStartDate() const
        { return startdate; }
    const Date& GetEndDate() const
        { return enddate; }
    double GetHourlyRate() const
        { return hourlyrate; }
    void SetStartDate(Date date)
        { startdate = date; }
    void SetEndDate(Date date)
        { enddate = date; }
    void SetHourlyRate(double rate)
        { hourlyrate = rate; }
    
    void FormattedDisplay(std::ostream& os);
};


void Contractor::FormattedDisplay(std::ostream& os)
{
    os << "----Contractor----\n";
    Person::FormattedDisplay(os);
    os << "Start date:     " << startdate  << '\n';
    os << "End date:       " << enddate    << '\n';
    os << "Hourly rate:    " << hourlyrate << '\n';
}

std::ostream& operator<<(std::ostream& os, const Contractor& cntr)
{
    os << ((Person&)cntr)      << '\n'
       << cntr.GetStartDate()  << '\n'
       << cntr.GetEndDate()    << '\n'
       << cntr.GetHourlyRate() << '\n';
    return os;
}

#endif

