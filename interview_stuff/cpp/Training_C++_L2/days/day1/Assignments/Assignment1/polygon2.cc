#include <iostream>
using namespace std;

class CPolygon {
	int a;
	int b;
	public :
		void set_values(int x, int y){
			a=x;
			b=y;
		}
		int getA(){
			return a;
		}
		int getB(){
			return b;
		}
		virtual int area(){
			return a*b;
		}
		
};

class CRectangle : public CPolygon{
	public :
		int area() {
			return (getA())*(getB());
		}
};

class CTriangle : public CPolygon{
	public :
		int area() {
			return ((getA())*(getB()))/2;
		}
		
};

int main()
{
   CPolygon poly;
   CRectangle rect;
   CTriangle tri;
   CPolygon* poly1 = &rect;
   CPolygon* poly2 = &tri;
   CPolygon* poly3 = &poly;
   poly1->set_values(4,5);
   poly2->set_values(4,5);
   poly3->set_values(4,5);
// you should be able to do the following and still get the correct results
// for the area of rectangle and triangle
   cout << poly1->area() << endl;
   cout << poly2->area() << endl;
   cout << poly3->area() << endl;
   cout << sizeof(rect)  << endl;

   return 0;
}
