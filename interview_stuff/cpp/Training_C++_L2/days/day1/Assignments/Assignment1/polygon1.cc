#include <iostream>
using namespace std;

class CPolygon {
	int a;
	int b;
	public :
		void set_values(int x, int y){
			a=x;
			b=y;
		}
		int getA(){
			return a;
		}
		int getB(){
			return b;
		}
		
};

class CRectangle : public CPolygon{
	public :
		int area() {
			return (getA())*(getB());
		}
};

class CTriangle : public CPolygon{
	public :
		int area() {
			return ((getA())*(getB()))/2;
		}
		
};

int main()
{
    CRectangle rect;
    CTriangle tri;
    CPolygon* poly1 = &rect;
    CPolygon* poly2 = &tri;
    poly1->set_values(4,5);
    poly2->set_values(4,5);
    // cannot call poly1->area() and poly2->area()
    // because area() is not a membefr fn of CPolygon
    cout << rect.area() << endl;
    cout << tri.area() << endl;
    cout << sizeof(rect) << endl;
    return 0;
}
