#include <iostream>
using std::cout;

class AbstractBase {
public:
  virtual ~AbstractBase() = 0;
};


AbstractBase::~AbstractBase(){
	std::cout << "Base Dtor" << "\n";
}

class Derived : public AbstractBase {};

int main() 
{ 
	Derived d; 
} 
