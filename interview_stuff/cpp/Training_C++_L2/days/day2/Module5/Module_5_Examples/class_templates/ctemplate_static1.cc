#include <iostream> 
using namespace std;

template <class Type>
class A{
public :
   static Type  st;
};



int main()
{
   A<int> ob1; // static member is of type int
   A<int*> ob2; // static member is of type int*
}
