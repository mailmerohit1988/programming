#include<cstring>
class StackException
{
	public:
		char extext[100];
		int exno;
	StackException(char messg[],int no)
	{
		strcpy(extext,messg);
		exno=no;
	}
};


template<class stackType>
class Stack {
	stackType *st;
	int top;
	int size;
public:
void push(stackType);
stackType pop();
Stack(int s);
~Stack() { delete [] st;}
};


template<class stackType>
void Stack<stackType>::push(stackType num)
{
	if(top<=0)
		throw StackException("Stack Overflow", -1);
	else
		st[--top]=num;
}

template<class stackType>
stackType Stack<stackType>::pop()
{
	if(top>=size)
		throw StackException("Stack Underflow", -2);
	else
		return st[top++];
}
template<class stackType>
Stack<stackType>::Stack(int s)
{
	if(s<=0)
	   s=1;	
	size=s;	
	st=new stackType[size];
	top=size;
}

