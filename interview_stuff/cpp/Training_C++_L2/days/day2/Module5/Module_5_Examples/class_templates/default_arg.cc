#include <iostream>
#include <cstdlib>
using namespace std;

template<class T=int ,int size=10> 
class SafeArray {
	T a[size];//length of array is passed in size
	public:
	SafeArray(){
		int i=0;
		for(i=0;i<size;i++)
			a[i]=i;
		}
		T& operator[](int i);
};
template <class T,int size> 
T& SafeArray<T,size>::operator[](int i)
{
	if(i<0 || i>size-1){
		cout << "\n Index value of " << i << " is out of bounds.\n";
		exit(1);
		}
		return a[i];
}
int main()
{
   SafeArray<int,15> o1;
   SafeArray<double> o2;
   SafeArray<> defarray;

   int i;
   cout << "Integer Array: ";
   for(i=0;i<15;i++) o1[i]=i;
   for(i=0;i<15;i++) cout << o1[i] << " ";
   cout << endl;
	
   cout << "Double Array: ";
   for(i=0;i<10;i++) o2[i]=(double) i/3;
   for(i=0;i<10;i++) cout << o2[i] << " ";
   cout << endl;

   cout << "Default Array: ";
   for(i=0;i<10;i++) defarray[i]=i;
   for(i=0;i<10;i++) cout << defarray[i] << " ";
   cout << endl;

}
