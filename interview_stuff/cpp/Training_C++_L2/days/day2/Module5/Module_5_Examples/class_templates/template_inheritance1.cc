//a template class that inherits from a non-template class 


#include <iostream>
using namespace std;

class num {
	protected:
	int val;
	public:
	num(int x){val=x;}
	virtual int getval()=0;
};
template<class T>
 class sqrnum:public num {
	T b;
	public:
	sqrnum(int a,T x):num(a){b=x;}
	int getval() {return val *val;}
};

int main()
{
	num *bp;
	sqrnum<int> *dp,sqrint_ob(3,4);
	sqrnum<double> ob2(2,3.5);
	bp=&ob2;
cout << bp->getval() << endl;
	bp=&sqrint_ob;
	cout << bp->getval() << endl;
	dp=&sqrint_ob;
	cout << dp->getval() << endl;
	int i;
	cin >> i;
}
