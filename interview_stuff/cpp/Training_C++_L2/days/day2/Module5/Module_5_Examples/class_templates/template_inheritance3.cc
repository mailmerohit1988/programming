//a template class that inherits from another template class 

#include <iostream>
using namespace std;

template <class T> class num {
protected:
	T val;
	public:
	num(T x){val=x;}
	virtual T getval() {return val;}
};

template< > class num <int> {
protected:
	int val;
	public:
	num(int  x){val=x+x;}
	virtual int getval() {cout << " returning x + x : " << endl;
                              return val;}
};

template<class T> class sqrnum:public num<T> {
	T x;
	public:
	sqrnum(T x):num<T>(x){}
	T getval() {
         return num<T>::val*num<T>::val;}
};

template<> class sqrnum<int>:public num<int> {
	int x;
	public:
	sqrnum(int x):num<int>(x){}
	int getval() {
         cout << "returning num * num : " << endl; 
         return num<int>::val*num<int>::val;}
};

int main()
{
	num<int> numint_ob(2);
	sqrnum<int> sqrint_ob(3);

        
	cout << "value sent to  numint_ob = 2" << endl;
	cout << "numint_ob.getval() : " << endl;
	cout << numint_ob.getval() << endl;

	cout << "value sent to  sqrint_ob = 3" << endl;
	cout << "sqrint_ob.getval() : " << endl;
	cout << sqrint_ob.getval() << endl;
}
