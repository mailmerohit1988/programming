//a non-template class that inherits from a template class if type of template class is
//given by derived class

#include <iostream>
using namespace std;

template <class T> class num {
	protected:
	T val;
	public:
	num(T x){val=x;}
	virtual T getval() {return val;}
};

 class sqrnum:public num<int> {

	public:
	sqrnum(int a):num<int>(a){}
	 int getval() {return val *val;}
};

int main()
{
	num<int> *bp,numint_ob(2);
	sqrnum *dp,sqrint_ob(3);
	
	bp=&numint_ob;
	cout << bp->getval() << endl;
	bp=&sqrint_ob;
	cout << bp->getval() << endl;
	int i;
cin >> i;
}
