#include <iostream>
using namespace std;

template <typename T>
class A
{
   public:
      A() {cout << "constructor" << endl;};
      ~A() {cout << "destructor" << endl;};
      void func1() {};
      void func2() {};
};

 //explicit instantiation that generates class A<double> 
 template class A<double>;   
 //explicit instantiation that generates class A<float> 
 template class  A<char> ;   
  
int main()
{
   A<double> ob1;
   return 0;
}
