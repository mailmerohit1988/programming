#include <iostream>
using namespace std;

template<class T>
class MyComplex{

  public:
   T real;
   T imaginary;

   MyComplex(T r,T i){real = r; imaginary = i; }// constructor
   template<class X> MyComplex(const MyComplex<X> &); // Copy constructor
};

template<typename T>
template<class X> MyComplex<T>::MyComplex(const MyComplex<X> &C){
    cout << "inside copy constructor" << endl;
    this->real = C.real;
    this->imaginary = C.imaginary;
} 
       
int main()
{
   MyComplex<char> ob1('c','d');
   MyComplex<int> ob2(ob1);
}
   


