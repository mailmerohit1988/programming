//a template class that inherits from another template class 

#include <iostream>
using namespace std;

template <class T> class num {
protected:
	T val;
	public:
	num(T x){val=x;}
	virtual T getval() {return val;}
};

template <class T> class sqrnum:public num<T> {
	T x;
	public:
	sqrnum(T x):num<T>(x){}
	T getval() {return num<T>::val*num<T>::val;}
};

int main()
{
	num<int> *bp,numint_ob(2);
	sqrnum<int> *dp,sqrint_ob(3);
	sqrnum<double> ob(3.4);
//	  bp=&ob;
	bp=&numint_ob;
	cout << bp->getval() << endl;
	bp=&sqrint_ob;
	cout << bp->getval() << endl;
}
