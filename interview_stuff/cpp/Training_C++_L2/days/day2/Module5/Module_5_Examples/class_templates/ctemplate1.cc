#include "ctemplate1.h"
#include<iostream>
#include<cstdlib>
#include<cstring>
using namespace std;



int main()
{

	int choice=0;
	Stack<int> st(5); // stack of int
	Stack<char> st2(3);// stack of char 
        try
        {
            st.push(1); st.push(2);
            st.push(3); st.push(4);
            st.push(5);
            cout << "Popping from Stack<int>" << endl;
            cout << st.pop() << endl;
            cout << st.pop() << endl;
            cout << st.pop() << endl;
            cout << st.pop() << endl;
            cout << st.pop() << endl;
			
			st2.push('a');st2.push('b');
			st2.push('c');
			cout << "Popping from Stack<char>" << endl;
			cout << st2.pop() << endl;
            cout << st2.pop() << endl;
            cout << st2.pop() << endl;
			
        }
        catch(StackException& ex)
        {
             cout << ex.extext << ":" << ex.exno << endl;
        }

}



