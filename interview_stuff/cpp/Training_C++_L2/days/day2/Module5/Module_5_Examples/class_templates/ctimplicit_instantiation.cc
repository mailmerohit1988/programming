template <typename T>

class A
{
   public:
      A() {};
      ~A() {};
      void func1() {};
      void func2() {};
};

int main()
{
   A<double> ob1;   //implicit instantiation that generates class A<double> 
   A<float> ob2;    //implicit instantiation that generates class A<float> 
   return 0;
}
