//a template class that inherits from another template class 

#include <iostream>
using namespace std;

template <class T> class num {
	protected:
	T val;
	public:
	num(T x){val=x;}
	virtual T getval() {return val;}
};

template <class T,class T1> class
 sqrnum:public num<T1> {
	protected:
		T y;
	public:
	sqrnum(T a,T1 b):num<T1>(b){y=a;}
	 T1 getval() {//return y*y;
	  return num<T1>::val * num<T1>::val;
}
};

int main()
{
	num<double> *bp,numdb_ob(2);
	sqrnum<int,double> *dp,sqrint_ob(3,3.5);
	
	bp=&numdb_ob;
	cout << bp->getval() << endl;
	bp=&sqrint_ob;
	cout << bp->getval() << endl;
}
