template <typename T>

class A
{
   public:
      A() {};
      ~A() {};
      void func1() {};
      void func2() {};
};

int main()
{
   A<double>*  ob1;   //no instantiation becasue no object created 
   A<float>* ob2;    //no instantiation
   return 0;
}
