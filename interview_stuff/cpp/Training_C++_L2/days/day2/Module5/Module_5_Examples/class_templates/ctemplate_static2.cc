#include <iostream> 
using namespace std;

template <class Type>
class A{
public :
   static Type  st;
};

template<class Type> Type  A<Type>::st = 0;
template<> float A<float>::st = 10;
int main()
{
   A<float> ob;
   cout << "ob.st = " << ob.st << endl;
   return 0;
}
