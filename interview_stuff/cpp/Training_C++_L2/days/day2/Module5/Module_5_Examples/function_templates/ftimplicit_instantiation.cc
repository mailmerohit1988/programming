#include <iostream>
using namespace std;
#include "function_template2.h"

int main()
{
	int i=10,j=20; 
	double x=10.1,y=23.3;
	char a='x',b='z';
	cout << "Initial i,j " << i << " " << j << endl;
	cout << "Initial x,y " << x << " " << y << endl;
	cout << "Initial a,b " << a << " " << b << endl;

	swapargs(i,j);
	swapargs(x,y);
	swapargs(a,b);
	
	
	cout<< "Swapped i,j " << i << " " << j << endl;
	cout <<"Swapped x,y " << x << " " << y << endl;
	cout <<"Swapped a,b " << a << " " << b << endl;

	return 0;
}
