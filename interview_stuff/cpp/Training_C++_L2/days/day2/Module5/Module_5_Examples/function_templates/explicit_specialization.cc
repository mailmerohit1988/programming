#include <iostream>
#include <cstring>
using namespace std;
template<class T>
T min3(T arg1, T arg2, T arg3)
{
    T min;

    if ((arg1 < arg2) && (arg1 < arg3))
        min = arg1;
    else if ((arg2 < arg1) && (arg2 < arg3))
        min = arg2;
    else
        min = arg3;
    return min;
}
// specialized version of min3 for strings
//template<>
const char* min3(const char* arg1, const char* arg2, const char* arg3)
{
    const char* min;

    int result1 = strcmp(arg1, arg2);
    int result2 = strcmp(arg1, arg3);
    int result3 = strcmp(arg2, arg1);
    int result4 = strcmp(arg2, arg3);
    if ((result1 < 0) && (result2 < 0))
        min = arg1;
    else if ((result3 < 0) && (result4 < 0))
        min = arg2;
    else
        min = arg3;
    return min;
}
int main()
{
    std::cout << min3(10, 20, 30) << std::endl;
    std::cout << min3(100.60, 10.872, 5.897) << std::endl;
    std::cout << min3('C', 'A', 'Z') << std::endl;
	 const char *s1="Smith";
	 const char *s2="Anderson";
	 const char *s3="White";
	 const char *s=min3(s1,s2,s3);
       std::cout << s
              << std::endl;
	 return 0;
}

