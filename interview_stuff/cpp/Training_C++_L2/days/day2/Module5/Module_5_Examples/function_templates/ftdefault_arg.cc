#include <iostream>
using namespace std;

const int TABWIDTH=8;
template <class X> void tabout(X data,int tab=0)
{
	for(;tab;tab--)
		for(int i=0;i<TABWIDTH;i++) cout << ' ';
	cout << data << endl;
}

int main()
{

	tabout("This is a test");
	tabout(100,1);
	tabout('X',2);
	tabout(10.0/3,3);
	return 0;
}
