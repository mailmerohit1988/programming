#include <iostream>
using namespace std;

template<class T,class U>
void func(T a,U b)
{
	cout << "func(T a,U b)" << endl;
}

template<class T,class U>
void func(T *a,U *b)
{
	cout << "func(T *a,U *b)" << endl;
}
template<class T,class U>
void func(T *a,U b)
{
	cout << "func(T *a,U b)" << endl;
	
}
template<class T>
void func(T a, int b)
{
	cout << "func(T ,int)" << endl;
}

template<class T>
void func(T* a, int b)
{
	cout << "func(T* ,int)" << endl;
}
int main()
{

	int i=10;
	char c='A';
	func(i,c);
	func(&i,c);
	func(&i,&c);
	func(c,i);
func(&c,i);
}

