#include <iostream>
#include "function_template1.h"

int main()
{
    std::cout << min3(10, 20, 30) << std::endl;
    std::cout << min3(100.60, 10.872, 5.897) << std::endl;
    std::cout << min3('C', 'A', 'Z') << std::endl;
    return 0;
}

