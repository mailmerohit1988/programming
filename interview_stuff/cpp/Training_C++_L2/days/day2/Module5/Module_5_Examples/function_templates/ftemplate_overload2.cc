#include <iostream>
using namespace std;

template <class X> void f(X a,int b)
{
	cout << "Inside f(X a,int b)\n";
}
template <class Y> void f(Y b)
{
	cout << "Inside f(Y b)\n";
}

template <class X,class Y> void f(X a,Y b)
{
	cout << "Inside f(X a,Y b)\n";
}
template <class X> void f(X a,X b)
{
	cout << "Inside f(X a,X b)\n";
}

template<> void f(int a,int b)
{
	cout << "Inside f(int,int)" << endl;
}

template<> void f(int a,int b, float c)
{
	cout << "Inside f(int,int,float)" << endl;
} 


int main()
{
f(10); //will match f(Y)
f(10,20.23); // will match f(X a,Y b)
f(10.0,20);  // will match f(X a,int b)
f(10,20);//error because of ambiguity
f(10.0,20.0); // will match f(X a,X b)
f(10,20,30);
return 0;
}
