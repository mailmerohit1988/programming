#include <iostream>
using namespace std;

template<typename type1, typename type2>
void display(type1 x, type2 y)
{
    cout << "x= "<< x << "y= " << y << endl;
}
/*
template<typename type1>
void display(type1 x, type1 y)
{
    cout << "Two x= "<< x << "y= " << y << endl;
}
*/
int main()
{
    int a = 25, b = 35;
    char ch1 = 'a', ch2 = 'b';

    display(a,ch1);
    display(ch1,ch2);
    display(a,b);

}

