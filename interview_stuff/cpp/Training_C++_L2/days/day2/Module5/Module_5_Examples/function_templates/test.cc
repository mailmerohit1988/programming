#include <iostream>
#include <string>
#include <typeinfo>

template <typename T>
bool max(T a, T b) {
 std::cout << "typeid(a) = " << typeid(a).name() << std::endl;
 return a<b?b:a;
}

int main()
{
 std::string s;
 std:: cout << ::max("apple","peach");
 std:: cout << ::max("apple","orange");
 std:: cout << ::max("apple",s); 
return 0;
}

