#include <iostream>
using namespace std;

template<class T> // Overload (a)
void f(T)
{
	cout << "in base template f(T) - (a)" << endl;
}
template<class T> // overload (b)
void f(int, T, double)
{
	cout << "in base template f(int, T, double) - (b)" << endl;
}


template<class T> // overload (c)
void f(T*)
{
	cout << "in base template f(T*) - (c)" << endl;
}

template<> void f<int>(int) // specialization (d)
{
	cout << "in specialized template f(T) - (d)" << endl;
}
void f(double) // plain function (e)
{
	cout << "in plain function f(double) - (e)" << endl;
}

void f(int) // plain function (fe)
{
	cout << "in plain function f(int) - (e)" << endl;
}
int main()
{
    bool b;
    int i;
    double d;
 
    f(b) ; 
    f(i,42,d) ; 
    f(&i);
    f(i);
    f(d);
}
