#include <iostream>
//using namespace std;
class Date {
	int mo;
	int da;
	int yr;
public:
	Date(int m=0,int d=0,int y=0)
{
	mo=m;
	da=d;
	yr=y;
}
void display()const
{
	std::cout <<mo<<"/"<<da<<"/"<<yr<<std::endl;
}

friend bool DateComp(Date,Date);
friend bool operator<(const Date& ,const Date&);
};

bool operator<(const Date& a,const Date& b)
{
		return a.yr < b.yr;
}
std::ostream& operator<<(std::ostream& os,const Date& d)
	{
		d.display();
		return os;
	}
bool DateComp(Date a, Date b)
{
	return a.yr > b.yr;
}
template<class T>
void swap(T &a,T &b)
{
	T temp(a);
	a=b;
	b=temp;
}
template<class T,class U>
void bub( T a[],int n,U comp)
{
	int i,j;
	for(i=0;i<n-1;i++)
		for(j=0;j<n-1;j++)
			if(comp(a[j+1],a[j]))
				swap(a[j+1],a[j]);
}
template<class T>
bool less(T a,T b)
{
	return a<b;
}
template<class T>
bool greater(T a,T b)
{
	return a > b;
}

class c1 {
public:
	bool operator()(int a,int b)
	{
		return a<b;
	}
};
/*bool less(int a,int b)
{
	return a<b;
}
bool greater(int a,int b)
{
	return a > b;
}*/




int main()
{
	int arr[]={3,1,2,5,0};
	typedef bool (*fp)(int,int);
	fp fptr=less;//less<int> gets generated 
	c1 ob;
	bub(arr,5,fptr);//bub(int[],int,bool (*)(int,int)) generated and invoked
	int i;
	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";
		
	bub(arr,5,greater<int>);// bub(int[],int,bool (*)(int,int)) is invoked.greater<int> gets generated and its address is passed

	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";

	bub(arr,5,ob);// bub (int[],int , c1) is generated and invoked

	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";

	Date arrd[]={Date(1,1,90),Date(1,1,89),
	Date(1,2,2000)};
		bub(arrd,3,less<Date>); //bub(Date [], int, bool (*)(Date,Date)) is generated and invoked.less<Date> is generated and its address is passed
	for(int i=0;i<3;i++)
		std::cout << arrd[i] << " ";
		
		
}
