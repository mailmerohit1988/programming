#include <iostream>
#include<deque>
using namespace std;
class Date {
	int mo;
	int da;
	int yr;
public:
	Date(int m=0,int d=0,int y=0)
{
	mo=m;
	da=d;
	yr=y;
}
void display()const
{
	std::cout <<mo<<"/"<<da<<"/"<<yr<<std::endl;
}
friend bool DateComp(const Date&,const Date&);
friend bool operator < (const Date& , const Date&);
};


std::ostream& operator<<(std::ostream& os,const Date& d)
{
		d.display();
		return os;
}
bool DateComp(const Date& a, const Date& b)
{
	return a.yr < b.yr;
}
 bool operator < (const Date& a, const Date& b)
{
	return a.yr < b.yr;
}
void Swap(int & a, int & b) // re-write as a template
{
	int temp =a;
	a=b;
	b=temp;
}
void bub( int* beg , int* end ,bool (*comp)( const int&,const int&)) //re-write as a template
{
	int* init(beg);
	
	int* last(end);
	
	--end;
	
	
	while(init != last)
	{
		
		int* beg1(beg);
		
		while(beg1 != end)
		{
			int* temp=beg1;
			++beg1;
			
		 if( comp(*beg1,*temp))
		{			
			
		   	  Swap(*beg1,*temp);
		}
	
		}
		++init;
	
	}
		
	
}

bool Greater(const int &a ,const int& b)// re-write as a template
{
   return a<b;
}



int main()
{
int i;
	int arr[]={3,1,2,5,0};
	
	bub(&arr[0],&arr[5],Greater);
	
	for(i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";

        //The following will not compile since the buble sort 
        // algorith is written only to work for int type 
		
/**	Date arrd[]={Date(1,1,90),Date(1,1,89),Date(1,2,2000)};
	
		bub(&arrd[0],&arrd[3],Greater<Date>);
		
	for( i=0;i<3;i++)
		std::cout << arrd[i] << " ";
	std::cout << endl;

	
	
		bub(&arrd[0],&arrd[3],DateComp);
		
	for( i=0;i<3;i++)
		std::cout << arrd[i] << " "; */
		
	
		
	
}
