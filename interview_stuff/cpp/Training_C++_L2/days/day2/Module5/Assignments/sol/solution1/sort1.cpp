#include <iostream>
#include<deque>
using namespace std;
class Date {
	int mo;
	int da;
	int yr;
public:
	Date(int m=0,int d=0,int y=0)
{
	mo=m;
	da=d;
	yr=y;
}
void display()const
{
	std::cout <<mo<<"/"<<da<<"/"<<yr<<std::endl;
}
friend bool DateComp(const Date&,const Date&);
friend bool operator < (const Date& , const Date&);
};


std::ostream& operator<<(std::ostream& os,const Date& d)
{
		d.display();
		return os;
}
bool DateComp(const Date& a, const Date& b)
{
	return a.yr < b.yr;
}
 bool operator < (const Date& a, const Date& b)
{
	return a.yr < b.yr;
}
template<class T>
void Swap(T& a,T& b)
{
	T temp(a);
	a=b;
	b=temp;
}
template<class T,class U>
void bub( T beg , T end , U comp)
{
	T init(beg);
	T last(end);
	--end;
	
	while(init != last)
	{
		T  beg1(beg);
		while(beg1 != end)
		{
			T temp=beg1;
			++beg1;
		 if( comp(*beg1,*temp))
		   	  Swap(*beg1,*temp);
		}
		++init;
	}
		
	
}

template <class T>
bool Greater (const T&a ,const T& b)
{
		return a<b;
}


int main()
{
int i;
	int arr[]={3,1,2,5,0};
	
	bub(&arr[0],&arr[5],Greater<int>);
	
	for(i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";
		
	Date arrd[]={Date(1,1,90),Date(1,1,89),Date(1,2,2000)};
	
		bub(&arrd[0],&arrd[3],Greater<Date>);
		
	for( i=0;i<3;i++)
		std::cout << arrd[i] << " ";
	std::cout << endl;

	
	
		bub(&arrd[0],&arrd[3],DateComp);
		
	for( i=0;i<3;i++)
		std::cout << arrd[i] << " ";
		
	
		
	
}
