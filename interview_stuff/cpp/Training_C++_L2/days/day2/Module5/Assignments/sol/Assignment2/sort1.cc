#include <iostream>
//using namespace std;
class Date {
	int mo;
	int da;
	int yr;
public:
	Date(int m=0,int d=0,int y=0)
{
	mo=m;
	da=d;
	yr=y;
}
void display()const
{
	std::cout <<mo<<"/"<<da<<"/"<<yr<<std::endl;
}

friend bool DateComp(Date,Date);
friend bool operator<(const Date& ,const Date&);
};

bool operator<(const Date& a,const Date& b)
{
		return a.yr < b.yr;
}
std::ostream& operator<<(std::ostream& os,const Date& d)
	{
		d.display();
		return os;
	}
bool DateComp(Date a, Date b)
{
	return a.yr > b.yr;
}
template<class T>
void swap(T &a,T &b)
{
	T temp(a);
	a=b;
	b=temp;
}
template<class T,class U>
void bub( T a[],int n,U comp)
{
	int i,j;
	for(i=0;i<n-1;i++)
		for(j=0;j<n-1;j++)
			if(comp(a[j+1],a[j]))
				swap(a[j+1],a[j]);
}
template<class T>
bool less(T a,T b)
{
	return a<b;
}
template<class T>
bool greater(T a,T b)
{
	return a > b;
}







int main()
{
	int arr[]={3,1,2,5,0};
	typedef bool (*fp)(int,int);
	fp fptr=less;//function gets generated 
	bub(arr,5,fptr);// function address is passed and is invoked
	int i;
	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";
		
        // Use of template function
	bub(arr,5,greater<int>);//function gets generated and its address is passed.It is also invoked
	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";

        // use of template class
/**	c1 ob;// template class in generated
	bub(arr,5,ob);//template class is passed and function call operator is invoked 
	for( i=0;i<5;i++)
		std::cout << arr[i] << " ";
		std::cout << "\n";

	Date arrd[]={Date(1,1,90),
                     Date(1,1,89),
	             Date(1,2,2000)};

        bub(arrd,3,less<Date>); //function is generated and its address is passed and invoked 
	for(int i=0;i<3;i++)
		std::cout << arrd[i] << " "; **/
		
}
