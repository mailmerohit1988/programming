#include <iostream>
#include <cstring>
#include <stdexcept>
using namespace std;

class MyException : public logic_error {
	
	public :
		int errNo;
		MyException(int err,char* s) : logic_error(s) {
			errNo=err;
		}
		const char * what() const throw() {
           cout << errNo << endl;
           return logic_error::what();
        }
};

class String {
    char *string;
public:
	String()
	{
		string=new char[1];
		*string = '\0';
	}
    String(const char* txt )
    {	
		int len = strlen(txt);
		string = new char[len+1];
		strcpy(string,txt);
		
    }
   void display()const
    {
		if(*string)
			cout << string << endl;
    }
    
	String(const String& s);
	String& operator=(const String& t);
	char & operator [](int i);
	const char& operator [](int i) const;

	~String(){cout << "destructor" << endl;delete [] string;}
	friend std::ostream& operator <<(std::ostream& os,const String& s);
};
String::String(const String& s)
{
	int len = strlen(s.string);
	string = new char[len+1];
	strcpy(string,s.string);		
}

String& String::operator=(const String& t)
{
    if (&t != this)
    {
		delete string;
		int len = strlen(t.string);
		string = new char[len+1];
		strcpy(string,t.string);		
	}
		return *this;
}

char & String::operator [](int i)
{
	static char ch='\0';
	if(i>=0 && i < strlen(string))
		return string[i];
	else{
		throw MyException(-2,"Invalid index");
	}
}
const char&  String::operator [](int i)const
{
	char ch='\0';
	if(i>=0 && i < strlen(string))
		return string[i];
	else
		throw MyException(-2,"Invalid index");
}

std::ostream& operator <<(std::ostream& os,const String& s)
{
	os << s.string;
	return os;
}

int main()
{
	try
	{    	
		String s1("Spencer");
	   	cout << s1 <<endl;
		s1[2]='A';
		cout << s1 << endl;
		
		try
		{
			char ch = s1[2];
			cout << " ch = " << ch << endl;
			s1[9]='B';
			cout << s1 << endl;
		}
		catch(exception& ex)
		{
			cout << ex.what() << endl;
		}
		
		s1[2]= 'B';
		cout << s1 << endl;
		char ch = s1[10];
		cout << " ch = " << ch << endl;
	}
	catch(exception& ex)
	{
		cout << ex.what() << endl;
	}
	cout << "end of program" << endl;		
	return 0;
}


