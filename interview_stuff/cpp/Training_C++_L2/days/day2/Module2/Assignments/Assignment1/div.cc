#include<iostream>
using namespace std;
float calculate(int a,int b)
{
 	float f=0;
 	if (b==0) throw 0;
	f=a/b; // control reaches here if and only if the divisor (b) does not equal 0
	return f;
}
int  main()
{ 
	float res; int x,y;

	do {	
		cout << "Enter two nos:";
		cin >> x >> y; // programmer has no control over the values entered by the user
		try{
			res=calculate(x,y);
			cout << "Result is " << res << endl; 
		}catch (int) {
			cout << "Can not divide by zero" << endl;
		}
	}while(y==0);
 }
