#include <iostream>
using namespace std;

float calculate(int a , int b) throw (int)
{
   float f = 0;
   if(b==0)
      throw 1.5;  // will result in unexpected being called
   f = a/b; // this is done only when the denominator != 0 
   return f;
}

void custom_unexpected()
{
   cout << " custom unexpected called" << endl;
}
void custom_terminate()
{
   cout << " custom terminate called" << endl;
}

int main()
{

    float res;
    int x,y;

    set_unexpected(custom_unexpected);
    set_terminate(custom_terminate);
    try
    {
       cout << "Enter 2 nos:";
       cin >> x >> y;
       res = calculate(x,y);
       // control reaches here only if successful
       cout << "Result is : " << res << endl;; 
    }
    catch(int i)
    { 
       cout << "Caught Exception: Divide by Zero cannot be done " << endl; 
    }
    cout << "End of Program" << endl;
}
