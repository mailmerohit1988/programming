#include <iostream>
#include <cstring>
using namespace std;



class Distance {
int feet;
float inches;
public:

Distance(int f,float in)
{
	
	cout << "Distance 2 arg constructor\n";
	setdata(f,in);
}
void setdata(int f,float in)
{
	feet=f;
	if(in > 12)
	{
		int f=in/12;
		feet+=f;
		inches = (in/12 -f )*12;
	}
	else
		inches=in;
}
~Distance(){cout << "Distance destructor" << endl; } 
};

class Box {
Distance height;
Distance width;
Distance depth;

public:
	Box();
        void display();
	~Box(){cout << "Box destructor" << endl;throw 200; } 
  /* When an exception is thrown from a try block, destructors for all automatic objects constructed within the try block will be called. Stack unwinding is guaranteed to have happened when the exception is being handled. If a destructor invoked as a result of stack unwinding throws an exception, terminate is called.
 */
};

// demonstrating what happens when an exception is thrown from a 
// constructor
Box::Box():height(0,0),width(0,0),depth(0,0) // initializer list
{
  cout << "Box default constructor\n"; 
  //notice the destructor of the member objects called 
  //before exception is handled
}

void Box::display()
{
    cout << "display method of Box Class" << endl;
}

int main()
{
       try{
        Box b;
        throw 50;
       }
       catch(int &i)
       {
           cout << "exception thrown from the Box constructor is caught" 
                << endl
                << "value of exception = " 
                << i 
                << endl;
       }
       return 0;
}
