#include <iostream>
using namespace std;

float calculate(int a , int b)
{
   float f = 0;
  try{
   try{
     if(b==0)
      throw b;
    f = a/b; // this is done only when the denominator != 0 
   }
   catch(int& d)
   {
      cout << "caught the int type exception in fn calculate" << endl;
      cout << "Rethrowing the exception to the caller " << endl;
      throw;   // rethrowing the same exception to the caller
   }
  }
  catch(...){
   //handle it or i rethrow
  }

   // throw;//placing the throw here will cause terminate fn to be called 
            // which will cause the program to abort
   return f; // will never be reached if b == 0 
}

int main()
{

    float res;
    int x,y;
    try
    {
       cout << "Enter 2 nos:";
       cin >> x >> y;
       res = calculate(x,y);
       // control reaches here only if successful
       cout << "Result is : " << res << endl;; 
    }
    catch(int& i)
    { 
       cout << "Caught int Exception in main: Divide by Zero cannot be done " << endl; 
       //throw;
    }
    cout << "End of Program" << endl;
}
