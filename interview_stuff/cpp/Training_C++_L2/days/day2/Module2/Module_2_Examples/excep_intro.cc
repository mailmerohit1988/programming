#include<iostream>
using namespace std;
int main()
{
 int num;
 cout << "Enter a number?";
 cin >> num;
 try  {
  if(num==0) throw num;
  else {
   double result= (float) 100/num;
   cout << "Quotient: " << result << endl;
  }
}
 catch(int& exp){
  cout << "Error! ";
  cout << "Cannot divide 100 by " << exp;
 }
 cout << endl <<"- Program Ends –" << endl;
}

