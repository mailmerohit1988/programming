#include <iostream>
using namespace std;
class B {
};
class D:public B {
};
int main()
{
	D derived;
        B base;
	try {
            throw derived; 
            throw base; // never executes
        }
	
	catch(D &d){
           cout << "caught a derived object\n";
           throw base; // not caught
        }

	catch(B &b){ // right place to put this 
            cout << "caught a base class\n";
        }

}
