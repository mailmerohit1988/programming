#include <iostream>
#include <memory>
using namespace std;

class MyClass
{
   private :
       int myData;
   public :
      MyClass(){ cout << " My Class Constructor" << endl;}
      ~MyClass() {cout << " My Class Destructor " << endl;}

      void setData( int i) { myData= i;}
      int getData( ) { return myData;} 
};


int main()
{
    auto_ptr<MyClass> ptrToMyClass(new MyClass);
    cout << " setting data (5) data in MyClass Object " << endl; 
    ptrToMyClass->setData(5);
    cout << " data in MyClass Object " << ptrToMyClass->getData()<<endl;
}

