#include<iostream>
#include<cstdlib>
#include<cstring>
using namespace std;
class StackException
{
	public:
		char extext[100];
		int exno;
	StackException(char messg[],int no)
	{
		strcpy(extext,messg);
		exno=no;
	}
};


class Stack {
	int *st;
	int top;
	int size;
public:
void push(int);
int pop();
Stack(int s);
Stack(const Stack& s);
Stack& operator=(const Stack& s);
~Stack() { delete [] st;}
};


void Stack::push(int num)
{
	if(top<=0)
		throw StackException("Stack Overflow", -1);
	else
		st[--top]=num;
}

int Stack::pop()
{
	if(top>=size)
		throw StackException("Stack Underflow", -2);
	else
		return st[top++];
}
Stack::Stack(int s)
{
	if(s<=0)
	   s=1;	
	size=s;	
	st=new int[size];
	top=size;
}
Stack::Stack(const Stack& s)
{
	size=s.size;
	st=new int[size];
	int i;
	for(i=0;i<size;++i)
		st[i]=s.st[i];
	top=s.top;
}
Stack& Stack::operator=(const Stack& s)
{
     if(&s != this)
     {
	delete [] st;	
	size=s.size;
	st=new int[size];
	int i;
	for(i=0;i<size;++i)
		st[i]=s.st[i];
	top=s.top;
       }
		return *this;
}

int main()
{

	int choice=0;
	Stack st(5);
	do
	{
		cout << "1 . Push " << endl;
		cout << "2. Pop " << endl;
		cout << "3. End " << endl;
		cout <<"     Enter Your Choice : " ;
		cin >> choice;
		try
		{
			int x;
			switch(choice)
			{
	
				case 1: 
					cout <<endl << " enter value: " ;
					cin >> x;
					st.push(x);
					break;

			case 2: 
				cout << endl << "**Value popped is :** " << st.pop()<< endl;
				break;
			case 3 : 
				exit(0);
				
			default: 
				cout << endl << " Invalid choice " << endl;
			}
		}
		catch (StackException &s)
		{
			cout << endl << "*****" << s.extext << "*****" << endl;
		}
	}while(true);
}



