#include <iostream>
using namespace std;
class B {
public:
  virtual void f() throw (int,double) {}
};
class D:public B {
 public :
  virtual void f() throw (int) {}; // throws a subset of base member
  //virtual void f() throw (int,double) {};//throw spec is not part of mangled name  
//  the following will be compiler errors 
//  virtual void f() {};// can throw any exception 
//  virtual void f()throw (int, double, bool) {}; //throws more than base member
// virtual void f() throw (){}; // throws no exception
};
int main()
{

}
