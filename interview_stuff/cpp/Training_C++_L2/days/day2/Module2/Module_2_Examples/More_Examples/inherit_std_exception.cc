#include <iostream>
//#include <cstring>
#include <stdexcept>

using namespace std;

class MyException:public logic_error {
public:
	
	int erno;
	MyException(char *s,int e):logic_error(s)
	{
	erno=e;
	
	}
	const char * what() const throw()
	{
		cout << erno << endl;
		return logic_error::what();
	}
		
};
void func()
{
	int i;
		try
		{
		cout << "Enter a positive number : ";
		cin >>i;
		if(i<0)
		{
			MyException ex("Not Positive",i);
			throw ex;
		}
		}
	
	catch(exception& e)
	{
		cout << e.what() << endl;
		
	}
}

int main()
{
	int i;

	func();
	
}
