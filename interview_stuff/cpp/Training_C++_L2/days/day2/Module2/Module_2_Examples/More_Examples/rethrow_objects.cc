#include <iostream>
#include <cstring>
using namespace std;

class MyException {
public:
	char str_what[80];
	int what;
	MyException(){*str_what=0;what=0;}
	MyException(char *s,int e)
	{strcpy(str_what,s);
	what =e;
	cout << "2 arg  constructor\n";
	}
	MyException(const MyException& ob)
	{
		*this=ob;
		cout << "copy constructor\n";
	}
	MyException& operator=(const MyException& ob)
	{
		strcpy(str_what,ob.str_what);
		what = ob.what;
		return *this;
	}
	~MyException()
	{
		cout << "destructor\n";
	}
};
void func()
{
	int i;
		try
		{
		cout << "Enter a positive number : ";
		cin >>i;
		if(i<0)
		{
			 MyException ex("Not Positive",i);
			throw ex;
		
		}
		}
	
	catch(MyException& e)
	{
		cout << e.str_what << endl;
		cout <<e.what << endl;
		throw;
	}
}
	

int main()
{
	int i;

	try {
		func();
	}
	catch(MyException& e)
	{
		cout << e.str_what << endl;
		cout <<e.what << endl;
	}
}
