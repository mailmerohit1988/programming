#include <iostream>
using namespace std;

float calculate(int a , int b)
{
   float f = 0;
   try{
     if(b==0)
      throw b;
    f = a/b; // this is done only when the denominator != 0 
   }
   catch(double& d)
   {
      cout << "caught the double type exception" << endl;
   }
   return f; // 0 returned if the exception is caught here
}

int main()
{

    float res;
    int x,y;
    try
    {
       cout << "Enter 2 nos:";
       cin >> x >> y;
       res = calculate(x,y);
       // control reaches here only if successful
       cout << "Result is : " << res << endl;; 
    }
    catch(int i)
    { 
       cout << "Caught int Exception: Divide by Zero cannot be done " << endl; 
    }
    cout << "End of Program" << endl;
}
