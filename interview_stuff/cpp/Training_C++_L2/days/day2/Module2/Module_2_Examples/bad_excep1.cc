#include <iostream>
using namespace std;

void custom_unexpected();
float calculate(int a , int b) throw (int)
{
   set_unexpected(custom_unexpected);
   float f = 0;
   if(b==0)
      throw 1.5;  // will result in unexpected being called
   f = a/b; // this is done only when the denominator != 0 
   return f;
}

void custom_unexpected()
{
   cout << " custom unexpected called" << endl;
   throw ;
}

int main()
{

    float res;
    int x,y;

    try
    {
       cout << "Enter 2 nos:";
       cin >> x >> y;
       res = calculate(x,y);
       // control reaches here only if successful
       cout << "Result is : " << res << endl;; 
    }
    catch(int i)
    { 
       cout << "Caught Exception: Divide by Zero cannot be done " << endl; 
    }
    catch(bad_exception &)
    { 
       cout << "Caught bad_exception" << endl; 
    }
    cout << "End of Program" << endl;
}
