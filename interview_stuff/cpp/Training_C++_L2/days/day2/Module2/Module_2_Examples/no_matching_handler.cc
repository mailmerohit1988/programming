#include <iostream>
using namespace std;

int main()
{
	cout << "Start \n";
	try {
	  cout << "inside try block\n";
          throw 100;
	  cout << "this will not execute\n";
	}
        catch(double d )
        {
             cout << " in catch(double): value is:" << d << endl;
        }
}
