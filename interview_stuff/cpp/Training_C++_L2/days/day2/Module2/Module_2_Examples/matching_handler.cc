#include <iostream>
using namespace std;
int main()
	{
		cout << "Start \n";
		try {
	
		cout << "inside try block\n";
	
		throw 100; // should have an exact match in the catch 
                          // or should have the catch(...)
		cout << "this will not execute\n";
}

// compiler error:1 or more catch blocks must immediately follow a try block
//cout << "before catch\n" 

catch(double d)
{
   cout << "Caught an exception in catch(double)---value is :" << d << endl;
}
catch(int i)
{
   cout << "Caught an exception in catch(int)---value is :" << i << endl;
}
catch(...)
{
   cout << "Caught an exception in catch(...)-value is :cannot print" << endl;

}

// program continues here after last statement in try or executing the catch  
cout << "end\n"; 

}
