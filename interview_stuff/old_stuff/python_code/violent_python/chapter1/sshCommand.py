import pexpect

PROMPT = ['# ', '>>> ', '> ', '\$ ']

def send_command(child, cmd):
	child.sendline(cmd)
	child.expect(PROMPT)
	print child.before
	
def connect(user, host, password):
	ssh_newkey = 'Are you sure you want to continue connecting'
	connStr = 'ssh ' + user + '@' + host
	print "spawning : "+connStr
	child = pexpect.spawn(connStr)
	ret = child.expect([pexpect.TIMEOUT, ssh_newkey, \
		'[P|p]assword:'])
	print "spawned"
	print ret
	if ret == 0:
		print '[-] Error Connecting'
		return
	if ret == 1:
		child.sendline('yes')
		print "expecting password"
		ret = child.expect([pexpect.TIMEOUT, \
			'root@localhost\'s password:'])
		print ret
		if ret == 0:
			print '[-] Error Connecting during password'
			return
	child.sendline(password)
	child.expect(PROMPT)
	return child

def main():
	host = '10.227.65.55'
	user = 'admin'	
	password = 'netapp1!'
	child = connect(user, host, password)
	send_command(child, 'vol status')

if __name__ == '__main__':
	main()
