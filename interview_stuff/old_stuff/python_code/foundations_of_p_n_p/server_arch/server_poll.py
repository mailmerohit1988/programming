#!/usr/bin/env python
# Foundations of Python Network Programming - Chapter 7 - server_poll.py
# An event-driven approach to serving several clients with poll().

# Blocking socket means that if you do a read from the socket and no data is available then that call
# will wait there forever until data reaches that socket

# Similarly for write if the buffers are full then in case of blocking the socket will wait 
# there until all data has been sent whereas in case of non-blocking the send call will return
# the number of bytes that has been sent

import launcelot
import select

listen_sock = launcelot.setup()
sockets = { listen_sock.fileno(): listen_sock }
print '*********** Printing sockets ***********' 
print sockets
print select.POLLIN
print '*********** Printing sockets done ***********' 
requests = {}
responses = {}
poll = select.poll()
poll.register(listen_sock, select.POLLIN)

while True:
	print '*********** Entering while loop ***********' 
	for fd, event in poll.poll():
		print '*********** Entering for loop ***********' 
		print fd
		print event
		print sockets[fd]
		print requests
		print responses
		sock = sockets[fd]
  
		# Removed closed sockets from our list.
		if event & (select.POLLHUP | select.POLLERR | select.POLLNVAL):
			poll.unregister(fd)
			del sockets[fd]
			requests.pop(sock, None)
			responses.pop(sock, None)

		# Accept connections from new sockets.
		elif sock is listen_sock:
			print '*********** New socket ***********' 
			# Here the remote socket says read from me , so the event=1
			newsock, sockname = sock.accept()
			print newsock
			print sockname
			newsock.setblocking(False)
			fd = newsock.fileno()
			print fd
			sockets[fd] = newsock
			print sockets[fd]
			poll.register(fd, select.POLLIN)
			requests[newsock] = ''

		# Collect incoming data until it forms a question.
		elif event & select.POLLIN:
			print '*********** event and select.POLLIN ***********' 
			print event
			print select.POLLIN
			# Here the remote socket says read from me , so the event=1
			data = sock.recv(4096)
			print data
			if not data:      # end-of-file
				print 'no data..closing socket'
				sock.close()  # makes POLLNVAL happen next time
				continue
			requests[sock] += data
			print '*********** printing requests after it has been added***********' 
			print requests
			if '?' in requests[sock]:
				question = requests.pop(sock)
				print '*********** printing requests after it has been removed***********' 
				print requests
				answer = dict(launcelot.qa)[question]
				poll.modify(sock, select.POLLOUT)
				responses[sock] = answer
				print '*********** printing responses after it has been added***********' 
				print responses


		# Send out pieces of each reply until they are all sent. 
		elif event & select.POLLOUT: 
			print '*********** event and select.POLLOUT ***********' 
			# select.POLLOUT means that data can be send to the client, in this case event=4
			response = responses.pop(sock) 
			n = sock.send(response) 
			if n < len(response): 
				responses[sock] = response[n:] 
			else: 
				poll.modify(sock, select.POLLIN) 
				requests[sock] = '' 
