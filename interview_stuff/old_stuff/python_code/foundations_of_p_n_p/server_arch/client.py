#!/usr/bin/env python 
# Foundations of Python Network Programming - Chapter 7 - client.py 
# Simple Launcelot client that asks three questions then disconnects. 
import socket, sys, launcelot 
import time

def client(hostname, port): 
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
	print 'trying to connect'
	s.connect((hostname, port)) 
	# when client connects , server gets to know that some one has connected to it so it has to read from client socket files periodically
	#print 'printing client socket'
	#print s
	s.sendall(launcelot.qa[0][0]) 
	#print 'sent'
	answer1 = launcelot.recv_until(s, '.') # answers end with '.' 
	# s.rcv and s.send simply puts and gets data from client socket file
	# when clientsock.rcv or clientsock.send is done , in this case server reads from those client socket files
	#print 'recieved reply'
	s.sendall(launcelot.qa[1][0]) 
	answer2 = launcelot.recv_until(s, '.') 
	s.sendall(launcelot.qa[2][0]) 
	answer3 = launcelot.recv_until(s, '.') 
	#print answer1 
	#print answer2 
	#print answer3 
	s.close() 

if __name__ == '__main__': 
	if not 2 <= len(sys.argv) <= 3: 
		print >>sys.stderr, 'usage: client.py hostname [port]' 
		sys.exit(2) 
	port = int(sys.argv[2]) if len(sys.argv) > 2 else launcelot.PORT 
	client(sys.argv[1], port)
