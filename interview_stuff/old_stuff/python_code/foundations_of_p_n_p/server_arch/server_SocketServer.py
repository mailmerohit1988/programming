#!/usr/bin/env python 
# Foundations of Python Network Programming - Chapter 7 - server_SocketServer.py 
# Answering Launcelot requests with a SocketServer. 

from SocketServer import ThreadingMixIn, TCPServer, BaseRequestHandler 
import launcelot, server_simple, socket 

class MyHandler(BaseRequestHandler): 
	print 'MyHandler called'
	def handle(self): 
		print 'MyHandler.handle called'
		server_simple.handle_client(self.request) 

class MyServerrrrr(ThreadingMixIn, TCPServer): 
	print 'MyServer called'
	# address_family = socket.AF_INET6 # if you need IPv6 
	allow_reuse_address = 1 
	
	
server = MyServerrrrr(('', launcelot.PORT), MyHandler) 
print server
server.serve_forever() 
