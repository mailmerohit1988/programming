#!/usr/bin/env python
# Foundations of Python Network Programming - Chapter 1 - search4.py
import socket
sock = socket.socket()
sock.connect(('maps.googleapis.com', 80))
sock.sendall(
	'GET /maps/api/geocode/json?oe=utf8&sensor=true&address=Solapur+Road+Bijapur\r\n'
	'Host: maps.googleapis.com:80\r\n'
	'User-Agent: search4.py\r\n'
	'Connection: close\r\n'
	'\r\n'
)
rawreply = sock.recv(4096)
print rawreply
