#!/usr/bin/env python 
# Foundations of Python Network Programming - Chapter 8 - queuecrazy.py 
# Small application that uses several different message queues 

# I think its a UDP based communication since connect call is not being made anywhere
# in the fountain call, the server is binding and then directly calling send without any information as to
# where it is sending, which means that it is putting those info in its local file
# Also in responder , it is directly calling recv after binding without giving any info as
# to where it receiving from , which means it is trying to read from its local file

# OOPS no  i see connect which means it is TCP based communucation

## OOps but there is no accept which means it is a UDP communication

## Not sure it might be a TCP communication .. PLEASE CHECK

import random, threading, time, zmq 

zcontext = zmq.Context() 

def fountain(url): 
	"""Produces a steady stream of words.""" 
	zsock = zcontext.socket(zmq.PUSH) 
	zsock.bind(url) 
	#print "fountain(url) : server having socket : zsock binded to url : " + url
	words = [ w for w in dir(__builtins__) if w.islower() ] 
	while True: 
		print 'fountain sending word'
		zsock.send(random.choice(words)) # this word will be send only when some one calls a connect to it , so send is a blocking call
		print 'fountain sent word' 
		time.sleep(0.4) 
	print "fountain() exiting"

def responder(url, function): 
	"""Performs a string operation on each word received.""" 
	print "responder(url, function) : server having socket : zsock binded to url : " + url
	print function  # Function object is created , not just function string
	zsock = zcontext.socket(zmq.REP) 
	zsock.bind(url) 
	while True: 
		word = zsock.recv() 
		print 'responder rcvd word : ' + word
		print 'responder sending word : ' + function(word)
		zsock.send(function(word)) # send the modified word back 
	print "responder() exiting"

def processor(n, fountain_url, responder_urls): 
	"""Read words as they are produced; get them processed; print them."""
	print 'processor() called ' 
	print n
	zpullsock = zcontext.socket(zmq.PULL) # going to pull(recv) the data from the socket thats why zpullsocket
	print "connect calling"
	zpullsock.connect(fountain_url)
	print "connect called"
	zreqsock = zcontext.socket(zmq.REQ) 
	for url in responder_urls: 
		zreqsock.connect(url) 
	while True: 
		word = zpullsock.recv() 
		print "processor rcvd word :   " + word
		print "processor sndng word :   " + word
		zreqsock.send(word)  # Data will be sent to both responders alternatively
		print n, zreqsock.recv() 

def start_thread(function, *args): 
	thread = threading.Thread(target=function, args=args) 
	thread.daemon = True # so you can easily Control-C the whole program 
	thread.start() 

start_thread(fountain, 'tcp://127.0.0.1:6700') 
start_thread(responder, 'tcp://127.0.0.1:6701', str.upper) 
start_thread(responder, 'tcp://127.0.0.1:6702', str.lower) 


for n in range(1): 
	start_thread(processor, n + 1, 'tcp://127.0.0.1:6700', 
				['tcp://127.0.0.1:6701', 'tcp://127.0.0.1:6702']) 

time.sleep(30) 
