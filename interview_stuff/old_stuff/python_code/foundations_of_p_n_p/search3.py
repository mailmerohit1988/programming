#!/usr/bin/env python
# Foundations of Python Network Programming - Chapter 1 - search3.py

import httplib

try:
	import json
except ImportError: # for Python 2.5
	import simplejson as json
path = ('/maps/api/geocode/json?oe=utf8&sensor=true&address=Evershine+city+%2CVasai+%2C+Maharashtra')
# https://maps.googleapis.com/maps/api/geocode/json?oe=utf8&sensor=true&address=Evershine+city+%2CVasai+%2C+Maharashtra
connection = httplib.HTTPConnection('maps.googleapis.com')
connection.request('GET', path)
rawreply = connection.getresponse().read()
reply = json.loads(rawreply)
print 'Latitude : ' + str(reply['results'][0]['geometry']['location']['lat'])
print 'Longitude : ' + str(reply['results'][0]['geometry']['location']['lng'])
