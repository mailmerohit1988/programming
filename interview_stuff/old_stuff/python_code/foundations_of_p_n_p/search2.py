#!/usr/bin/env python
# Foundations of Python Network Programming - Chapter 1 - search2.py
import urllib, urllib2

try:
	import json
except ImportError: # for Python 2.5
	print 'exception while importing json'
	import simplejson as json

params = {'address': 'Evershine city ,Vasai , Maharashtra','sensor': 'true', 'oe': 'utf8'}

url = 'https://maps.googleapis.com/maps/api/geocode/json?' + urllib.urlencode(params)
print 'url : ' +url

rawreply = urllib2.urlopen(url).read()
print 'raw reply : '+ rawreply

reply = json.loads(rawreply)
#print reply

print 'Latitude : ' + str(reply['results'][0]['geometry']['location']['lat'])
print 'Longitude : ' + str(reply['results'][0]['geometry']['location']['lng'])
