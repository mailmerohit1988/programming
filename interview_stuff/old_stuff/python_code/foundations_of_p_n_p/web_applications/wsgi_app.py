#!/usr/bin/env python 
# Foundations of Python Network Programming - Chapter 11 - wsgi_app.py 
# A simple web application built directly against the low-level WSGI spec. 

import cgi, base64 
from wsgiref.simple_server import make_server 

def page(content, *args): 
	print 'page enter'
	print content
	print args 
	yield '<html><head><title>wsgi_app.py</title></head><body>' 
	yield content % args 
	yield '</body>' 
	print 'page() exit'

def simple_app(environ, start_response): 
	print 'simple_app is being called' # This simple_app will be called when you request for http://localhost:8000/
	print environ # environ is a hash reference 
	print start_response
	gohome = '<br><a href="/">Return to the home page..**MODIFIED**</a>' 
	q = cgi.parse_qs(environ['QUERY_STRING']) 
	print q # {'mystring': ['helloWorld']} is q when you do a get
	if environ['PATH_INFO'] == '/': 
		if environ['REQUEST_METHOD'] != 'GET' or environ['QUERY_STRING']: 
			start_response('400 Bad Request', [('Content-Type', 'text/plain')]) 
			return ['Error: the front page is not a form'] 
		start_response('200 OK', [('Content-Type', 'text/html')])  #if you comment this line then this is the error you get : A server error occurred.  Please contact the administrator.
		## start_response('400 OK', [('Content-Type', 'text/html')]) 
		## 127.0.0.1 - - [16/Jul/2014 13:27:10] "GET / HTTP/1.1" 400 156
		# I think start_response prepares some reponse object
		print 'page is being called from simple_app' 
		return page('Welcome! Enter a string: <form action="encode">' 
					'<input name="mystring"><input type="submit"></form>') 
	elif environ['PATH_INFO'] == '/encode': 
		if environ['REQUEST_METHOD'] != 'GET': 
			start_response('400 Bad Request', [('Content-Type', 'text/plain')]) 
			return ['Error: this form does not support POST parameters'] 
		if 'mystring' not in q or not q['mystring'][0]: 
			start_response('400 Bad Request', [('Content-Type', 'text/plain')]) 
			return ['Error: this form requires a "mystring" parameter'] 
		my = q['mystring'][0] 
		print my #helloWorld
		start_response('200 OK', [('Content-Type', 'text/html')]) 
		return page('<tt>%s</tt> base64 encoded is: <tt>%s</tt>' + gohome, 
					cgi.escape(repr(my)), cgi.escape(base64.b64encode(my))) #("'helloWorld'", 'aGVsbG9Xb3JsZA==')
	else: 
		start_response('404 Not Found', [('Content-Type', 'text/plain')]) 
		return ['That URL is not valid'] 

print 'Listening on localhost:8000' 
make_server('localhost', 8000, simple_app).serve_forever() # WSGI will start a server on localhost at 8000 port so when any request comes on that through HTTP , WSGI knows how to decode it
# and then it forwards that request to simple_app() to respond to that request same as how web container calls appropriate methods and then WSGI uses HTTP to talk to the web browser 
# back


# When the first time you request for http://localhost:8000/ , Below are the values 
#===================================================================================
#{'wsgi.version': (1, 0), 'WINDOWID': '41943058', 'wsgi.multiprocess': False, 'HTTP_ACCEPT_LANGUAGE': 'en-US,en;q=0.9', 'SERVER_PROTOCOL': 'HTTP/1.1', 'SERVER_SOFTWARE': 'WSGIServer/0.1 Python/2.7.3', 'DBUS_SESSION_BUS_ADDRESS': 'unix:abstract=/tmp/dbus-yqGF3E2PGf,guid=89d4544798d13eb4921d62bb53c621b8', 'REQUEST_METHOD': 'GET', 'LOGNAME': 'root', 'USER': 'root', 'PATH': '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin', 'QUERY_STRING': '', 'DM_CONTROL': '/var/run/xdmctl', 'XCURSOR_THEME': 'default', 'SSH_AGENT_PID': '3147', 'LANG': 'en_IN', 'PROFILEHOME': '', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'COLORFGBG': '0;15', 'XDG_SESSION_COOKIE': '5579209dc20a00bbc295e2c253521fbe-1405494311.906227-164659386', 'LANGUAGE': 'en_IN:en', 'SESSION_MANAGER': 'local/debian:@/tmp/.ICE-unix/3235,unix/debian:/tmp/.ICE-unix/3235', 'SHLVL': '2', 'CONTENT_TYPE': 'text/plain', 'KDE_SESSION_VERSION': '4', 'wsgi.url_scheme': 'http', 'WINDOWPATH': '7', 'SERVER_PORT': '8000', 'KDE_FULL_SESSION': 'true', 'HOME': '/root', 'KONSOLE_DBUS_SESSION': '/Sessions/1', 'PATH_INFO': '/', 'CONTENT_LENGTH': '', 'KDE_SESSION_UID': '1000', 'QT_PLUGIN_PATH': '/home/rohit4/.kde/lib/kde4/plugins/:/usr/lib/kde4/plugins/', 'HTTP_ACCEPT_CHARSET': 'utf-8,*;q=0.5', 'XDM_MANAGED': 'method=classic', 'SSH_AUTH_SOCK': '/tmp/ssh-SKd8KUiQVB7C/agent.3061', 'KONSOLE_DBUS_SERVICE': ':1.80', 'wsgi.input': <socket._fileobject object at 0xb70fc8ac>, 'HTTP_USER_AGENT': 'Mozilla/5.0 (X11; Linux i686) KHTML/4.8.4 (like Gecko) Konqueror/4.8', 'HTTP_HOST': 'localhost:8000', 'GTK2_RC_FILES': '/etc/gtk-2.0/gtkrc:/home/rohit4/.gtkrc-2.0:/home/rohit4/.kde/share/config/gtkrc-2.0', 'wsgi.multithread': True, 'HTTP_CONNECTION': 'keep-alive', 'GS_LIB': '/home/rohit4/.fonts', 'SHELL_SESSION_ID': '07cde6c4347c4000a658040e57b36574', '_': '/usr/bin/python', 'HTTP_ACCEPT': 'text/html, text/*;q=0.9, image/jpeg;q=0.9, image/png;q=0.9, image/*;q=0.9, */*;q=0.8', 'DESKTOP_SESSION': 'default', 'wsgi.file_wrapper': <class wsgiref.util.FileWrapper at 0xb711686c>, 'DISPLAY': ':0', 'SERVER_NAME': 'localhost', 'GATEWAY_INTERFACE': 'CGI/1.1', 'wsgi.run_once': False, 'OLDPWD': '/home/rohit4', 'REMOTE_ADDR': '127.0.0.1', 'SCRIPT_NAME': '', 'wsgi.errors': <open file '<stderr>', mode 'w' at 0xb75c00d0>, 'XDG_DATA_DIRS': '/usr/share:/usr/share:/usr/local/share', 'PWD': '/home/rohit4/python_code/foundations_of_p_n_p/web_applications', 'GTK_RC_FILES': '/etc/gtk/gtkrc:/home/rohit4/.gtkrc:/home/rohit4/.kde/share/config/gtkrc', 'MAIL': '/var/mail/root', 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:', 'REMOTE_HOST': 'localhost', 'HTTP_ACCEPT_ENCODING': 'gzip, deflate, x-gzip, x-deflate', 'KDE_MULTIHEAD': 'false'}

### QUERY_STRING 	=> ''
### PATH_INFO    	=> '/'
### REQUEST_METHOD 	=> GET

# When the press submit on http://localhost:8000/ , Below are the values 
#===================================================================================
#{'wsgi.version': (1, 0), 'WINDOWID': '41943058', 'wsgi.multiprocess': False, 'HTTP_REFERER': 'http://localhost:8000/', 'HTTP_ACCEPT_LANGUAGE': 'en-US,en;q=0.9', 'SERVER_PROTOCOL': 'HTTP/1.1', 'SERVER_SOFTWARE': 'WSGIServer/0.1 Python/2.7.3', 'DBUS_SESSION_BUS_ADDRESS': 'unix:abstract=/tmp/dbus-yqGF3E2PGf,guid=89d4544798d13eb4921d62bb53c621b8', 'REQUEST_METHOD': 'GET', 'LOGNAME': 'root', 'USER': 'root', 'PATH': '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin', 'QUERY_STRING': 'mystring=helloWorld', 'DM_CONTROL': '/var/run/xdmctl', 'XCURSOR_THEME': 'default', 'SSH_AGENT_PID': '3147', 'LANG': 'en_IN', 'PROFILEHOME': '', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'COLORFGBG': '0;15', 'XDG_SESSION_COOKIE': '5579209dc20a00bbc295e2c253521fbe-1405494311.906227-164659386', 'LANGUAGE': 'en_IN:en', 'SESSION_MANAGER': 'local/debian:@/tmp/.ICE-unix/3235,unix/debian:/tmp/.ICE-unix/3235', 'SHLVL': '2', 'CONTENT_TYPE': 'text/plain', 'KDE_SESSION_VERSION': '4', 'wsgi.url_scheme': 'http', 'WINDOWPATH': '7', 'SERVER_PORT': '8000', 'KDE_FULL_SESSION': 'true', 'HOME': '/root', 'KONSOLE_DBUS_SESSION': '/Sessions/1', 'PATH_INFO': '/encode', 'CONTENT_LENGTH': '', 'KDE_SESSION_UID': '1000', 'QT_PLUGIN_PATH': '/home/rohit4/.kde/lib/kde4/plugins/:/usr/lib/kde4/plugins/', 'HTTP_ACCEPT_CHARSET': 'utf-8,*;q=0.5', 'XDM_MANAGED': 'method=classic', 'SSH_AUTH_SOCK': '/tmp/ssh-SKd8KUiQVB7C/agent.3061', 'KONSOLE_DBUS_SERVICE': ':1.80', 'wsgi.input': <socket._fileobject object at 0xb70338ec>, 'HTTP_USER_AGENT': 'Mozilla/5.0 (X11; Linux i686) KHTML/4.8.4 (like Gecko) Konqueror/4.8', 'HTTP_HOST': 'localhost:8000', 'GTK2_RC_FILES': '/etc/gtk-2.0/gtkrc:/home/rohit4/.gtkrc-2.0:/home/rohit4/.kde/share/config/gtkrc-2.0', 'wsgi.multithread': True, 'HTTP_CONNECTION': 'keep-alive', 'GS_LIB': '/home/rohit4/.fonts', 'SHELL_SESSION_ID': '07cde6c4347c4000a658040e57b36574', '_': '/usr/bin/python', 'HTTP_ACCEPT': 'text/html, text/*;q=0.9, image/jpeg;q=0.9, image/png;q=0.9, image/*;q=0.9, */*;q=0.8', 'DESKTOP_SESSION': 'default', 'wsgi.file_wrapper': <class wsgiref.util.FileWrapper at 0xb704d86c>, 'DISPLAY': ':0', 'SERVER_NAME': 'localhost', 'GATEWAY_INTERFACE': 'CGI/1.1', 'wsgi.run_once': False, 'OLDPWD': '/home/rohit4', 'REMOTE_ADDR': '127.0.0.1', 'SCRIPT_NAME': '', 'wsgi.errors': <open file '<stderr>', mode 'w' at 0xb74f70d0>, 'XDG_DATA_DIRS': '/usr/share:/usr/share:/usr/local/share', 'PWD': '/home/rohit4/python_code/foundations_of_p_n_p/web_applications', 'GTK_RC_FILES': '/etc/gtk/gtkrc:/home/rohit4/.gtkrc:/home/rohit4/.kde/share/config/gtkrc', 'MAIL': '/var/mail/root', 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:', 'REMOTE_HOST': 'localhost', 'HTTP_ACCEPT_ENCODING': 'gzip, deflate, x-gzip, x-deflate', 'KDE_MULTIHEAD': 'false'}<bound method ServerHandler.start_response of <wsgiref.simple_server.ServerHandler instance at 0xb705a06c>>

### QUERY_STRING 	=> 'mystring=helloWorld'
### PATH_INFO    	=> '/encode'
### REQUEST_METHOD 	=> GET

# When you click on return to home page
#===================================================================================
#{'wsgi.version': (1, 0), 'WINDOWID': '41943058', 'wsgi.multiprocess': False, 'HTTP_REFERER': 'http://localhost:8000/encode?mystring=helloWorld', 'HTTP_ACCEPT_LANGUAGE': 'en-US,en;q=0.9', 'SERVER_PROTOCOL': 'HTTP/1.1', 'SERVER_SOFTWARE': 'WSGIServer/0.1 Python/2.7.3', 'DBUS_SESSION_BUS_ADDRESS': 'unix:abstract=/tmp/dbus-yqGF3E2PGf,guid=89d4544798d13eb4921d62bb53c621b8', 'REQUEST_METHOD': 'GET', 'LOGNAME': 'root', 'USER': 'root', 'PATH': '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin', 'QUERY_STRING': '', 'DM_CONTROL': '/var/run/xdmctl', 'XCURSOR_THEME': 'default', 'SSH_AGENT_PID': '3147', 'LANG': 'en_IN', 'PROFILEHOME': '', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'COLORFGBG': '0;15', 'XDG_SESSION_COOKIE': '5579209dc20a00bbc295e2c253521fbe-1405494311.906227-164659386', 'LANGUAGE': 'en_IN:en', 'SESSION_MANAGER': 'local/debian:@/tmp/.ICE-unix/3235,unix/debian:/tmp/.ICE-unix/3235', 'SHLVL': '2', 'CONTENT_TYPE': 'text/plain', 'KDE_SESSION_VERSION': '4', 'wsgi.url_scheme': 'http', 'WINDOWPATH': '7', 'SERVER_PORT': '8000', 'KDE_FULL_SESSION': 'true', 'HOME': '/root', 'KONSOLE_DBUS_SESSION': '/Sessions/1', 'PATH_INFO': '/', 'CONTENT_LENGTH': '', 'KDE_SESSION_UID': '1000', 'QT_PLUGIN_PATH': '/home/rohit4/.kde/lib/kde4/plugins/:/usr/lib/kde4/plugins/', 'HTTP_ACCEPT_CHARSET': 'utf-8,*;q=0.5', 'XDM_MANAGED': 'method=classic', 'SSH_AUTH_SOCK': '/tmp/ssh-SKd8KUiQVB7C/agent.3061', 'KONSOLE_DBUS_SERVICE': ':1.80', 'wsgi.input': <socket._fileobject object at 0xb70608ec>, 'HTTP_USER_AGENT': 'Mozilla/5.0 (X11; Linux i686) KHTML/4.8.4 (like Gecko) Konqueror/4.8', 'HTTP_HOST': 'localhost:8000', 'GTK2_RC_FILES': '/etc/gtk-2.0/gtkrc:/home/rohit4/.gtkrc-2.0:/home/rohit4/.kde/share/config/gtkrc-2.0', 'wsgi.multithread': True, 'HTTP_CONNECTION': 'keep-alive', 'GS_LIB': '/home/rohit4/.fonts', 'SHELL_SESSION_ID': '07cde6c4347c4000a658040e57b36574', '_': '/usr/bin/python', 'HTTP_ACCEPT': 'text/html, text/*;q=0.9, image/jpeg;q=0.9, image/png;q=0.9, image/*;q=0.9, */*;q=0.8', 'DESKTOP_SESSION': 'default', 'wsgi.file_wrapper': <class wsgiref.util.FileWrapper at 0xb707a86c>, 'DISPLAY': ':0', 'SERVER_NAME': 'localhost', 'GATEWAY_INTERFACE': 'CGI/1.1', 'wsgi.run_once': False, 'OLDPWD': '/home/rohit4', 'REMOTE_ADDR': '127.0.0.1', 'SCRIPT_NAME': '', 'wsgi.errors': <open file '<stderr>', mode 'w' at 0xb75240d0>, 'XDG_DATA_DIRS': '/usr/share:/usr/share:/usr/local/share', 'PWD': '/home/rohit4/python_code/foundations_of_p_n_p/web_applications', 'GTK_RC_FILES': '/etc/gtk/gtkrc:/home/rohit4/.gtkrc:/home/rohit4/.kde/share/config/gtkrc', 'MAIL': '/var/mail/root', 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:', 'REMOTE_HOST': 'localhost', 'HTTP_ACCEPT_ENCODING': 'gzip, deflate, x-gzip, x-deflate', 'KDE_MULTIHEAD': 'false'}<bound method ServerHandler.start_response of <wsgiref.simple_server.ServerHandler instance at 0xb70870ec>>

### QUERY_STRING 	=> ''
### PATH_INFO    	=> '/'
### REQUEST_METHOD 	=> GET
