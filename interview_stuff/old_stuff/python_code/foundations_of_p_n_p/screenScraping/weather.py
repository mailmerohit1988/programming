#!/usr/bin/env python 
# Foundations of Python Network Programming - Chapter 10 - weather.py 
# Fetch the weather forecast from the National Weather Service. 

import sys, urllib, urllib2 
#import lxml.etree 
#from lxml.cssselect import CSSSelector 
from BeautifulSoup import BeautifulSoup 

if len(sys.argv) < 2: 
	print >>sys.stderr, 'usage: weather.py CITY, STATE' 
	exit(2) 

data = urllib.urlencode({'inputstring': ' '.join(sys.argv[1:])}) 
info = urllib2.urlopen('http://forecast.weather.gov/zipcity.php', data) 
print {'inputstring': ' '.join(sys.argv[1:])}
print "hello"
content = info.read() 
open('phoenix.html', 'w').write(content) 

# LXML.ETREE DOESNT WORK FOR ME , SO I AM GOING TO COMMENT OUT THE SOLUTION #1
# Solution #1 
#parser = lxml.etree.HTMLParser(encoding='utf-8') 
#ree = lxml.etree.fromstring(content, parser) 
#big = CSSSelector('td.big')(tree)[0] 
#if big.find('font') is not None: 
#	big = big.find('font') 
#print 'Condition:', big.text.strip() 
#print 'Temperature:', big.findall('br')[1].tail 
#tr = tree.xpath('.//td[b="Humidity"]')[0].getparent() 
#print 'Humidity:', tr.findall('td')[1].text 
#print 

# Solution #2 
soup = BeautifulSoup(content) # doctest: +SKIP 
#big = soup.find('td', 'big') 

condition = soup.find('p', 'myforecast-current') 

#if condition.font is not None: 
	#condition = condition.font 
	
print 'Condition:', condition.contents[0]

temp=soup.find('span', 'myforecast-current-sm') 
temperature=temp.contents[0].string.strip() 
print 'Temperature:', temperature.replace('&deg;', ' ') 

humid=soup.find('b', text='Humidity').parent.parent.parent
print 'Humidity:', humid('li')[0].contents[1]
