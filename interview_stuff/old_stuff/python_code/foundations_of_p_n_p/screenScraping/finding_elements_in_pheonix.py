>>> 
>>> from BeautifulSoup import BeautifulSoup
>>> soup = BeautifulSoup(open('phoenix.html'))
>>> soup.find('div', 'div-full current-conditions') 
<div class="div-full current-conditions"><div id="current_conditions" class="one-third-first"><div class="div-half"><p class="feature"><img src="/images/wtf/large/nbkn.png" alt="" width="134" height="134" /></p></div><div class="div-half-right"><p class="myforecast-current">Mostly Cloudy</p><p class="myforecast-current-lrg">89&deg;F</p><p><span class="myforecast-current-sm">32&deg;C</span></p></div></div><div id="current_conditions_detail" class="one-third-first"><ul class="current-conditions-detail"><li><span class="label">Humidity</span>39%</li><li><span class="label">Wind Speed</span>calm</li><li><span class="label">Barometer</span>29.83 in (1007.9 mb)</li><li><span class="label">Dewpoint</span>61&deg;F (16&deg;C)</li><li><span class="label">Visibility</span>10.00 mi</li><li><span class="label">Heat Index</span>89&deg;F (32&deg;C)</li></ul><p class="current-conditions-timestamp">Last Update on 07 Jul 4:51 am MST </p><br /></div><div id="current_conditions_station" class="one-third-last"><p style="font-size: 8pt;">Current conditions at</p><p class="current-conditions-location">Phoenix, Phoenix Sky Harbor International Airport (KPHX)</p><p> Lat: 33.427799&deg;N   Lon: 112.003465&deg;W   Elev: 1115ft.<br /></p><div class="current-conditions-extra"><p><a href="http://www.wrh.noaa.gov/total_forecast/other_obs.php?wfo=psr&amp;zone=AZZ023">More Local Wx</a> | <a href="http://www.wrh.noaa.gov/mesowest/getobext.php?wfo=psr&amp;sid=KPHX&amp;num=72&amp;raw=0">3 Day History</a> | <a href="http://mobile.weather.gov/index.php?lat=33.427799&amp;lon=-112.003465">Mobile Weather</a></p></div></div></div>
>>> 


>>> from BeautifulSoup import BeautifulSoup
>>> soup = BeautifulSoup(open('phoenix.html'))

>>> soup.find('div', 'div-full current-conditions') 
<div class="div-full current-conditions"><div id="current_conditions" class="one-third-first"><div class="div-half"><p class="feature"><img src="/images/wtf/large/nbkn.png" alt="" width="134" height="134" /></p></div><div class="div-half-right"><p class="myforecast-current">Mostly Cloudy</p><p class="myforecast-current-lrg">89&deg;F</p><p><span class="myforecast-current-sm">32&deg;C</span></p></div></div><div id="current_conditions_detail" class="one-third-first"><ul class="current-conditions-detail"><li><span class="label">Humidity</span>39%</li><li><span class="label">Wind Speed</span>calm</li><li><span class="label">Barometer</span>29.83 in (1007.9 mb)</li><li><span class="label">Dewpoint</span>61&deg;F (16&deg;C)</li><li><span class="label">Visibility</span>10.00 mi</li><li><span class="label">Heat Index</span>89&deg;F (32&deg;C)</li></ul><p class="current-conditions-timestamp">Last Update on 07 Jul 4:51 am MST </p><br /></div><div id="current_conditions_station" class="one-third-last"><p style="font-size: 8pt;">Current conditions at</p><p class="current-conditions-location">Phoenix, Phoenix Sky Harbor International Airport (KPHX)</p><p> Lat: 33.427799&deg;N   Lon: 112.003465&deg;W   Elev: 1115ft.<br /></p><div class="current-conditions-extra"><p><a href="http://www.wrh.noaa.gov/total_forecast/other_obs.php?wfo=psr&amp;zone=AZZ023">More Local Wx</a> | <a href="http://www.wrh.noaa.gov/mesowest/getobext.php?wfo=psr&amp;sid=KPHX&amp;num=72&amp;raw=0">3 Day History</a> | <a href="http://mobile.weather.gov/index.php?lat=33.427799&amp;lon=-112.003465">Mobile Weather</a></p></div></div></div>

>>> soup.find('div', 'one-third-first') 
<div id="current_conditions" class="one-third-first"><div class="div-half"><p class="feature"><img src="/images/wtf/large/nbkn.png" alt="" width="134" height="134" /></p></div><div class="div-half-right"><p class="myforecast-current">Mostly Cloudy</p><p class="myforecast-current-lrg">89&deg;F</p><p><span class="myforecast-current-sm">32&deg;C</span></p></div></div>
>>> 



>>> soup.find('div', 'one-third-first') 
<div id="current_conditions" class="one-third-first"><div class="div-half"><p class="feature"><img src="/images/wtf/large/nbkn.png" alt="" width="134" height="134" /></p></div><div class="div-half-right"><p class="myforecast-current">Mostly Cloudy</p><p class="myforecast-current-lrg">89&deg;F</p><p><span class="myforecast-current-sm">32&deg;C</span></p></div></div>
>>> 

>>> soup.find('div', 'one-third-first') 
<div id="current_conditions" class="one-third-first"><div class="div-half"><p class="feature"><img src="/images/wtf/large/nbkn.png" alt="" width="134" height="134" /></p></div><div class="div-half-right"><p class="myforecast-current">Mostly Cloudy</p><p class="myforecast-current-lrg">89&deg;F</p><p><span class="myforecast-current-sm">32&deg;C</span></p></div></div>
>>> td = soup.find('div', 'one-third-first') 
>>> td.contents[0]
<div class="div-half"><p class="feature"><img src="/images/wtf/large/nbkn.png" alt="" width="134" height="134" /></p></div>
>>> 



>>> td=soup.find('div', 'div-full current-conditions') 
>>> td.contents[0]
<div id="current_conditions" class="one-third-first"><div class="div-half"><p class="feature"><img src="/images/wtf/large/nbkn.png" alt="" width="134" height="134" /></p></div><div class="div-half-right"><p class="myforecast-current">Mostly Cloudy</p><p class="myforecast-current-lrg">89&deg;F</p><p><span class="myforecast-current-sm">32&deg;C</span></p></div></div>
>>> soup.find('p', 'myforecast-current') 
<p class="myforecast-current">Mostly Cloudy</p>
>>> td=soup.find('p', 'myforecast-current') 
>>> td.contents[0]
u'Mostly Cloudy'
>>> 

>>> soup.find('span', 'myforecast-current-sm') 
<span class="myforecast-current-sm">32&deg;C</span>
>>> 



>>> soup.find('span', 'label') 
<span class="label">Humidity</span>
>>> 


>>> soup.find('b', text='Humidity').parent.parent.parent
<ul class="current-conditions-detail"><li><span class="label">Humidity</span>39%</li><li><span class="label">Wind Speed</span>calm</li><li><span class="label">Barometer</span>29.83 in (1007.9 mb)</li><li><span class="label">Dewpoint</span>61&deg;F (16&deg;C)</li><li><span class="label">Visibility</span>10.00 mi</li><li><span class="label">Heat Index</span>89&deg;F (32&deg;C)</li></ul>
>>> soup.find('b', text='Humidity')
u'Humidity'
>>> soup.find('b', text='Humidity').parent
<span class="label">Humidity</span>
>>> soup.find('b', text='Humidity').parent.parent
<li><span class="label">Humidity</span>39%</li>
>>> 
>>> soup.find('b', text='Humidity').parent.parent.parent
<ul class="current-conditions-detail"><li><span class="label">Humidity</span>39%</li><li><span class="label">Wind Speed</span>calm</li><li><span class="label">Barometer</span>29.83 in (1007.9 mb)</li><li><span class="label">Dewpoint</span>61&deg;F (16&deg;C)</li><li><span class="label">Visibility</span>10.00 mi</li><li><span class="label">Heat Index</span>89&deg;F (32&deg;C)</li></ul>
>>> 


>>> soup.find('b', text='Humidity').parent.parent.parent
<ul class="current-conditions-detail"><li><span class="label">Humidity</span>39%</li><li><span class="label">Wind Speed</span>calm</li><li><span class="label">Barometer</span>29.83 in (1007.9 mb)</li><li><span class="label">Dewpoint</span>61&deg;F (16&deg;C)</li><li><span class="label">Visibility</span>10.00 mi</li><li><span class="label">Heat Index</span>89&deg;F (32&deg;C)</li></ul>
>>> 

>>> tr=soup.find('b', text='Humidity').parent.parent.parent
>>> print 'Humidity:', tr('span')[0]
Humidity: <span class="label">Humidity</span>
>>> print 'Humidity:', tr('span')[1]
Humidity: <span class="label">Wind Speed</span>
>>> print 'Humidity:', tr('span')[2]
Humidity: <span class="label">Barometer</span>
>>> 

>>> soup.find('b', text='Humidity').parent.parent.parent
<ul class="current-conditions-detail"><li><span class="label">Humidity</span>39%</li><li><span class="label">Wind Speed</span>calm</li><li><span class="label">Barometer</span>29.83 in (1007.9 mb)</li><li><span class="label">Dewpoint</span>61&deg;F (16&deg;C)</li><li><span class="label">Visibility</span>10.00 mi</li><li><span class="label">Heat Index</span>89&deg;F (32&deg;C)</li></ul>
>>> tr=soup.find('b', text='Humidity').parent.parent.parent
>>> print 'Humidity:', tr('li')
Humidity: [<li><span class="label">Humidity</span>39%</li>, <li><span class="label">Wind Speed</span>calm</li>, <li><span class="label">Barometer</span>29.83 in (1007.9 mb)</li>, <li><span class="label">Dewpoint</span>61&deg;F (16&deg;C)</li>, <li><span class="label">Visibility</span>10.00 mi</li>, <li><span class="label">Heat Index</span>89&deg;F (32&deg;C)</li>]
>>> 


>>> tr=soup.find('b', text='Humidity').parent.parent.parent
>>> print 'Humidity:', tr('li')
Humidity: [<li><span class="label">Humidity</span>39%</li>, <li><span class="label">Wind Speed</span>calm</li>, <li><span class="label">Barometer</span>29.83 in (1007.9 mb)</li>, <li><span class="label">Dewpoint</span>61&deg;F (16&deg;C)</li>, <li><span class="label">Visibility</span>10.00 mi</li>, <li><span class="label">Heat Index</span>89&deg;F (32&deg;C)</li>]
>>> print 'Humidity:', tr('li')[0]
Humidity: <li><span class="label">Humidity</span>39%</li>
>>> 

>>> tr=soup.find('b', text='Humidity').parent.parent.parent
>>> a=tr('li')[0]
>>> a.contents[1]
u'39%'
>>> 


