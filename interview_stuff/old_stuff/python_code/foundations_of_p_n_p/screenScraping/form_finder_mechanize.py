import mechanize 

br = mechanize.Browser() 

response = br.open('http://www.weather.gov/')  # when we do a br.open() then in open() call the forms variable gets intialized with all the forms that the webpage contains
print 'opened the webpage'

for form in br.forms(): 
	print '---------------'
	print form
	print '---------------'
	print '%r %r %s' % (form.name, form.attrs.get('id'), form.action) 
	for control in form.controls: 
		print ' ', control.type, control.name, repr(control.value) 
