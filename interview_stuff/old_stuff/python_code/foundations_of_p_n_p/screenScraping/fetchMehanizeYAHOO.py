#!/usr/bin/env python 
# Foundations of Python Network Programming - Chapter 10 - fetch_mechanize.py 
# Submitting a form and retrieving a page with mechanize 

import mechanize 

br = mechanize.Browser() 
br.open('http://www.yahoomail.com/') 

print 'opened'
for form in br.forms(): 
	print '---------------'
	print form
	print '---------------'
	print '%r %r %s' % (form.name, form.attrs.get('id'), form.action) 
	for control in form.controls: 
		print ' ', control.type, control.name, repr(control.value) 


br.select_form(predicate=lambda(form): 'yahoo' in form.action) 
br['username'] = 'mailmerohit1988' 
br['passwd'] = 'gotohell' 
response = br.submit()

content = response.read() 
open('phoenix.html', 'w').write(content) 

# This program doesnt work
