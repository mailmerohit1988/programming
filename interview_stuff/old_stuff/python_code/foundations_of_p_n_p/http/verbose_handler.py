#!/usr/bin/env python 
# Foundations of Python Network Programming - Chapter 9 - verbose_handler.py 
# HTTP request handler for urllib2 that prints requests and responses. 

import StringIO, httplib, urllib2 

class VerboseHTTPResponse(httplib.HTTPResponse): 
	def _read_status(self): 
		print " ******************* _read_status() called ************************************ \n\n "
		s = self.fp.read() 
		print self # self is object of VerboseHTTPResponse
		#print s # This print the entire response
		print 'done \n\n' 
		print '-' * 20, 'Response', '-' * 20 
		print s.split('\r\n\r\n')[0] 
		self.fp = StringIO.StringIO(s) 
		return httplib.HTTPResponse._read_status(self) 

class VerboseHTTPConnection(httplib.HTTPConnection): 
	response_class = VerboseHTTPResponse 
	def send(self, s): 
		print "************************ send() called *****************************************\n\n"
		print self
		print s
		print 'done \n\n' 
		print '-' * 50 
		print s.strip() 
		httplib.HTTPConnection.send(self, s) # sending a http request from our classs

class VerboseHTTPHandler(urllib2.HTTPHandler):  # I think urllib2.HTTPHandler is used when you are doing self.do_open , so it is calling do_open in urllib2.HTTPHandler
	def http_open(self, req): 					# or maybe VerboseHTTPHandler IS CHILD OF urllib2.HTTPHandler
		print "*************************** http_open() called **********************************\n\n"
		print self # Object of VerboseHTTPHandler
		print req  # Request object of urllib2
		print "*************************** http_open()->do_open() called **********************************\n\n"
		return self.do_open(VerboseHTTPConnection, req) # I think here it is calling some method from urllib2 which  is preparing the request s and then 
														# is calling send() method from VerboseHTTPConnection class and after sending request it collects response and 
														# it calls _read_status of VerboseHTTPResponse
												


# SO BEFORE SENDING THE REQUEST IT IS CALLING YOUR METHOD FOR YOU TO SEE THE REQUEST
# AFTER GETTING THE RESPONSE IT IS CALLING YOUR METHOD FOR YOU TO SEE THE RESPONSE
