from verbose_handler import VerboseHTTPHandler
import urllib, urllib2

# print VerboseHTTPHandler ## This is an object of VerboseHTTPHandler class

opener = urllib2.build_opener(VerboseHTTPHandler) 

print "******************** printing opener object ****************************** \n\n "
print opener # opener is an object of urllib2.OpenerDirector :  <urllib2.OpenerDirector instance at 0xb74828ac> and it opener() 
# method also knows about VerboseHTTPHandler class's http_open or I MAY EVEN SAY THAT """" OPENER OBJECT HAS VerboseHTTPHandler OBJECT INSIDE IT """"

print "******************** opening a website *********************************** \n\n"
opener.open('http://www.ietf.org/rfc/rfc2616.txt') # so if open() method is called with the object of urllib2.OpenerDirector then it calls some method of urllib2 and from there it
# calls http_open() method of VerboseHTTPHandler class .. if you change the http_open() method name to any other method name then it wont work

"""


>>> from verbose_handler import VerboseHTTPHandler
>>> import urllib, urllib2
>>> opener = urllib2.build_opener(VerboseHTTPHandler) 
>>> form = urllib.urlencode({'inputstring': 'Atlanta, GA'})
>>> response = opener.open('http://forecast.weather.gov/zipcity.php', form)
*************************** http_open() called **********************************


<verbose_handler.VerboseHTTPHandler instance at 0xb705076c>
<urllib2.Request instance at 0xb74f694c>
*************************** http_open()->do_open() called **********************************


************************ send() called *****************************************


<verbose_handler.VerboseHTTPConnection instance at 0xb706bd6c>
POST /zipcity.php HTTP/1.1
Accept-Encoding: identity
Content-Length: 25
Host: forecast.weather.gov
Content-Type: application/x-www-form-urlencoded
Connection: close
User-Agent: Python-urllib/2.7

inputstring=Atlanta%2C+GA
done 

                                                                                                                                                                                                                                             
--------------------------------------------------                                                                                                                                                                                           
POST /zipcity.php HTTP/1.1                                                                                                                                                                                                                   
Accept-Encoding: identity                                                                                                                                                                                                                    
Content-Length: 25                                                                                                                                                                                                                           
Host: forecast.weather.gov                                                                                                                                                                                                                   
Content-Type: application/x-www-form-urlencoded                                                                                                                                                                                              
Connection: close                                                                                                                                                                                                                            
User-Agent: Python-urllib/2.7                                                                                                                                                                                                                
                                                                                                                                                                                                                                             
inputstring=Atlanta%2C+GA                                                                                                                                                                                                                    
 ******************* _read_status() called ************************************                                                                                                                                                              
                                                                                                                                                                                                                                             
                                                                                                                                                                                                                                             
<verbose_handler.VerboseHTTPResponse instance at 0xb706becc>                                                                                                                                                                                 
done                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                             
                                                                                                                                                                                                                                             
-------------------- Response --------------------                                                                                                                                                                                           
HTTP/1.1 302 Moved Temporarily                                                                                                                                                                                                               
Server: Apache/2.2.15 (Red Hat)
X-NIDS-ServerID: www2.mo
Location: http://forecast.weather.gov/MapClick.php?CityName=Atlanta&state=GA&site=FFC&textField1=33.7629&textField2=-84.4226&e=1
Content-Length: 522
Content-Type: text/html; charset=UTF-8
Cache-Control: max-age=900
Expires: Fri, 27 Jun 2014 10:58:29 GMT
Date: Fri, 27 Jun 2014 10:43:29 GMT
Connection: close
*************************** http_open() called **********************************


<verbose_handler.VerboseHTTPHandler instance at 0xb705076c>
<urllib2.Request instance at 0xb706be6c>
*************************** http_open()->do_open() called **********************************


************************ send() called *****************************************


<verbose_handler.VerboseHTTPConnection instance at 0xb707010c>
GET /MapClick.php?CityName=Atlanta&state=GA&site=FFC&textField1=33.7629&textField2=-84.4226&e=1 HTTP/1.1
Accept-Encoding: identity
Host: forecast.weather.gov
Connection: close
User-Agent: Python-urllib/2.7


done 


--------------------------------------------------
GET /MapClick.php?CityName=Atlanta&state=GA&site=FFC&textField1=33.7629&textField2=-84.4226&e=1 HTTP/1.1
Accept-Encoding: identity
Host: forecast.weather.gov
Connection: close
User-Agent: Python-urllib/2.7
 ******************* _read_status() called ************************************ 

 
<verbose_handler.VerboseHTTPResponse instance at 0xb707020c>
done 


-------------------- Response --------------------
HTTP/1.1 200 OK
Server: Apache/2.2.15 (Red Hat)
X-NIDS-ServerID: www13.mo
Content-Type: text/html; charset=UTF-8
Vary: Accept-Encoding
Cache-Control: max-age=850
Expires: Fri, 27 Jun 2014 10:57:40 GMT
Date: Fri, 27 Jun 2014 10:43:30 GMT
Transfer-Encoding:  chunked
Connection: close
Connection: Transfer-Encoding
>>> 

"""
