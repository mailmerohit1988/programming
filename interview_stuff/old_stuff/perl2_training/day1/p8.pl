# Total team size
# Display team details which has proficiency in C


my @lang1=("C++","C","Unix");
my %h1=(
	'location' => 'BLR',
	'size'     => 20,
	'prof'     => \@lang1,
);

my @lang2=("QTP","MT","VBS");
my %h2=(
	'location' => 'CHN',
	'size'     => 23,
	'prof'     => \@lang2,
);

my @lang3=("JAVA","J2EE","C");
my %h3=(
	'location' => 'HYD',
	'size'     => 35,
	'prof'     => \@lang3,
);


my %teams = (
	'team1' => \%h1,
	'team2' => \%h2,
	'team3' => \%h3,
);

foreach my $key (keys(%teams)){
	print "Size of $key is $teams{$key}->{size}\n";
	
	my @profs = @{$teams{$key}->{prof}};
	if ( grep( /^C$/, @profs ) ) {
		print "Team $key contains prof as C\n";
	}
}
