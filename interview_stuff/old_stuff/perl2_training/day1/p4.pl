use List::Util qw(shuffle);

my $value=45;
my @nums=(20..30);
my %hash=('arun'=>10,'chetan'=>45);
sub fun { print "Hello World\n"; }

@arr=(\$value,\@nums,\%hash,\&fun);
@arr=shuffle(@arr);

foreach (@arr) {
	my $ref = ref $_;
	if($ref eq "SCALAR"){
		print ${$_}."\n";
	}elsif($ref eq "ARRAY"){
		my @arr=@{$_};
		foreach (@arr){
			print $_." ";
		}
		print "\n";
	}elsif($ref eq "HASH"){
		my %hash=%{$_};
		while ( ($key, $value) = each %hash ){
			print "key: $key, value: $hash{$key}\n";
		}
	}elsif($ref eq "CODE"){
		&$_();
	}
}

