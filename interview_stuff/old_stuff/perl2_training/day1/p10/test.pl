use Vehicle;

$bus=  new Vehicle; # or 
					# $bus = Vehicle::new(); C++ Style , 1st argument will not be Vehicle
					# $bus = Vehicle->new(); Perl style , 1st argument will be Vehicle

$bus->move();

print $bus."\n";
