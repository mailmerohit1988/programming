package Vehicle;

sub new {
	print "First argument : $_[0]\n";
	$ref={name=>undef,wheels=>undef};
	bless($ref,Vehicle);
	# return $ref; Need not to return if bless is the last statement
}

sub move {print "move called\n";}
sub turn {}
sub break {}

1;
