# Counts number of files and dirs
my @files;
my @directories;

opendir(FH,".") || die "Can't open: $!";
@dirs = readdir(FH);

foreach (@dirs){
	if(-d $_){
		push (@directories,$_);
	}elsif(-f $_){
		push (@files,$_);
	}
}

closedir (FH);

my %hash;

foreach (@files){
	if($_ =~ /.*\.(.*)/){
		$hash{$1}++;
	}
}

my $n_files=@files;
print "Number of files : $n_files\n";

my $n_directories=@directories;
print "Number of dirs : $n_directories\n";

use Data::Dumper;
print Dumper(\%hash);
