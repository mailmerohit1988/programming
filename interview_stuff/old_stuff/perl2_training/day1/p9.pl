$ref=[
	{'dvd'=>5,'cd'=>10,'prn'=>3},
	{'mon'=>1,'prn'=>4},
	{'dvd'=>4,'prn'=>5}
];

my %f;

foreach (@{$ref}){
	my %hash=%{$_};
	while (($k,$v)=each %hash){
		if (exists $f{$k}){
			$f{$k}=$v+$f{$k};
		}else{
			$f{$k}=$v;
		}
	}
}

use Data::Dumper;
print Dumper(\%f);
