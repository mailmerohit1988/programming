use List::Util qw(sum);

@w=(23,48,12);
@b=(30,40,50);
@s=(20,20,20);
@c=(10,10,10);

%t = (
	'wifi'=>\@w,
	'bt'=>\@b,
	'sms'=>\@s,
	'calls'=>\@c,
);

#Way1
#====
foreach my $i (keys(%t)) {
	my @arr = @{$t{$i}};
	my $c=@arr;
	my $tot=0;
	foreach(@arr){
		$tot+=$_;
	}
	print "Average is : ". $tot/$c."\n";
}

# Another way to print averages
# Way2
#======
foreach my $i (keys(%t)) {
	$avg=sum(@{$t{$i}})/@{$t{$i}};
	$avg=sprintf("%4.2f",$avg);
	print "Average is : ". $avg."\n";
}
