my @ar1 = (1,2,3,4);
my @ar2 = (5,6,7,8);

fun(\@ar1,\@ar2);


sub fun {
	my @arr=@_;
	my $size1 = @{$arr[0]};
	my $size2 = @{$arr[1]};
	print $size1."\n";
	print $size2."\n";
}

# You can alsouse $_[0] or $_[1]
