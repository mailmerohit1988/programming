use Product;

my @arr;
push @arr,Product->new(
				name  => 'prod1',
				price => '20',
				qty   => '30',
			);
			
push @arr,Product->new(
				name  => 'prod2',
				price => '20',
				qty   => '30',
			);

push @arr,Product->new(
				name  => 'prod3',
				price => '20',
				qty   => '30',
			);
push @arr,Product->new(
				name  => 'prod4',
				price => '20',
				qty   => '30',
			);

push @arr,Product->new(
				name  => 'prod5',
				price => '20',
				qty   => '30',
			);

my $total_price;

foreach (@arr){
	$_->display();
	$total_price+=$_->price();
}

print "Total price : $total_price\n";

print "TOTAL PRICE (Using static members): ".Product::get_total_price()."\n";
