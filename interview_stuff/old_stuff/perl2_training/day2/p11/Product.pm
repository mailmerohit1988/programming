package Product;

my $sum =0;# This is not a part of object, it is a part of your class or STATIC MEMBERS
			# my variables are not accessible directly outside the package so write a function to get it outside this package
			# or else you can declare it as our so it can be directly accessed outside the package
			
sub new {
	my $pack = shift;
	my $ref={@_};
	$sum+=$ref->{price};
	bless ($ref, Product);
}

sub display{
	my $self=shift;
	print $self->{name}."\t";
	print $self->{price}."\t";
	print $self->{qty}."\n";
}

sub price{
	my $self=shift;
	return $self->{price};
}

sub get_total_price{
	return $sum;
}

1;
