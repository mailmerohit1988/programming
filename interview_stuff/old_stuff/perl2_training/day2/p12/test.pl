use Bus;


my $stud1 =  Student->new(
				name=>'arun',
				dept=>'CSE',
				marks=>[10,20,30]
);

my $stud2 =  Student->new(
				name=>'gawli',
				dept=>'CSE',
				marks=>[10,20,40]
);

my $stud3 =  Student->new(
				name=>'mohit',
				dept=>'CSE',
				marks=>[10,20,50]
);


print "Total is : ".$stud1->cal_total()."\n";


Student::display();

my $bus1 =  Bus->new(
				name=>'arun',
				dept=>'CSE',
				marks=>[10,20,30]
);

use Data::Dumper;

print Dumper($bus1);

print "Total is : ".$bus1->cal_total()."\n";
