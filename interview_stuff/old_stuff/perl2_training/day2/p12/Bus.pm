package Bus;

use Student;

our @ISA=qw(Student);

sub new {
	my $class=shift;
	my $ref={@_};
	bless($ref,$class);
}
