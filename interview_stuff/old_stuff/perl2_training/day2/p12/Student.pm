package Student;

my %avg_marks;

my $display = sub {
	use Data::Dumper;
	print Dumper(\%avg_marks);
};

sub new {
	my $class=shift;
	my $ref={@_};
	my $name=$ref->{name};
	bless($ref,Student);
	my $total = $ref->cal_total();
	$avg_marks{$name}=$total;
	return $ref; # if bless is not the last statement then you have to return $ref
}

sub cal_total{
	my $self=shift;
	my $sum=0;
	foreach (@{$self->{marks}}){
		$sum+=$_;
	}
	return $sum;
}

sub display{
	$display->();
}

1;
