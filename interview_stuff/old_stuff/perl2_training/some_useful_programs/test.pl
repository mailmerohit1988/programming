$report = " {
1691           'sevenmtt' => {
1692                         'controller' => {
1693                                         'sysname' => 'csiqa-6030-4',
1694                                         'version' => '7.3.7',
1695                                         'vfiler' => {
1696                                                     'messages' => {
1697                                                                   'message' => [
1698                                                                                {
1699                                                                                  'resources' => {
1700                                                                                                 'resource' => {
1701                                                                                                               'controlleripaddress' => '10.227.202.146',
1702                                                                                                               'resourcetype' => '',
1703                                                                                                               'volume' => '',
1704                                                                                                               'lun' => '',
1705                                                                                                               'controller' => 'csiqa-6030-4',
1706                                                                                                               'objects' => {},
1707                                                                                                               'qtree' => '',
1708                                                                                                               'vfiler' => 'vfiler0',
1709                                                                                                               'aggregate' => ''
1710                                                                                                             },
1711                                                                                                 'resourcetype' => ''
1712                                                                                               },
1713                                                                                  'qtrees' => {},
1714                                                                                  'message' => 'FPolicy is configured on the 7-Mode system. However, transitioning FPolicy is not supported by the 7-Mode Transition Tool.',
1715                                                                                  'luns' => {},
1716                                                                                  'level' => 'controller',
1717                                                                                  'correctiveaction' => 'After the transition is complete, manually configure FPolicy on the target SVM.',
1718                                                                                  'volumes' => {},
1719                                                                                  'type' => 'Warning',
1720                                                                                  'operation' => 'CIFS FPolicy',
1721                                                                                  'category' => 'CIFS',
1722                                                                                  'message-tags' => {
1723                                                                                                    'tag' => {
1724                                                                                                             'name' => '7-Mode Transition Tool Configuration Transition Gap'
1725                                                                                                           }
1726                                                                                                  },
1727                                                                                  'code' => '20442'
1728                                                                                },
1729                                                                                {
1730                                                                                  'resources' => {
1731                                                                                                 'resource' => {
1732                                                                                                               'controlleripaddress' => '10.227.202.146',
1733                                                                                                               'resourcetype' => '7-Mode Option',
1734                                                                                                               'volume' => '',
1735                                                                                                               'lun' => '',
1736                                                                                                               'controller' => 'csiqa-6030-4',
1737                                                                                                               'objects' => {
1738                                                                                                                            'object' => {
1739                                                                                                                                        'name' => 'cifs.symlinks.enable'
1740                                                                                                                                      }
1741                                                                                                                          },
1742                                                                                                               'qtree' => '',
1743                                                                                                               'vfiler' => 'vfiler0',
1744                                                                                                               'aggregate' => ''
1745                                                                                                             },
1746                                                                                                 'resourcetype' => '7-Mode Option'
1747                                                                                               },
1748                                                                                  'qtrees' => {},
1749                                                                                  'message' => 'The option \'cifs.symlinks.enable\' is enabled on the 7-Mode storage system. Data ONTAP operating in 7-Mode uses this option along with the \'-widelink\' setting on the CIFS shares to process the widelinks. However, clustered Data ONTAP uses a single CIFS-share-level option for both symlinks and widelinks processing. After transition, widelinks will get processed for all the transitioned shares regardless of the \'-widelink\' settings on the 7-Mode shares.',
1750                                                                                  'luns' => {},
1751                                                                                  'level' => 'controller',
1752                                                                                  'correctiveaction' => '',
1753                                                                                  'volumes' => {},
1754                                                                                  'type' => 'Warning',
1755                                                                                  'operation' => 'CIFS Symlink',
1756                                                                                  'category' => 'CIFS',
1757                                                                                  'code' => '20472'
1758                                                                                },
1759                                                                                {
1760                                                                                  'resources' => {
1761                                                                                                 'resource' => {
1762                                                                                                               'controlleripaddress' => '10.227.202.146',
1763                                                                                                               'resourcetype' => '7-Mode Option',
1764                                                                                                               'volume' => '',
1765                                                                                                               'lun' => '',
1766                                                                                                               'controller' => 'csiqa-6030-4',
1767                                                                                                               'objects' => {
1768                                                                                                                            'object' => {
1769                                                                                                                                        'name' => 'cifs.preserve_unix_security'
1770                                                                                                                                      }
1771                                                                                                                          },
1772                                                                                                               'qtree' => '',
1773                                                                                                               'vfiler' => 'vfiler0',
1774                                                                                                               'aggregate' => ''
1775                                                                                                             },
1776                                                                                                 'resourcetype' => '7-Mode Option'
1777                                                                                               },
1778                                                                                  'qtrees' => {},
1779                                                                                  'message' => 'The \'cifs.preserve_unix_security\' option is disabled on the 7-Mode storage system. The \'cifs.preserve_unix_security\' option is \'on\' by default in clustered Data ONTAP and can not be turned off.',
1780                                                                                  'luns' => {},
1781                                                                                  'level' => 'controller',
1782                                                                                  'correctiveaction' => '',
1783                                                                                  'volumes' => {},
1784                                                                                  'type' => 'Warning',
1785                                                                                  'operation' => 'CIFS Preserve UNIX Security',
1786                                                                                  'category' => 'CIFS',
1787                                                                                  'code' => '20562'
1788                                                                                },
1789                                                                                {
1790                                                                                  'resources' => {
1791                                                                                                 'resource' => {
1792                                                                                                               'controlleripaddress' => '10.227.202.146',
1793                                                                                                               'resourcetype' => '7-Mode Option',
1794                                                                                                               'volume' => '',
1795                                                                                                               'lun' => '',
1796                                                                                                               'controller' => 'csiqa-6030-4',
1797                                                                                                               'objects' => {
1798                                                                                                                            'object' => {
1799                                                                                                                                        'name' => 'cifs.LMCompatibilityLevel'
1800                                                                                                                                      }
1801                                                                                                                          },
1802                                                                                                               'qtree' => '',
1803                                                                                                               'vfiler' => 'vfiler0',
1804                                                                                                               'aggregate' => ''
1805                                                                                                             },
1806                                                                                                 'resourcetype' => '7-Mode Option'
1807                                                                                               },
1808                                                                                  'qtrees' => {},
1809                                                                                  'message' => 'LM authentication level is \'1\' on the 7-Mode storage system \'vfiler0@csiqa-6030-4\'. Windows clients older than Windows NT might face authentication failure since this level of authentication is not supported in clustered Data ONTAP.',
1810                                                                                  'luns' => {},
1811                                                                                  'level' => 'controller',
1812                                                                                  'correctiveaction' => '',
1813                                                                                  'volumes' => {},
1814                                                                                  'type' => 'Warning',
1815                                                                                  'operation' => 'CIFS LM Authentication',
1816                                                                                  'category' => 'CIFS',
1817                                                                                  'code' => '20792'
1818                                                                                },
1819                                                                                {
1820                                                                                  'resources' => {
1821                                                                                                 'resource' => {
1822                                                                                                               'controlleripaddress' => '10.227.202.146',
1823                                                                                                               'resourcetype' => '7-Mode Option',
1824                                                                                                               'volume' => '',
1825                                                                                                               'lun' => '',
1826                                                                                                               'controller' => 'csiqa-6030-4',
1827                                                                                                               'objects' => {
1828                                                                                                                            'object' => {
1829                                                                                                                                        'name' => 'cifs.signing.enable'
1830                                                                                                                                      }
1831                                                                                                                          },
1832                                                                                                               'qtree' => '',
1833                                                                                                               'vfiler' => 'vfiler0',
1834                                                                                                               'aggregate' => ''
1835                                                                                                             },
1836                                                                                                 'resourcetype' => '7-Mode Option'
1837                                                                                               },
1838                                                                                  'qtrees' => {},
1839                                                                                  'message' => 'Clustered Data ONTAP has a single setting to control the SMB signing for all the SMB protocol versions, unlike in Data ONTAP running in 7-Mode that has two separate options for SMB1 and SMB 2.x. This setting is controlled with the following command: \'vserver cifs security modify -vserver <vserver_name> -is-signing-required [true|false]\'. The default value for this setting is \'false\', which means that CIFS clients can negotiate signing with clustered Data ONTAP. However, if this setting is set to \'true\', all CIFS clients will be required to sign CIFS requests irrespective of the SMB protocol version.',
1840                                                                                  'luns' => {},
1841                                                                                  'level' => 'controller',
1842                                                                                  'correctiveaction' => 'After transitioning to clustered Data ONTAP, evaluate the impact of the difference in the SMB signing behavior between clustered Data ONTAP and Data ONTAP operating in 7-Mode, and set appropriate value for the SMB signing required setting for the destination SVM.',
1843                                                                                  'volumes' => {},
1844                                                                                  'type' => 'Warning',
1845                                                                                  'operation' => 'CIFS SMB Signing',
1846                                                                                  'category' => 'CIFS',
1847                                                                                  'code' => '20992'
1848                                                                                },
1849                                                                                {
1850                                                                                  'resources' => {
1851                                                                                                 'resource' => {
1852                                                                                                               'controlleripaddress' => '10.227.202.146',
1853                                                                                                               'resourcetype' => '7-Mode Option',
1854                                                                                                               'volume' => '',
1855                                                                                                               'lun' => '',
1856                                                                                                               'controller' => 'csiqa-6030-4',
1857                                                                                                               'objects' => {
1858                                                                                                                            'object' => {
1859                                                                                                                                        'name' => 'cifs.home_dirs_public_for_admin'
1860                                                                                                                                      }
1861                                                                                                                          },
1862                                                                                                               'qtree' => '',
1863                                                                                                               'vfiler' => 'vfiler0',
1864                                                                                                               'aggregate' => ''
1865                                                                                                             },
1866                                                                                                 'resourcetype' => '7-Mode Option'
1867                                                                                               },
1868                                                                                  'qtrees' => {},
1869                                                                                  'message' => 'The administrator is allowed (\'cifs.home_dirs_public_for_admin\' option) to access home directories of CIFS users on the 7-Mode storage system. However, in clustered Data ONTAP, the administrator is not allowed to access home directories of the CIFS users.',
1870                                                                                  'luns' => {},
1871                                                                                  'level' => 'controller',
1872                                                                                  'correctiveaction' => 'In clustered Data ONTAP, if the administrator is required to perform administrative tasks on the home directories of the CIFS users, a share needs to be created for each home-directory search path with ACLs set to allow full access to the administrator. The shares can then be accessed by the administrator to perform the administrative tasks on the home directories of the CIFS users.',
1873                                                                                  'volumes' => {},
1874                                                                                  'type' => 'Warning',
1875                                                                                  'operation' => 'Adminsitrator Home directories Access',
1876                                                                                  'category' => 'CIFS',
1877                                                                                  'code' => '25022'
1878                                                                                },
1879                                                                                {
1880                                                                                  'resources' => {
1881                                                                                                 'resource' => {
1882                                                                                                               'controlleripaddress' => '10.227.202.146',
1883                                                                                                               'resourcetype' => '7-Mode Option',
1884                                                                                                               'volume' => '',
1885                                                                                                               'lun' => '',
1886                                                                                                               'controller' => 'csiqa-6030-4',
1887                                                                                                               'objects' => {
1888                                                                                                                            'object' => {
1889                                                                                                                                        'name' => 'cifs.restrict_anonymous'
1890                                                                                                                                      }
1891                                                                                                                          },
1892                                                                                                               'qtree' => '',
1893                                                                                                               'vfiler' => 'vfiler0',
1894                                                                                                               'aggregate' => ''
1895                                                                                                             },
1896                                                                                                 'resourcetype' => '7-Mode Option'
1897                                                                                               },
1898                                                                                  'qtrees' => {},
1899                                                                                  'message' => 'The \'cifs.restrict_anonymous\' option is enabled on the 7-Mode storage system \'vfiler0@csiqa-6030-4\'. In clustered Data ONTAP 8.2.1, this setting is not supported and by default, anonymous or null users can enumerate and connect to CIFS shares; however, enumeration of CIFS local users and groups is not allowed. Therefore, the \'cifs.restrict_anonymous\' option on a 7-Mode storage system will be ignored during transition.',
1900                                                                                  'luns' => {},
1901                                                                                  'level' => 'controller',
1902                                                                                  'correctiveaction' => '',
1903                                                                                  'volumes' => {},
1904                                                                                  'type' => 'Warning',
1905                                                                                  'operation' => 'CIFS Restrict Anonymous',
1906                                                                                  'category' => 'CIFS',
1907                                                                                  'code' => '25052'
1908                                                                                },
1909                                                                                {
1910                                                                                  'resources' => {
1911                                                                                                 'resource' => {
1912                                                                                                               'controlleripaddress' => '10.227.202.146',
1913                                                                                                               'resourcetype' => '7-Mode Option',
1914                                                                                                               'volume' => '',
1915                                                                                                               'lun' => '',
1916                                                                                                               'controller' => 'csiqa-6030-4',
1917                                                                                                               'objects' => {
1918                                                                                                                            'object' => {
1919                                                                                                                                        'name' => 'replication.throttle.enable'
1920                                                                                                                                      }
1921                                                                                                                          },
1922                                                                                                               'qtree' => '',
1923                                                                                                               'vfiler' => 'vfiler0',
1924                                                                                                               'aggregate' => ''
1925                                                                                                             },
1926                                                                                                 'resourcetype' => '7-Mode Option'
1927                                                                                               },
1928                                                                                  'qtrees' => {},
1929                                                                                  'message' => 'The \'replication.throttle.enable\' option is enabled on the 7-Mode storage system. However, this option is not supported in clustered Data ONTAP.',
1930                                                                                  'luns' => {},
1931                                                                                  'level' => 'controller',
1932                                                                                  'correctiveaction' => '',
1933                                                                                  'volumes' => {},
1934                                                                                  'type' => 'Warning',
1935                                                                                  'operation' => 'Replication Throttle',
1936                                                                                  'category' => 'Data Protection',
1937                                                                                  'message-tags' => {
1938                                                                                                    'tag' => {
1939                                                                                                             'name' => '7-Mode Transition Tool Specific'
1940                                                                                                           }
1941                                                                                                  },
1942                                                                                  'code' => '60512'
1943                                                                                },
1944                                                                                {
1945                                                                                  'resources' => {
1946                                                                                                 'resource' => {
1947                                                                                                               'controlleripaddress' => '10.227.202.146',
1948                                                                                                               'resourcetype' => '7-Mode Option',
1949                                                                                                               'volume' => '',
1950                                                                                                               'lun' => '',
1951                                                                                                               'controller' => 'csiqa-6030-4',
1952                                                                                                               'objects' => {
1953                                                                                                                            'object' => {
1954                                                                                                                                        'name' => 'snapmirror.checkip.enable'
1955                                                                                                                                      }
1956                                                                                                                          },
1957                                                                                                               'qtree' => '',
1958                                                                                                               'vfiler' => 'vfiler0',
1959                                                                                                               'aggregate' => ''
1960                                                                                                             },
1961                                                                                                 'resourcetype' => '7-Mode Option'
1962                                                                                               },
1963                                                                                  'qtrees' => {},
1964                                                                                  'message' => 'The \'snapmirror.checkip.enable\' option is enabled on the 7-Mode storage system. However, this option is not supported in clustered Data ONTAP.',
1965                                                                                  'luns' => {},
1966                                                                                  'level' => 'controller',
1967                                                                                  'correctiveaction' => '',
1968                                                                                  'volumes' => {},
1969                                                                                  'type' => 'Warning',
1970                                                                                  'operation' => 'SnapMirror Check IP',
1971                                                                                  'category' => 'Data Protection',
1972                                                                                  'message-tags' => {
1973                                                                                                    'tag' => {
1974                                                                                                             'name' => '7-Mode Transition Tool Specific'
1975                                                                                                           }
1976                                                                                                  },
1977                                                                                  'code' => '60542'
1978                                                                                },
1979                                                                                {
1980                                                                                  'resources' => {
1981                                                                                                 'resource' => {
1982                                                                                                               'controlleripaddress' => '10.227.202.146',
1983                                                                                                               'resourcetype' => '7-Mode Option',
1984                                                                                                               'volume' => '',
1985                                                                                                               'lun' => '',
1986                                                                                                               'controller' => 'csiqa-6030-4',
1987                                                                                                               'objects' => {
1988                                                                                                                            'object' => {
1989                                                                                                                                        'name' => 'nfs.netgroup.strict'
1990                                                                                                                                      }
1991                                                                                                                          },
1992                                                                                                               'qtree' => '',
1993                                                                                                               'vfiler' => 'vfiler0',
1994                                                                                                               'aggregate' => ''
1995                                                                                                             },
1996                                                                                                 'resourcetype' => '7-Mode Option'
1997                                                                                               },
1998                                                                                  'qtrees' => {},
1999                                                                                  'message' => 'The \'nfs.netgroup.strict\' option is disabled on the 7-Mode storage system \'vfiler0@csiqa-6030-4\'. Therefore, the netgroup entries in the \'etc/exports\' file might not have the \'@\' prefix. But, in clustered data ONTAP, netgroups must have the \'@\' prefix.',
2000                                                                                  'luns' => {},
2001                                                                                  'level' => 'controller',
2002                                                                                  'correctiveaction' => 'Edit the \'/etc/exports\' file on 7-Mode storage system to ensure that the netgroups have \'@\' prefix.',
2003                                                                                  'volumes' => {},
2004                                                                                  'type' => 'Warning',
2005                                                                                  'operation' => 'Netgroup strict verification',
2006                                                                                  'category' => 'NFS',
2007                                                                                  'code' => '10031'
2008                                                                                },
2009                                                                                {
2010                                                                                  'resources' => {
2011                                                                                                 'resource' => {
2012                                                                                                               'controlleripaddress' => '10.227.202.146',
2013                                                                                                               'resourcetype' => '',
2014                                                                                                               'volume' => '',
2015                                                                                                               'lun' => '',
2016                                                                                                               'controller' => 'csiqa-6030-4',
2017                                                                                                               'objects' => {},
2018                                                                                                               'qtree' => '',
2019                                                                                                               'vfiler' => 'vfiler0',
2020                                                                                                               'aggregate' => ''
2021                                                                                                             },
2022                                                                                                 'resourcetype' => ''
2023                                                                                               },
2024                                                                                  'qtrees' => {},
2025                                                                                  'message' => 'In a 7-Mode storage system, sub-domains in the \'/etc/exports\' file can begin with a \'.\' prefix or no qualifier at all. However, in clustered Data ONTAP, sub-domains in the export rules must always begin with a \'.\' prefix.',
2026                                                                                  'luns' => {},
2027                                                                                  'level' => 'controller',
2028                                                                                  'correctiveaction' => 'Edit the \'/etc/exports\' file on the 7-Mode storage system \'vfiler0@csiqa-6030-4\' and prefix all the sub-domains with a \'.\'.',
2029                                                                                  'volumes' => {},
2030                                                                                  'type' => 'Warning',
2031                                                                                  'operation' => 'NFS Subdomains',
2032                                                                                  'category' => 'NFS',
2033                                                                                  'code' => '10041'
2034                                                                                },
2035                                                                                {
2036                                                                                  'resources' => {
2037                                                                                                 'resource' => {
2038                                                                                                               'controlleripaddress' => '10.227.202.146',
2039                                                                                                               'resourcetype' => '',
2040                                                                                                               'volume' => '',
2041                                                                                                               'lun' => '',
2042                                                                                                               'controller' => 'csiqa-6030-4',
2043                                                                                                               'objects' => {},
2044                                                                                                               'qtree' => '',
2045                                                                                                               'vfiler' => 'vfiler0',
2046                                                                                                               'aggregate' => ''
2047                                                                                                             },
2048                                                                                                 'resourcetype' => ''
2049                                                                                               },
2050                                                                                  'qtrees' => {},
2051                                                                                  'message' => 'The showmount {-a/-e} command, which provides information about the exports and the mounted paths, is not supported in clustered Data ONTAP.',
2052                                                                                  'luns' => {},
2053                                                                                  'level' => 'controller',
2054                                                                                  'correctiveaction' => '',
2055                                                                                  'volumes' => {},
2056                                                                                  'type' => 'Warning',
2057                                                                                  'operation' => 'NFS showmount',
2058                                                                                  'category' => 'NFS',
2059                                                                                  'code' => '10421'
2060                                                                                },
2061                                                                                {
2062                                                                                  'resources' => {
2063                                                                                                 'resource' => {
2064                                                                                                               'controlleripaddress' => '10.227.202.146',
2065                                                                                                               'resourcetype' => '',
2066                                                                                                               'volume' => '',
2067                                                                                                               'lun' => '',
2068                                                                                                               'controller' => 'csiqa-6030-4',
2069                                                                                                               'objects' => {},
2070                                                                                                               'qtree' => '',
2071                                                                                                               'vfiler' => 'vfiler0',
2072                                                                                                               'aggregate' => ''
2073                                                                                                             },
2074                                                                                                 'resourcetype' => ''
2075                                                                                               },
2076                                                                                  'qtrees' => {},
2077                                                                                  'message' => 'Clustered Data ONTAP doesn\'t support any exports options that allow fencing of specific NFS clients from specific file system paths.',
2078                                                                                  'luns' => {},
2079                                                                                  'level' => 'controller',
2080                                                                                  'correctiveaction' => '',
2081                                                                                  'volumes' => {},
2082                                                                                  'type' => 'Warning',
2083                                                                                  'operation' => 'NFS Exports Fencing',
2084                                                                                  'category' => 'NFS',
2085                                                                                  'code' => '10441'
2086                                                                                },
2087                                                                                {
2088                                                                                  'resources' => {
2089                                                                                                 'resource' => {
2090                                                                                                               'controlleripaddress' => '10.227.202.146',
2091                                                                                                               'resourcetype' => '7-Mode Option',
2092                                                                                                               'volume' => '',
2093                                                                                                               'lun' => '',
2094                                                                                                               'controller' => 'csiqa-6030-4',
2095                                                                                                               'objects' => {
2096                                                                                                                            'object' => {
2097                                                                                                                                        'name' => 'nfs.v2.enable'
2098                                                                                                                                      }
2099                                                                                                                          },
2100                                                                                                               'qtree' => '',
2101                                                                                                               'vfiler' => 'vfiler0',
2102                                                                                                               'aggregate' => ''
2103                                                                                                             },
2104                                                                                                 'resourcetype' => '7-Mode Option'
2105                                                                                               },
2106                                                                                  'qtrees' => {},
2107                                                                                  'message' => 'The \'option nfs.v2.enable\' option is enabled on the 7-Mode storage system. However, NFSv2 is not supported in clustered Data ONTAP.',
2108                                                                                  'luns' => {},
2109                                                                                  'level' => 'controller',
2110                                                                                  'correctiveaction' => 'Use NFSv3 or later for any NFS configurations in clustered Data ONTAP.',
2111                                                                                  'volumes' => {},
2112                                                                                  'type' => 'Warning',
2113                                                                                  'operation' => 'NFSv2 Enable',
2114                                                                                  'category' => 'NFS',
2115                                                                                  'code' => '10712'
2116                                                                                },
2117                                                                                {
2118                                                                                  'resources' => {
2119                                                                                                 'resource' => {
2120                                                                                                               'controlleripaddress' => '10.227.202.146',
2121                                                                                                               'resourcetype' => '7-Mode Option',
2122                                                                                                               'volume' => '',
2123                                                                                                               'lun' => '',
2124                                                                                                               'controller' => 'csiqa-6030-4',
2125                                                                                                               'objects' => {
2126                                                                                                                            'object' => {
2127                                                                                                                                        'name' => 'dns.cache.enable'
2128                                                                                                                                      }
2129                                                                                                                          },
2130                                                                                                               'qtree' => '',
2131                                                                                                               'vfiler' => 'vfiler0',
2132                                                                                                               'aggregate' => ''
2133                                                                                                             },
2134                                                                                                 'resourcetype' => '7-Mode Option'
2135                                                                                               },
2136                                                                                  'qtrees' => {},
2137                                                                                  'message' => 'DNS caching (\'dns.cache.enable\' option) is enabled on 7-Mode storage system. However, DNS caching is not supported in clustered Data ONTAP.',
2138                                                                                  'luns' => {},
2139                                                                                  'level' => 'controller',
2140                                                                                  'correctiveaction' => '',
2141                                                                                  'volumes' => {},
2142                                                                                  'type' => 'Warning',
2143                                                                                  'operation' => 'DNS Cache',
2144                                                                                  'category' => 'Nameservices',
2145                                                                                  'code' => '40512'
2146                                                                                },
2147                                                                                {
2148                                                                                  'resources' => {
2149                                                                                                 'resource' => {
2150                                                                                                               'controlleripaddress' => '10.227.202.146',
2151                                                                                                               'resourcetype' => '7-Mode Option',
2152                                                                                                               'volume' => '',
2153                                                                                                               'lun' => '',
2154                                                                                                               'controller' => 'csiqa-6030-4',
2155                                                                                                               'objects' => {
2156                                                                                                                            'object' => {
2157                                                                                                                                        'name' => 'dns.update.enable'
2158                                                                                                                                      }
2159                                                                                                                          },
2160                                                                                                               'qtree' => '',
2161                                                                                                               'vfiler' => 'vfiler0',
2162                                                                                                               'aggregate' => ''
2163                                                                                                             },
2164                                                                                                 'resourcetype' => '7-Mode Option'
2165                                                                                               },
2166                                                                                  'qtrees' => {},
2167                                                                                  'message' => 'Dynamic DNS (\'dns.update.enable\' option) is enabled on 7-Mode storage system. However, Dynamic DNS is not supported in clustered Data ONTAP.',
2168                                                                                  'luns' => {},
2169                                                                                  'level' => 'controller',
2170                                                                                  'correctiveaction' => '',
2171                                                                                  'volumes' => {},
2172                                                                                  'type' => 'Warning',
2173                                                                                  'operation' => 'Dynamic DNS',
2174                                                                                  'category' => 'Nameservices',
2175                                                                                  'code' => '40522'
2176                                                                                },
2177                                                                                {
2178                                                                                  'resources' => {
2179                                                                                                 'resource' => {
2180                                                                                                               'controlleripaddress' => '10.227.202.146',
2181                                                                                                               'resourcetype' => '7-Mode Option',
2182                                                                                                               'volume' => '',
2183                                                                                                               'lun' => '',
2184                                                                                                               'controller' => 'csiqa-6030-4',
2185                                                                                                               'objects' => {
2186                                                                                                                            'object' => {
2187                                                                                                                                        'name' => 'nis.netgroup.domain_search.enable'
2188                                                                                                                                      }
2189                                                                                                                          },
2190                                                                                                               'qtree' => '',
2191                                                                                                               'vfiler' => 'vfiler0',
2192                                                                                                               'aggregate' => ''
2193                                                                                                             },
2194                                                                                                 'resourcetype' => '7-Mode Option'
2195                                                                                               },
2196                                                                                  'qtrees' => {},
2197                                                                                  'message' => 'The \'nis.netgroup.domain_search.enable\' option is enabled on the 7-Mode storage system. However, considering the domain names in search directives from the \'/etc/resolv.conf\' file while doing netgroup entry comparison is not supported in clustered Data ONTAP.',
2198                                                                                  'luns' => {},
2199                                                                                  'level' => 'controller',
2200                                                                                  'correctiveaction' => '',
2201                                                                                  'volumes' => {},
2202                                                                                  'type' => 'Warning',
2203                                                                                  'operation' => 'Netgroup Domain Search',
2204                                                                                  'category' => 'Nameservices',
2205                                                                                  'code' => '40532'
2206                                                                                },
2207                                                                                {
2208                                                                                  'resources' => {
2209                                                                                                 'resource' => {
2210                                                                                                               'controlleripaddress' => '10.227.202.146',
2211                                                                                                               'resourcetype' => '7-Mode Option',
2212                                                                                                               'volume' => '',
2213                                                                                                               'lun' => '',
2214                                                                                                               'controller' => 'csiqa-6030-4',
2215                                                                                                               'objects' => {
2216                                                                                                                            'object' => {
2217                                                                                                                                        'name' => 'nis.netgroup.legacy_nisdomain_search.enable'
2218                                                                                                                                      }
2219                                                                                                                          },
2220                                                                                                               'qtree' => '',
2221                                                                                                               'vfiler' => 'vfiler0',
2222                                                                                                               'aggregate' => ''
2223                                                                                                             },
2224                                                                                                 'resourcetype' => '7-Mode Option'
2225                                                                                               },
2226                                                                                  'qtrees' => {},
2227                                                                                  'message' => 'The \'nis.netgroup.legacy_nisdomain_search.enable\' option is enabled on the 7-Mode storage system. However, considering legacy SUNOS compatible nisdomainname in search directive for netgroup entry comparison is not supported in clustered Data ONTAP.',
2228                                                                                  'luns' => {},
2229                                                                                  'level' => 'controller',
2230                                                                                  'correctiveaction' => '',
2231                                                                                  'volumes' => {},
2232                                                                                  'type' => 'Warning',
2233                                                                                  'operation' => 'Netgroup NIS Domain Name',
2234                                                                                  'category' => 'Nameservices',
2235                                                                                  'code' => '40542'
2236                                                                                },
2237                                                                                {
2238                                                                                  'resources' => {
2239                                                                                                 'resource' => {
2240                                                                                                               'controlleripaddress' => '10.227.202.146',
2241                                                                                                               'resourcetype' => '7-Mode Option',
2242                                                                                                               'volume' => '',
2243                                                                                                               'lun' => '',
2244                                                                                                               'controller' => 'csiqa-6030-4',
2245                                                                                                               'objects' => {
2246                                                                                                                            'object' => {
2247                                                                                                                                        'name' => 'nis.slave.enable'
2248                                                                                                                                      }
2249                                                                                                                          },
2250                                                                                                               'qtree' => '',
2251                                                                                                               'vfiler' => 'vfiler0',
2252                                                                                                               'aggregate' => ''
2253                                                                                                             },
2254                                                                                                 'resourcetype' => '7-Mode Option'
2255                                                                                               },
2256                                                                                  'qtrees' => {},
2257                                                                                  'message' => 'NIS slave (\'nis.slave.enable\' option) is enabled on 7-Mode storage system. However, NIS slave is not supported in clustered Data ONTAP.',
2258                                                                                  'luns' => {},
2259                                                                                  'level' => 'controller',
2260                                                                                  'correctiveaction' => '',
2261                                                                                  'volumes' => {},
2262                                                                                  'type' => 'Warning',
2263                                                                                  'operation' => 'NIS Slave',
2264                                                                                  'category' => 'Nameservices',
2265                                                                                  'code' => '40552'
2266                                                                                },
2267                                                                                {
2268                                                                                  'resources' => {
2269                                                                                                 'resource' => {
2270                                                                                                               'controlleripaddress' => '10.227.202.146',
2271                                                                                                               'resourcetype' => '',
2272                                                                                                               'volume' => '',
2273                                                                                                               'lun' => '',
2274                                                                                                               'controller' => 'csiqa-6030-4',
2275                                                                                                               'objects' => {},
2276                                                                                                               'qtree' => '',
2277                                                                                                               'vfiler' => 'vfiler0',
2278                                                                                                               'aggregate' => ''
2279                                                                                                             },
2280                                                                                                 'resourcetype' => ''
2281                                                                                               },
2282                                                                                  'qtrees' => {},
2283                                                                                  'message' => 'Routing Information Protocol(RIP) is enabled on the 7-Mode storage system.',
2284                                                                                  'luns' => {},
2285                                                                                  'level' => 'controller',
2286                                                                                  'correctiveaction' => 'Routing Information Protocol(RIP) and ICMP Router Discovery Protocol are not supported in clustered Data ONTAP. Routes added by Routing Information Protocol(RIP) should be added manually. For more information, refer to the Clustered Data ONTAP Network Management Guide.',
2287                                                                                  'volumes' => {},
2288                                                                                  'type' => 'Warning',
2289                                                                                  'operation' => 'RIP Status',
2290                                                                                  'category' => 'Networking',
2291                                                                                  'code' => '30041'
2292                                                                                },
2293                                                                                {
2294                                                                                  'resources' => {
2295                                                                                                 'resource' => {
2296                                                                                                               'controlleripaddress' => '10.227.202.146',
2297                                                                                                               'resourcetype' => '',
2298                                                                                                               'volume' => '',
2299                                                                                                               'lun' => '',
2300                                                                                                               'controller' => 'csiqa-6030-4',
2301                                                                                                               'objects' => {},
2302                                                                                                               'qtree' => '',
2303                                                                                                               'vfiler' => 'vfiler0',
2304                                                                                                               'aggregate' => ''
2305                                                                                                             },
2306                                                                                                 'resourcetype' => ''
2307                                                                                               },
2308                                                                                  'qtrees' => {},
2309                                                                                  'message' => 'Default router discovery is configured on the 7-Mode storage system. However, this feature is not supported in the clustered Data ONTAP.',
2310                                                                                  'luns' => {},
2311                                                                                  'level' => 'controller',
2312                                                                                  'correctiveaction' => '',
2313                                                                                  'volumes' => {},
2314                                                                                  'type' => 'Warning',
2315                                                                                  'operation' => 'RTFO Check',
2316                                                                                  'category' => 'Networking',
2317                                                                                  'code' => '30141'
2318                                                                                },
2319                                                                                {
2320                                                                                  'resources' => {
2321                                                                                                 'resource' => {
2322                                                                                                               'controlleripaddress' => '10.227.202.146',
2323                                                                                                               'resourcetype' => '7-Mode Option',
2324                                                                                                               'volume' => '',
2325                                                                                                               'lun' => '',
2326                                                                                                               'controller' => 'csiqa-6030-4',
2327                                                                                                               'objects' => {
2328                                                                                                                            'object' => {
2329                                                                                                                                        'name' => 'tftpd.enable'
2330                                                                                                                                      }
2331                                                                                                                          },
2332                                                                                                               'qtree' => '',
2333                                                                                                               'vfiler' => 'vfiler0',
2334                                                                                                               'aggregate' => ''
2335                                                                                                             },
2336                                                                                                 'resourcetype' => '7-Mode Option'
2337                                                                                               },
2338                                                                                  'qtrees' => {},
2339                                                                                  'message' => 'TFTP server is configured on the 7-Mode storage system. However, TFTP server is not supported in the clustered Data ONTAP.',
2340                                                                                  'luns' => {},
2341                                                                                  'level' => 'controller',
2342                                                                                  'correctiveaction' => '',
2343                                                                                  'volumes' => {},
2344                                                                                  'type' => 'Warning',
2345                                                                                  'operation' => 'TFTP Check',
2346                                                                                  'category' => 'Networking',
2347                                                                                  'code' => '30161'
2348                                                                                },
2349                                                                                {
2350                                                                                  'resources' => {
2351                                                                                                 'resource' => {
2352                                                                                                               'controlleripaddress' => '10.227.202.146',
2353                                                                                                               'resourcetype' => '7-Mode Option',
2354                                                                                                               'volume' => '',
2355                                                                                                               'lun' => '',
2356                                                                                                               'controller' => 'csiqa-6030-4',
2357                                                                                                               'objects' => {
2358                                                                                                                            'object' => {
2359                                                                                                                                        'name' => 'sftp.enable'
2360                                                                                                                                      }
2361                                                                                                                          },
2362                                                                                                               'qtree' => '',
2363                                                                                                               'vfiler' => 'vfiler0',
2364                                                                                                               'aggregate' => ''
2365                                                                                                             },
2366                                                                                                 'resourcetype' => '7-Mode Option'
2367                                                                                               },
2368                                                                                  'qtrees' => {},
2369                                                                                  'message' => 'SFTP server is configured on the 7-Mode storage system. However, SFTP server is not supported in the clustered Data ONTAP.',
2370                                                                                  'luns' => {},
2371                                                                                  'level' => 'controller',
2372                                                                                  'correctiveaction' => '',
2373                                                                                  'volumes' => {},
2374                                                                                  'type' => 'Warning',
2375                                                                                  'operation' => 'SFTP Check',
2376                                                                                  'category' => 'Networking',
2377                                                                                  'code' => '30171'
2378                                                                                },
2379                                                                                {
2380                                                                                  'resources' => {
2381                                                                                                 'resource' => {
2382                                                                                                               'controlleripaddress' => '10.227.202.146',
2383                                                                                                               'resourcetype' => '7-Mode Option',
2384                                                                                                               'volume' => '',
2385                                                                                                               'lun' => '',
2386                                                                                                               'controller' => 'csiqa-6030-4',
2387                                                                                                               'objects' => {
2388                                                                                                                            'object' => {
2389                                                                                                                                        'name' => 'ip.v6.enable'
2390                                                                                                                                      }
2391                                                                                                                          },
2392                                                                                                               'qtree' => '',
2393                                                                                                               'vfiler' => 'vfiler0',
2394                                                                                                               'aggregate' => ''
2395                                                                                                             },
2396                                                                                                 'resourcetype' => '7-Mode Option'
2397                                                                                               },
2398                                                                                  'qtrees' => {},
2399                                                                                  'message' => 'IPv6 (\'ip.v6.enable\' option) is enabled in the 7-Mode storage system. However, 7-Mode Transition Tool does not support the transition of IPv6 configurations.',
2400                                                                                  'luns' => {},
2401                                                                                  'level' => 'controller',
2402                                                                                  'correctiveaction' => 'IPv6 configuration must be manually configured on the destination SVM. For more information, see the clustered Data ONTAP Network Management Guide.',
2403                                                                                  'volumes' => {},
2404                                                                                  'type' => 'Warning',
2405                                                                                  'operation' => 'IP V6',
2406                                                                                  'category' => 'Networking',
2407                                                                                  'message-tags' => {
2408                                                                                                    'tag' => {
2409                                                                                                             'name' => '7-Mode Transition Tool Configuration Transition Gap'
2410                                                                                                           }
2411                                                                                                  },
2412                                                                                  'code' => '30512'
2413                                                                                },
2414                                                                                {
2415                                                                                  'resources' => {
2416                                                                                                 'resource' => {
2417                                                                                                               'controlleripaddress' => '10.227.202.146',
2418                                                                                                               'resourcetype' => '7-Mode Option',
2419                                                                                                               'volume' => '',
2420                                                                                                               'lun' => '',
2421                                                                                                               'controller' => 'csiqa-6030-4',
2422                                                                                                               'objects' => {
2423                                                                                                                            'object' => {
2424                                                                                                                                        'name' => 'ip.ipsec.enable'
2425                                                                                                                                      }
2426                                                                                                                          },
2427                                                                                                               'qtree' => '',
2428                                                                                                               'vfiler' => 'vfiler0',
2429                                                                                                               'aggregate' => ''
2430                                                                                                             },
2431                                                                                                 'resourcetype' => '7-Mode Option'
2432                                                                                               },
2433                                                                                  'qtrees' => {},
2434                                                                                  'message' => 'IPsec (\'ip.ipsec.enable\' option) is enabled in the 7-Mode storage system. However, this feature is not supported in the clustered Data ONTAP.',
2435                                                                                  'luns' => {},
2436                                                                                  'level' => 'controller',
2437                                                                                  'correctiveaction' => '',
2438                                                                                  'volumes' => {},
2439                                                                                  'type' => 'Warning',
2440                                                                                  'operation' => 'IP IPSec',
2441                                                                                  'category' => 'Networking',
2442                                                                                  'code' => '30522'
2443                                                                                },
2444                                                                                {
2445                                                                                  'resources' => {
2446                                                                                                 'resource' => {
2447                                                                                                               'controlleripaddress' => '10.227.202.146',
2448                                                                                                               'resourcetype' => '7-Mode Option',
2449                                                                                                               'volume' => '',
2450                                                                                                               'lun' => '',
2451                                                                                                               'controller' => 'csiqa-6030-4',
2452                                                                                                               'objects' => {
2453                                                                                                                            'object' => {
2454                                                                                                                                        'name' => 'ip.drd.ping_interval'
2455                                                                                                                                      }
2456                                                                                                                          },
2457                                                                                                               'qtree' => '',
2458                                                                                                               'vfiler' => 'vfiler0',
2459                                                                                                               'aggregate' => ''
2460                                                                                                             },
2461                                                                                                 'resourcetype' => '7-Mode Option'
2462                                                                                               },
2463                                                                                  'qtrees' => {},
2464                                                                                  'message' => 'Periodic pinging of default routers (\'ip.drd.ping_interval\' option) is enabled in the 7-Mode storage system. However, this feature is not supported in the clustered Data ONTAP.',
2465                                                                                  'luns' => {},
2466                                                                                  'level' => 'controller',
2467                                                                                  'correctiveaction' => '',
2468                                                                                  'volumes' => {},
2469                                                                                  'type' => 'Warning',
2470                                                                                  'operation' => 'IP DRD Ping Interval',
2471                                                                                  'category' => 'Networking',
2472                                                                                  'code' => '30532'
2473                                                                                },
2474                                                                                {
2475                                                                                  'resources' => {
2476                                                                                                 'resource' => {
2477                                                                                                               'controlleripaddress' => '10.227.202.146',
2478                                                                                                               'resourcetype' => '7-Mode Option',
2479                                                                                                               'volume' => '',
2480                                                                                                               'lun' => '',
2481                                                                                                               'controller' => 'csiqa-6030-4',
2482                                                                                                               'objects' => {
2483                                                                                                                            'object' => {
2484                                                                                                                                        'name' => 'ip.drd.ping_retry'
2485                                                                                                                                      }
2486                                                                                                                          },
2487                                                                                                               'qtree' => '',
2488                                                                                                               'vfiler' => 'vfiler0',
2489                                                                                                               'aggregate' => ''
2490                                                                                                             },
2491                                                                                                 'resourcetype' => '7-Mode Option'
2492                                                                                               },
2493                                                                                  'qtrees' => {},
2494                                                                                  'message' => 'Pinging dead routers (\'ip.drd.ping_retry\' option) is enabled in the 7-Mode storage system. However, this feature is not supported in the clustered Data ONTAP.',
2495                                                                                  'luns' => {},
2496                                                                                  'level' => 'controller',
2497                                                                                  'correctiveaction' => '',
2498                                                                                  'volumes' => {},
2499                                                                                  'type' => 'Warning',
2500                                                                                  'operation' => 'IP DRD Ping Retry',
2501                                                                                  'category' => 'Networking',
2502                                                                                  'code' => '30542'
2503                                                                                },
2504                                                                                {
2505                                                                                  'resources' => {
2506                                                                                                 'resource' => {
2507                                                                                                               'controlleripaddress' => '10.227.202.146',
2508                                                                                                               'resourcetype' => '7-Mode Option',
2509                                                                                                               'volume' => '',
2510                                                                                                               'lun' => '',
2511                                                                                                               'controller' => 'csiqa-6030-4',
2512                                                                                                               'objects' => {
2513                                                                                                                            'object' => {
2514                                                                                                                                        'name' => 'ip.fastpath.enable'
2515                                                                                                                                      }
2516                                                                                                                          },
2517                                                                                                               'qtree' => '',
2518                                                                                                               'vfiler' => 'vfiler0',
2519                                                                                                               'aggregate' => ''
2520                                                                                                             },
2521                                                                                                 'resourcetype' => '7-Mode Option'
2522                                                                                               },
2523                                                                                  'qtrees' => {},
2524                                                                                  'message' => 'Fastpath feature (\'ip.fastpath.enable\' option) is enabled in the 7-Mode storage system. However, this feature is not supported in the clustered Data ONTAP.',
2525                                                                                  'luns' => {},
2526                                                                                  'level' => 'controller',
2527                                                                                  'correctiveaction' => '',
2528                                                                                  'volumes' => {},
2529                                                                                  'type' => 'Warning',
2530                                                                                  'operation' => 'IP Fastpath',
2531                                                                                  'category' => 'Networking',
2532                                                                                  'code' => '30552'
2533                                                                                },
2534                                                                                {
2535                                                                                  'resources' => {
2536                                                                                                 'resource' => {
2537                                                                                                               'controlleripaddress' => '10.227.202.146',
2538                                                                                                               'resourcetype' => '',
2539                                                                                                               'volume' => '',
2540                                                                                                               'lun' => '',
2541                                                                                                               'controller' => 'csiqa-6030-4',
2542                                                                                                               'objects' => {},
2543                                                                                                               'qtree' => '',
2544                                                                                                               'vfiler' => 'vfiler0',
2545                                                                                                               'aggregate' => ''
2546                                                                                                             },
2547                                                                                                 'resourcetype' => ''
2548                                                                                               },
2549                                                                                  'qtrees' => {},
2550                                                                                  'message' => 'Non-Unicode directories on the transitioned volume will be converted to Unicode when needed (for example, when the directory is accessed using NFSv4 or CIFS). The conversion impacts performance of clustered DATA ONTAP system temporarily.',
2551                                                                                  'luns' => {},
2552                                                                                  'level' => 'controller',
2553                                                                                  'correctiveaction' => 'Consider scheduling conversion of large directories to Unicode during non-business hours or a maintenance window. See the NetApp support library article on Converting existing directories to Unicode format for more information.',
2554                                                                                  'volumes' => {},
2555                                                                                  'type' => 'Warning',
2556                                                                                  'operation' => 'Non-Unicode Direcotry Conversion',
2557                                                                                  'category' => 'WAFL',
2558                                                                                  'message-tags' => {
2559                                                                                                    'tag' => {
2560                                                                                                             'name' => '7-Mode Transition Tool Limitation'
2561                                                                                                           }
2562                                                                                                  },
2563                                                                                  'code' => '90701'
2564                                                                                },
2565                                                                                {
2566                                                                                  'resources' => {
2567                                                                                                 'resource' => {
2568                                                                                                               'controlleripaddress' => '10.227.202.146',
2569                                                                                                               'resourcetype' => '7-Mode Option',
2570                                                                                                               'volume' => '',
2571                                                                                                               'lun' => '',
2572                                                                                                               'controller' => 'csiqa-6030-4',
2573                                                                                                               'objects' => {
2574                                                                                                                            'object' => {
2575                                                                                                                                        'name' => 'ip.icmp_ignore_redirect.enable'
2576                                                                                                                                      }
2577                                                                                                                          },
2578                                                                                                               'qtree' => '',
2579                                                                                                               'vfiler' => 'vfiler0',
2580                                                                                                               'aggregate' => ''
2581                                                                                                             },
2582                                                                                                 'resourcetype' => '7-Mode Option'
2583                                                                                               },
2584                                                                                  'qtrees' => {},
2585                                                                                  'message' => 'Disabling ICMP redirects (\'ip.icmp_ignore_redirect.enable\' option) is enabled in the 7-Mode storage system. This feature is supported in the clustered Data ONTAP, however it uses the default value.',
2586                                                                                  'luns' => {},
2587                                                                                  'level' => 'controller',
2588                                                                                  'correctiveaction' => '',
2589                                                                                  'volumes' => {},
2590                                                                                  'type' => 'Informational',
2591                                                                                  'operation' => 'IP ICMP Ignore Redirect',
2592                                                                                  'category' => 'Networking',
2593                                                                                  'code' => '30562'
2594                                                                                },
2595                                                                                {
2596                                                                                  'resources' => {
2597                                                                                                 'resource' => {
2598                                                                                                               'controlleripaddress' => '10.227.202.146',
2599                                                                                                               'resourcetype' => '7-Mode Option',
2600                                                                                                               'volume' => '',
2601                                                                                                               'lun' => '',
2602                                                                                                               'controller' => 'csiqa-6030-4',
2603                                                                                                               'objects' => {
2604                                                                                                                            'object' => {
2605                                                                                                                                        'name' => 'ip.match_any_ifaddr'
2606                                                                                                                                      }
2607                                                                                                                          },
2608                                                                                                               'qtree' => '',
2609                                                                                                               'vfiler' => 'vfiler0',
2610                                                                                                               'aggregate' => ''
2611                                                                                                             },
2612                                                                                                 'resourcetype' => '7-Mode Option'
2613                                                                                               },
2614                                                                                  'qtrees' => {},
2615                                                                                  'message' => 'Feature to accept packet from any interface (\'ip.match_any_ifaddr\' option) is enabled in the 7-Mode storage system. This feature is supported in the clustered Data ONTAP, however it uses the default value.',
2616                                                                                  'luns' => {},
2617                                                                                  'level' => 'controller',
2618                                                                                  'correctiveaction' => '',
2619                                                                                  'volumes' => {},
2620                                                                                  'type' => 'Informational',
2621                                                                                  'operation' => 'IP Match Any Ifaddr',
2622                                                                                  'category' => 'Networking',
2623                                                                                  'code' => '30572'
2624                                                                                },
2625                                                                                {
2626                                                                                  'resources' => {
2627                                                                                                 'resource' => {
2628                                                                                                               'controlleripaddress' => '10.227.202.146',
2629                                                                                                               'resourcetype' => '7-Mode Option',
2630                                                                                                               'volume' => '',
2631                                                                                                               'lun' => '',
2632                                                                                                               'controller' => 'csiqa-6030-4',
2633                                                                                                               'objects' => {
2634                                                                                                                            'object' => {
2635                                                                                                                                        'name' => 'ip.path_mtu_discovery.enable'
2636                                                                                                                                      }
2637                                                                                                                          },
2638                                                                                                               'qtree' => '',
2639                                                                                                               'vfiler' => 'vfiler0',
2640                                                                                                               'aggregate' => ''
2641                                                                                                             },
2642                                                                                                 'resourcetype' => '7-Mode Option'
2643                                                                                               },
2644                                                                                  'qtrees' => {},
2645                                                                                  'message' => 'Discovery of MTU size of path for TCP connection (\'ip.path_mtu_discovery.enable\' option) is enabled in the 7-Mode storage system. This feature is supported in the clustered Data ONTAP, however it uses the default value.',
2646                                                                                  'luns' => {},
2647                                                                                  'level' => 'controller',
2648                                                                                  'correctiveaction' => '',
2649                                                                                  'volumes' => {},
2650                                                                                  'type' => 'Informational',
2651                                                                                  'operation' => 'IP Ping MTU Discovery',
2652                                                                                  'category' => 'Networking',
2653                                                                                  'code' => '30582'
2654                                                                                },
2655                                                                                {
2656                                                                                  'resources' => {
2657                                                                                                 'resource' => {
2658                                                                                                               'controlleripaddress' => '10.227.202.146',
2659                                                                                                               'resourcetype' => '7-Mode Option',
2660                                                                                                               'volume' => '',
2661                                                                                                               'lun' => '',
2662                                                                                                               'controller' => 'csiqa-6030-4',
2663                                                                                                               'objects' => {
2664                                                                                                                            'object' => {
2665                                                                                                                                        'name' => 'ip.ping_throttle.alarm_interval'
2666                                                                                                                                      }
2667                                                                                                                          },
2668                                                                                                               'qtree' => '',
2669                                                                                                               'vfiler' => 'vfiler0',
2670                                                                                                               'aggregate' => ''
2671                                                                                                             },
2672                                                                                                 'resourcetype' => '7-Mode Option'
2673                                                                                               },
2674                                                                                  'qtrees' => {},
2675                                                                                  'message' => 'Preventing ping floods denial of service attack (\'ip.ping_throttle.alarm_interval\' option) is enabled in the 7-Mode storage system. This feature is supported in the clustered Data ONTAP, however it uses the default value.',
2676                                                                                  'luns' => {},
2677                                                                                  'level' => 'controller',
2678                                                                                  'correctiveaction' => '',
2679                                                                                  'volumes' => {},
2680                                                                                  'type' => 'Informational',
2681                                                                                  'operation' => 'IP Ping Throttle Alarm Interval',
2682                                                                                  'category' => 'Networking',
2683                                                                                  'code' => '30592'
2684                                                                                },
2685                                                                                {
2686                                                                                  'resources' => {
2687                                                                                                 'resource' => {
2688                                                                                                               'controlleripaddress' => '10.227.202.146',
2689                                                                                                               'resourcetype' => '7-Mode Option',
2690                                                                                                               'volume' => '',
2691                                                                                                               'lun' => '',
2692                                                                                                               'controller' => 'csiqa-6030-4',
2693                                                                                                               'objects' => {
2694                                                                                                                            'object' => {
2695                                                                                                                                        'name' => 'ip.tcp.newreno.enable'
2696                                                                                                                                      }
2697                                                                                                                          },
2698                                                                                                               'qtree' => '',
2699                                                                                                               'vfiler' => 'vfiler0',
2700                                                                                                               'aggregate' => ''
2701                                                                                                             },
2702                                                                                                 'resourcetype' => '7-Mode Option'
2703                                                                                               },
2704                                                                                  'qtrees' => {},
2705                                                                                  'message' => 'Use of the NewReno modification to TCP\'s fast recovery algorithm (\'ip.tcp.newreno.enable\' option) is enabled in the 7-Mode storage system. This feature is supported in the clustered Data ONTAP, however it uses the default value.',
2706                                                                                  'luns' => {},
2707                                                                                  'level' => 'controller',
2708                                                                                  'correctiveaction' => '',
2709                                                                                  'volumes' => {},
2710                                                                                  'type' => 'Informational',
2711                                                                                  'operation' => 'IP TCP Newreno',
2712                                                                                  'category' => 'Networking',
2713                                                                                  'code' => '30602'
2714                                                                                },
2715                                                                                {
2716                                                                                  'resources' => {
2717                                                                                                 'resource' => {
2718                                                                                                               'controlleripaddress' => '10.227.202.146',
2719                                                                                                               'resourcetype' => '7-Mode Option',
2720                                                                                                               'volume' => '',
2721                                                                                                               'lun' => '',
2722                                                                                                               'controller' => 'csiqa-6030-4',
2723                                                                                                               'objects' => {
2724                                                                                                                            'object' => {
2725                                                                                                                                        'name' => 'ip.tcp.sack.enable'
2726                                                                                                                                      }
2727                                                                                                                          },
2728                                                                                                               'qtree' => '',
2729                                                                                                               'vfiler' => 'vfiler0',
2730                                                                                                               'aggregate' => ''
2731                                                                                                             },
2732                                                                                                 'resourcetype' => '7-Mode Option'
2733                                                                                               },
2734                                                                                  'qtrees' => {},
2735                                                                                  'message' => 'Use of TCP Selective Acknowledgements (\'ip.tcp.sack.enable\' option) is enabled in the 7-Mode storage system. This feature supported in the clustered Data ONTAP, however it uses the default value.',
2736                                                                                  'luns' => {},
2737                                                                                  'level' => 'controller',
2738                                                                                  'correctiveaction' => '',
2739                                                                                  'volumes' => {},
2740                                                                                  'type' => 'Informational',
2741                                                                                  'operation' => 'IP TCP Sack',
2742                                                                                  'category' => 'Networking',
2743                                                                                  'code' => '30612'
2744                                                                                }
2745                                                                              ]
2746                                                                 },
2747                                                     'volumes' => '1',
2748                                                     'name' => 'vfiler0'
2749                                                   },
2750                                         'serno' => '15396',
2751                                         'hostname' => '10.227.202.146',
2752                                         'sysmodel' => 'FAS6030'
2753                                       },
2754                         'targetversion' => '8.2.1'
2755                       }
2756         };";
    


my @arr = split("\n",$report);

use Data::Dumper;



foreach (@arr){
    $_ =~ s/\w+(.*)/$1/;
  
}



$someNames = join("\n", @arr);

print $someNames;
