function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

% Set K
K = size(centroids, 1);

% You need to return the following variables correctly.
idx = zeros(size(X,1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Go over every example, find its closest centroid, and store
%               the index inside idx at the appropriate location.
%               Concretely, idx(i) should contain the index of the centroid
%               closest to example i. Hence, it should be a value in the 
%               range 1..K
%
% Note: You can use a for-loop over the examples to compute this.
%


for i=1:size(X,1)
        
        % Calculate the distance of this ith training example from all the centroids
        closest_for_i = realmax;

        for j=1:size(centroids)
                
                a = X(i,:);
                b = centroids(j,:);
                c = a - b;
                c = c.^2;
                d = sum(c);
                if (d < closest_for_i) 
                        closest_for_i = d;
                        idx(i) = j;
                end
                
        
        end     


end





% =============================================================

end

