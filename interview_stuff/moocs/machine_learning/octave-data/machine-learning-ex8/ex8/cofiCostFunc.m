function [J, grad] = cofiCostFunc(params, Y, R, num_users, num_movies, ...
                                  num_features, lambda)
%COFICOSTFUNC Collaborative filtering cost function
%   [J, grad] = COFICOSTFUNC(params, Y, R, num_users, num_movies, ...
%   num_features, lambda) returns the cost and gradient for the
%   collaborative filtering problem.
%

% Unfold the U and W matrices from params
X = reshape(params(1:num_movies*num_features), num_movies, num_features);
Theta = reshape(params(num_movies*num_features+1:end), ...
                num_users, num_features);

            
% You need to return the following values correctly
J = 0;
X_grad = zeros(size(X));
Theta_grad = zeros(size(Theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost function and gradient for collaborative
%               filtering. Concretely, you should first implement the cost
%               function (without regularization) and make sure it is
%               matches our costs. After that, you should implement the 
%               gradient and use the checkCostFunction routine to check
%               that the gradient is correct. Finally, you should implement
%               regularization.
%
% Notes: X - num_movies  x num_features matrix of movie features
%        Theta - num_users  x num_features matrix of user features
%        Y - num_movies x num_users matrix of user ratings of movies
%        R - num_movies x num_users matrix, where R(i, j) = 1 if the 
%            i-th movie was rated by the j-th user
%
% You should set the following variables correctly:
%
%        X_grad - num_movies x num_features matrix, containing the 
%                 partial derivatives w.r.t. to each element of X
%        Theta_grad - num_users x num_features matrix, containing the 
%                     partial derivatives w.r.t. to each element of Theta
%

a = X * Theta';
b = a - Y;
c = b .^ 2;
d = c .* R;
e = sum(sum(d));

J = e/2;

% To compute X_grad
for i= 1:size(X,1)
        
        g = Theta * X(i,:)';
        h = g' - Y(i,:);
        temp1 = h .* R(i,:); % 4*1
        cost = Theta' * temp1'; % 3*4 * 4*1 

        X_grad(i,:) = cost; % should be of size 1*3
end



% To compute Theta_grad
for j= 1:size(Theta,1)
       
        % user = j = constant
 
        g1 = Theta(j,:) * X';
        h1 = g1' - Y(:,j);
        temp11 = h1 .* R(:,j); % 5*1
        cost = X' * temp11;  %  3*5 5*1 

        Theta_grad(j,:) = cost; % should be of size 1*3
end

squares = Theta.^2;
squares = (lambda/2) * squares;

squares_for_x = X.^2;
squares_for_x = (lambda/2) * squares_for_x;

J = J + sum(sum(squares)) + sum(sum(squares_for_x)); 

for i= 1:size(X,1)

        X_grad(i,:) = X_grad(i,:) + (lambda * X(i,:)); % should be of size 1*3

end

for j= 1:size(Theta,1)

        Theta_grad(j,:) = Theta_grad(j,:) + (lambda * Theta(j,:)); % should be of size 1*3
end
% =============================================================

grad = [X_grad(:); Theta_grad(:)];

end
