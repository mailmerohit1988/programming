function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta



h1 = X * theta;
h2 = sigmoid(h1);
h3 = log(h2);
h4 = log(1-h2);
h3 = y .* h3;
h3 = -h3;
h4 = (1-y) .* h4;
h4 = -h4;

h5 = h3+h4;


h6 = sum(h5);
h7 = h6 / m;

h8 = theta .^ 2;
h8 = sum(h8) - (theta(1)^2);
h9 = lambda / (2*m);
h10 = h8 * h9;

J = h7 + h10;

% Let us calculate Gradient now for this value of theta

g1 = h2 - y;
g2 = g1' * X;

g3 = g2 / m;

g4 = lambda * theta;
g5 = g4 / m;
g5(1) = 0;

grad = g3' + g5; 


% =============================================================

end
