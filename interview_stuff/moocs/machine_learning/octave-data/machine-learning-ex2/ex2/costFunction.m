function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%

h1 = X * theta;
h2 = sigmoid(h1);
h3 = log(h2);
h4 = log(1-h2);
h3 = y .* h3;
h3 = -h3;
h4 = (1-y) .* h4;
h4 = -h4;

h5 = h3+h4;


h6 = sum(h5);
J = h6 / m;


% Let us calculate Gradient now for this value of theta

g1 = h2 - y;
g2 = g1' * X;

grad = g2 / m;





% =============================================================

end
