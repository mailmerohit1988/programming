function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%

h1 = X * theta;
h2 = h1 - y;
h3 = h2 .^ 2;
h4 = sum(h3);
h5 = h4 ./ (2*m);

h6 = theta .^ 2;
h7 = sum(h6);
h8 = lambda * h7;
h9 = h8 / (2*m);
h10 = ((theta(1) ^ 2) * lambda ) / (2*m);
h11 = h9 - h10;

J = h5 + h11;

% lets calculate gradient

h10 = X' * h2;
h11 = h10 ./ m;

h12 = (lambda / m);
h13 = h12 * theta;

h14 = h13 + h11;
h14(1) = h11(1);

grad = h14;

% =========================================================================

grad = grad(:);

end
