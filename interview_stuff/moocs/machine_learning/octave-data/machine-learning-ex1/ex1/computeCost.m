function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples
r1 = X * theta;
r2 = r1 - y;
r2 = r2 .^ 2;
r3 = sum(r2);
r3 = r3 ./ (2*m);



% You need to return the following variables correctly 
J = r3;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.





% =========================================================================

end
