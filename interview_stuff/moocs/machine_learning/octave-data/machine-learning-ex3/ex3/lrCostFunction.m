function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Hint: The computation of the cost function and gradients can be
%       efficiently vectorized. For example, consider the computation
%
%           sigmoid(X * theta)
%
%       Each row of the resulting matrix will contain the value of the
%       prediction for that example. You can make use of this to vectorize
%       the cost function and gradient computations. 
%
% Hint: When computing the gradient of the regularized cost function, 
%       there're many possible vectorized solutions, but one solution
%       looks like:
%           grad = (unregularized gradient for logistic regression)
%           temp = theta; 
%           temp(1) = 0;   % because we don't add anything for j = 0  
%           grad = grad + YOUR_CODE_HERE (using the temp variable)
%

h1 = X * theta;
h2 = sigmoid(h1);
h3 = log(h2);

h4 = log(1-h2);
h5 = y .* h3;

h6 = (1-y) .* h4;

h5 = -h5;
h6 = -h6;

h7 = h5+h6;

h8 = sum(h7);

a1 = theta;
a1(1) = 0;
a2 = a1 .^ 2;
a3 = sum(a2);

a4 = lambda / (2*m);
a5 = a4 * a3;

J = (h8/m) + a5;


h9 = h2 - y;
h10 = X' * h9;

a5 = lambda / m;
a6 = theta * a5;
a6(1) = 0;

grad = (h10 / m) + a6;



% =============================================================


end
