function J = costFunctionJ(X, y, theta)

m = size(X,1) % no of training examples, number of rows... X is design matrix with first coloumn all 1. 

predictions = X*theta; % (m*2) * (2*1) = m*1 dimension matrix
sqrErrors = (predictions - y).^2

J = 1/(2*m) * sum(sqrErrors)




