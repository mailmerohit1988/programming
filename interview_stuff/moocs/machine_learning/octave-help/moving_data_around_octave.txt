octave:19> 
octave:19> A = [1 2; 3 4; 5 6]
A =

   1   2
   3   4
   5   6

octave:20> size(A)
ans =

   3   2

octave:21> sz = size(A)
sz =

   3   2

octave:22> size (sz)
ans =

   1   2

octave:23> size(A,1) // no of rows in matrix A
parse error:

  syntax error

>>> size(A,1) // no of rows in matrix A
               ^

octave:23> size(A,1) %no of rows in matrix A
ans =  3
octave:24> size(A,2) %no of cols in matrix A
ans =  2

octave:25> 
octave:25> V = [1 2 3 4]
V =

   1   2   3   4

octave:26> length(V) % returns length of longest vector
ans =  4
octave:27> length(A)
ans =  3


octave:32> pwd
ans = /home/rohit4/study/moocs/machine_learning
octave:33> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:35> who % shows what variable you have in your octave workspace
Variables in the current scope:

A    V    ans  sz   w

octave:36> 

octave:36> pwd
ans = /home/rohit4/study/moocs/machine_learning
octave:37> cd 
.moving_data_around_octave.txt.swp  moving_data_around_octave.txt       octave-data/                        octave_cmds.txt                     octave_help.txt                     
octave:37> cd octave-data
octave:38> pwd
ans = /home/rohit4/study/moocs/machine_learning/octave-data
octave:39> cd practice1/
octave:40> ls
featuresX  priceY
octave:41> 


octave:41> ls
featuresX  priceY
octave:42> load featuresX
octave:43> load priceY
octave:44> who
Variables in the current scope:

A          V          ans        featuresX  priceY     sz         w

octave:45> featuresX
featuresX =

   1000      2
   3000      4
   5000      6
   1111      3
    205      1

octave:46> priceY
priceY =

   100
    76
    23
    45
    98


octave:47> size(featuresX)
ans =

   5   2

octave:48> size(priceY)
ans =

   5   1


octave:49> whos
Variables in the current scope:

   Attr Name           Size                     Bytes  Class
   ==== ====           ====                     =====  ===== 
        A              3x2                         48  double
        V              1x4                         32  double
        ans            1x2                         16  double
        featuresX      5x2                         80  double
        priceY         5x1                         40  double
        sz             1x2                         16  double
        w              1x100000                800000  double

Total is 100029 elements using 800232 bytes

octave:50> clear featuresX
octave:51> who
Variables in the current scope:

A       V       ans     priceY  sz      w

octave:52> whos
Variables in the current scope:

   Attr Name        Size                     Bytes  Class
   ==== ====        ====                     =====  ===== 
        A           3x2                         48  double
        V           1x4                         32  double
        ans         1x2                         16  double
        priceY      5x1                         40  double
        sz          1x2                         16  double
        w           1x100000                800000  double

Total is 100019 elements using 800152 bytes

octave:53> 


octave:53> v = priceY(1:2) % v is first 2 elements of priceY
v =

   100
    76

octave:54> whos
Variables in the current scope:

   Attr Name        Size                     Bytes  Class
   ==== ====        ====                     =====  ===== 
        A           3x2                         48  double
        V           1x4                         32  double
        ans         1x2                         16  double
        priceY      5x1                         40  double
        sz          1x2                         16  double
        v           2x1                         16  double
        w           1x100000                800000  double

Total is 100021 elements using 800168 bytes

octave:55> 

root@debian:/home/rohit4/study/moocs/machine_learning/octave-data/practice1# touch hello.mat
root@debian:/home/rohit4/study/moocs/machine_learning/octave-data/practice1# ls -latri
total 16
177761 drwxr-xr-x 3 root root 4096 Jul 21 18:58 ..
172483 -rw-r--r-- 1 root root   35 Jul 21 18:59 featuresX
172484 -rw-r--r-- 1 root root   16 Jul 21 18:59 priceY
163350 -rw-r--r-- 1 root root    0 Jul 21 19:05 hello.mat
177762 drwxr-xr-x 2 root root 4096 Jul 21 19:05 .
root@debian:/home/rohit4/study/moocs/machine_learning/octave-data/practice1# chmod 777 *
root@debian:/home/rohit4/study/moocs/machine_learning/octave-data/practice1# ls -latri                                                                                                                                                       
total 16
177761 drwxr-xr-x 3 root root 4096 Jul 21 18:58 ..
172483 -rwxrwxrwx 1 root root   35 Jul 21 18:59 featuresX
172484 -rwxrwxrwx 1 root root   16 Jul 21 18:59 priceY
163350 -rwxrwxrwx 1 root root    0 Jul 21 19:05 hello.mat
177762 drwxr-xr-x 2 root root 4096 Jul 21 19:05 .
root@debian:/home/rohit4/study/moocs/machine_learning/octave-data/practice1# 

octave:56> save hello.mat v;




octave:59> clear % clears all variables
octave:60> whos


octave:61> a = [1 2; 3 4; 5 6]
a =

   1   2
   3   4
   5   6

octave:62> a(3, 2) % indexes into 3rd row 2nd col element in A                                                                                                                                                                               
ans =  6


octave:63> A(2,:) % : means every elemtn along that row/coloum
error: `A' undefined near line 63 column 1
octave:63> a(2,:) % : means every elemtn along that row/coloum                                                                                                                                                                               
ans =

   3   4

octave:64> a(:,2) % : means every elemtn along that row/coloum                                                                                                                                                                               
ans =

   2
   4
   6

octave:65> a(:,2) = [10; 11; 12] % : assinigng 2nd col with 10 11 12
a =

    1   10
    3   11
    5   12

octave:66> a
a =

    1   10
    3   11
    5   12



octave:67> a = [a, [100; 101; 102]] % appends another coloumn vector to right
a =

     1    10   100
     3    11   101
     5    12   102


octave:69> a(:) % puts all elements of a into a single coloumn vector
ans =

     1
     3
     5
    10
    11
    12
   100
   101
   102


octave:70> a
a =

     1    10   100
     3    11   101
     5    12   102

octave:71> b = eye(3)
b =

Diagonal Matrix

   1   0   0
   0   1   0
   0   0   1

octave:72> c = [a b]
c =

     1    10   100     1     0     0
     3    11   101     0     1     0
     5    12   102     0     0     1



octave:73> c = [a;  b] % semi colon means to go to the next line
c =

     1    10   100
     3    11   101
     5    12   102
     1     0     0
     0     1     0
     0     0     1




