package scalashop
import common._

object parMergeSortRunner {
  def main(args: Array[String]) {
    var arr = Array(9, 4, 3, 5, 6, 7, 1);
    parMergeSort.mergeSort(arr, 0, arr.length-1)
    for (i <- 0 to (arr.length -1)) {
      print(" ," + arr(i))
    }
    
  }  
}

object parMergeSort {
  
  def mergeSort(arr: Array[Int], start: Int, end: Int): Unit = {
    if (start < end) {
    	var m = start + (end-start)/2 //// Same as (start+end)/2, but avoids overflow for large start and end
    	parallel(mergeSort(arr, start, m), mergeSort(arr, m+1, end))
    	merge(arr, start, m, end)
    }
  }
  
  
  def merge(arr: Array[Int], start: Int, m: Int, end: Int): Unit = {
    // create two temp arrays
    var temp1Size = m - start +1
    var temp2Size = end - (m+1) + 1
    
    val temp1 = new Array[Int](temp1Size)
    val temp2 = new Array[Int](temp2Size)
    
    // Copy the elements into the two array
    parallel(Array.copy(arr, start, temp1, 0, temp1Size), Array.copy(arr, m+1, temp2, 0, temp2Size));
    
    var l = start;
    var j = 0
    var k = 0
    while (k < temp2Size && j < temp1Size) {
      if (temp1(j) <= temp2(k)) {
    	arr(l) = temp1(j) 
    	j = j + 1
      } else {
    	arr(l) = temp2(k)
    	k = k + 1
      }
    l = l + 1
    }
    
    // Copy remaining elements of temp1 or temp2 into arr
    while (j < temp1Size) {
      arr(l) = temp1(j)
      j = j + 1
      l = l + 1
    }
    while (k < temp2Size) {
      arr(l) = temp2(k)
      k = k + 1
      l = l + 1
    }
    
    
    
    
  }
  
}