#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int check_authentication(char *password) {
	int auth_flag = 0;
	char password_buffer[16];
	strcpy(password_buffer, password);
	if(strcmp(password_buffer, "brillig") == 0)
		auth_flag = 1;
	if(strcmp(password_buffer, "outgrabe") == 0)
		auth_flag = 1;
	return auth_flag;
}
int main(int argc, char *argv[]) {
	if(argc < 2) {
		printf("Usage: %s <password>\n", argv[0]);
		exit(0);
	}
	if(check_authentication(argv[1])) {
		printf("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
		printf(" Access Granted.\n");
		printf("-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
	} else {
		printf("\nAccess Denied.\n");
	}
}

/*
gdb) x/b 0xbffff589
0xbffff589:      "brillig"
(gdb) x/bc 0xbffff589
0xbffff589:     98 'b'
(gdb) x/8bc 0xbffff589
0xbffff589:     98 'b'  114 'r' 105 'i' 108 'l' 108 'l' 105 'i' 103 'g' 0 '\000'
(gdb) 
* 
* (gdb) x/s password
0xbffff589:      "brillig"
(gdb) x/s password_buffer
0xbffff2fc:      "brillig"
(gdb) 
(gdb) x/s password_buffer
0xbffff2cc:      'A' <repeats 46 times>, "a"
(gdb) x/16s password_buffer
0xbffff2cc:      'A' <repeats 46 times>, "a"
0xbffff2fc:      "\364?\374\267\340\205\004\b"
0xbffff305:      ""
0xbffff306:      ""
0xbffff307:      ""
0xbffff308:      "\210\363\377\277f\256\347\267\002"
0xbffff312:      ""
0xbffff313:      ""
0xbffff314:      "\264\363\377\277\300\363\377\277`\b\376\267!h\377\267\377\377\377\377\364\357\377\267\274\202\004\b\001"
0xbffff332:      ""
0xbffff333:      ""
0xbffff334:      "p\363\377\277\026\374\376\267\300\372\377\267X\v\376\267\364", <incomplete sequence \374\267>
0xbffff349:      ""
0xbffff34a:      ""
0xbffff34b:      ""
0xbffff34c:      ""
(gdb) x/16xw password_buffer
0xbffff2cc:     0x41414141      0x41414141      0x41414141      0x41414141
0xbffff2dc:     0x41414141      0x41414141      0x41414141      0x41414141
0xbffff2ec:     0x41414141      0x41414141      0x41414141      0x00614141
0xbffff2fc:     0xb7fc3ff4      0x080485e0      0x00000000      0xbffff388
* (gdb) x/x &auth_flag
0xbffff2dc:     0x41414141
(gdb) 

(gdb) 
* (gdb) x/4b &auth_flag 
0xbffff2dc:     65 'A'  65 'A'  65 'A'  65 'A'
(gdb) x/16xw password_buffer
0xbffff2cc:     0x41414141      0x41414141      0x41414141      0x41414141
0xbffff2dc:     0x41414141      0x41414141      0x41414141      0x41414141
0xbffff2ec:     0x41414141      0x41414141      0x41414141      0x00614141
0xbffff2fc:     0xb7fc3ff4      0x080485e0      0x00000000      0xbffff388

**************NOTE HERE THAT 41 IS HEXADECIMAL OF A AND 65 IS ASCII OF A******************************
* 
* 
* 

*/
