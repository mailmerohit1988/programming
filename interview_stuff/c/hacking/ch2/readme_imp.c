#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int check_authentication(char *password) {
	char password_buffer[16];
	int auth_flag = 0;
	strcpy(password_buffer, password);
	if(strcmp(password_buffer, "brillig") == 0)
		auth_flag = 1;
	if(strcmp(password_buffer, "outgrabe") == 0)
		auth_flag = 1;
	return auth_flag;
}
int main(int argc, char *argv[]) {
	if(argc < 2) {
		printf("Usage: %s <password>\n", argv[0]);
		exit(0);
	}
	if(check_authentication(argv[1])) {
		printf("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
		printf(" Access Granted.\n");
		printf("-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
	} else {
		printf("\nAccess Denied.\n");
	}
}

/*root@debian:/home/rohit4/c/hacking/ch2# gdb -q readme_imp
Reading symbols from /home/rohit4/c/hacking/ch2/readme_imp...done.
(gdb) break 20
Breakpoint 1 at 0x804857c: file readme_imp.c, line 20.
(gdb) break 8
Breakpoint 2 at 0x80484f9: file readme_imp.c, line 8.
(gdb) break 13
Breakpoint 3 at 0x8048547: file readme_imp.c, line 13.
(gdb) i r    esp
The program has no registers now.
(gdb) run AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Starting program: /home/rohit4/c/hacking/ch2/readme_imp AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

Breakpoint 1, main (argc=2, argv=0xbffff3c4) at readme_imp.c:20
20              if(check_authentication(argv[1])) {
(gdb) i r eso
Invalid register `eso'
(gdb) i r esp
esp            0xbffff300       0xbffff300
(gdb) x/32xw 0xbffff300
0xbffff300:     0xb7e938e5      0xb7ff0590      0x080485eb      0xb7fc3ff4
0xbffff310:     0x080485e0      0x00000000      0xbffff398      0xb7e7ae66
0xbffff320:     0x00000002      0xbffff3c4      0xbffff3d0      0xb7fe0860
0xbffff330:     0xb7ff6821      0xffffffff      0xb7ffeff4      0x080482bc
0xbffff340:     0x00000001      0xbffff380      0xb7fefc16      0xb7fffac0
0xbffff350:     0xb7fe0b58      0xb7fc3ff4      0x00000000      0x00000000
0xbffff360:     0xbffff398      0x9b7ac86a      0xabc0de7a      0x00000000
0xbffff370:     0x00000000      0x00000000      0x00000002      0x08048400
(gdb) c
Continuing.

Breakpoint 2, check_authentication (password=0xbffff575 'A' <repeats 30 times>) at readme_imp.c:8
8               strcpy(password_buffer, password);
(gdb) i r eso
Invalid register `eso'
(gdb) i r esp
esp            0xbffff2c0       0xbffff2c0
(gdb) x/32xw $esp
0xbffff2c0:     0xbffff2e6      0xb7e936e5      0xbffff2e7      0x00000001
0xbffff2d0:     0x00000000      0xbffff370      0xb7fc4ce0      0x08048358
0xbffff2e0:     0xb7ff0590      0x08049888      0xbffff318      0x00000000
0xbffff2f0:     0x00000002      0xbffff3c4      0xbffff318      0x0804858c >>>>>>>>>>>>>>>>>>>>>>>>> 0x0804858c is the return address
0xbffff300:     0xbffff575      0xb7ff0590      0x080485eb      0xb7fc3ff4
0xbffff310:     0x080485e0      0x00000000      0xbffff398      0xb7e7ae66
0xbffff320:     0x00000002      0xbffff3c4      0xbffff3d0      0xb7fe0860
0xbffff330:     0xb7ff6821      0xffffffff      0xb7ffeff4      0x080482bc
(gdb) x/s &auth_flag
0xbffff2ec:      ""
(gdb) x/s password_buffer
0xbffff2dc:      "X\203\004\b\220\005\377\267\210\230\004\b\030\363\377\277"
(gdb) x/s 0xbffff300
0xbffff300:      "u\365\377\277\220\005\377\267\353\205\004\b\364?\374\267\340\205\004\b"
(gdb) x/s 0xbffff303
0xbffff303:      "\277\220\005\377\267\353\205\004\b\364?\374\267\340\205\004\b"
(gdb) x/s 0xbffff308
0xbffff308:      "\353\205\004\b\364?\374\267\340\205\004\b"
(gdb) x/s 0xbffff304
0xbffff304:      "\220\005\377\267\353\205\004\b\364?\374\267\340\205\004\b"
(gdb) x/x 0xbffff304
0xbffff304:     0x90
(gdb) x/s 0xbffff575
0xbffff575:      'A' <repeats 30 times>
(gdb) disass main
Dump of assembler code for function main:
   0x0804854c <+0>:     push   ebp
   0x0804854d <+1>:     mov    ebp,esp
   0x0804854f <+3>:     and    esp,0xfffffff0
   0x08048552 <+6>:     sub    esp,0x10
   0x08048555 <+9>:     cmp    DWORD PTR [ebp+0x8],0x1
   0x08048559 <+13>:    jg     0x804857c <main+48>
   0x0804855b <+15>:    mov    eax,DWORD PTR [ebp+0xc]
   0x0804855e <+18>:    mov    eax,DWORD PTR [eax]
   0x08048560 <+20>:    mov    DWORD PTR [esp+0x4],eax
   0x08048564 <+24>:    mov    DWORD PTR [esp],0x8048671
   0x0804856b <+31>:    call   0x80483a0 <printf@plt>
   0x08048570 <+36>:    mov    DWORD PTR [esp],0x0
   0x08048577 <+43>:    call   0x80483e0 <exit@plt>
   0x0804857c <+48>:    mov    eax,DWORD PTR [ebp+0xc]
   0x0804857f <+51>:    add    eax,0x4
   0x08048582 <+54>:    mov    eax,DWORD PTR [eax]
   0x08048584 <+56>:    mov    DWORD PTR [esp],eax
   0x08048587 <+59>:    call   0x80484ec <check_authentication>
   0x0804858c <+64>:    test   eax,eax                              >>>>>>>>>>>>>>>>> 0x0804858c is the return address
   0x0804858e <+66>:    je     0x80485b6 <main+106>
   0x08048590 <+68>:    mov    DWORD PTR [esp],0x8048687
   0x08048597 <+75>:    call   0x80483c0 <puts@plt>
   0x0804859c <+80>:    mov    DWORD PTR [esp],0x80486a4
   0x080485a3 <+87>:    call   0x80483c0 <puts@plt>
   0x080485a8 <+92>:    mov    DWORD PTR [esp],0x80486b5
   0x080485af <+99>:    call   0x80483c0 <puts@plt>
   0x080485b4 <+104>:   jmp    0x80485c2 <main+118>
   0x080485b6 <+106>:   mov    DWORD PTR [esp],0x80486d1
   0x080485bd <+113>:   call   0x80483c0 <puts@plt>
   0x080485c2 <+118>:   leave  
   0x080485c3 <+119>:   ret    
End of assembler dump.
(gdb) ^Z
[19]+  Stopped                 gdb -q readme_imp
*/
