#include <stdio.h>

void usage(char *program_name) {
	printf("Usage: %s <message> <# of times to repeat>\n", program_name);
	exit(1);
}

int main(int argc, char *argv[]) {
	
	int i, count;

	printf("%s %s %s %s \n\n", argv[0], argv[1], argv[2], argv[3] );

	if(argc < 3) // If fewer than 3 arguments are used,
		usage(argv[0]); // display usage message and exit.
	
	count = atoi(argv[2]); // Convert the 2nd arg into an integer.
	
	printf("Repeating %d times..\n", count);
	
	for(i=0; i < count; i++)
		printf("%30d - %20s\n", i, argv[i]); // Print the 1st arg. To get the segmentation fault try reading the larger values of argv which are not supplied by the user

}

/*
 
 Output
 ===================
root@debian:/home/rohit4/c/hacking/ch1# gcc -o convert convert.c 
convert.c: In function ‘usage’:
convert.c:5:2: warning: incompatible implicit declaration of built-in function ‘exit’ [enabled by default]
^[[Aroot@debian:/home/rohit4/c/hacking/ch1# ./convert "Hello World" 30000
./convert Hello World 30000 (null) 

Repeating 30000 times..
                             0 -            ./convert
                             1 -          Hello World
                             2 -                30000
                             3 -               (null)
                             4 -  KDE_MULTIHEAD=false
                             5 -   SSH_AGENT_PID=3213
                             6 - DM_CONTROL=/var/run/xdmctl
                             7 -      SHELL=/bin/bash
                             8 -           TERM=xterm
                             9 - XDG_SESSION_COOKIE=5579209dc20a00bbc295e2c253521fbe-1404922732.471771-1675173998
                            10 - XDM_MANAGED=method=classic
                            11 - KONSOLE_DBUS_SERVICE=:1.43
                            12 - GTK2_RC_FILES=/etc/gtk-2.0/gtkrc:/home/rohit4/.gtkrc-2.0:/home/rohit4/.kde/share/config/gtkrc-2.0
                            13 - GTK_RC_FILES=/etc/gtk/gtkrc:/home/rohit4/.gtkrc:/home/rohit4/.kde/share/config/gtkrc
                            14 - GS_LIB=/home/rohit4/.fonts
                            15 -    WINDOWID=67108882
                            16 -  OLDPWD=/home/rohit4
                            17 - SHELL_SESSION_ID=8dae78b982774000a6e699e54f332699
                            18 - KDE_FULL_SESSION=true
                            19 -            USER=root
                            20 - LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:
                            21 - SSH_AUTH_SOCK=/tmp/ssh-WdzHpVAra80W/agent.3132
                            22 - SESSION_MANAGER=local/debian:@/tmp/.ICE-unix/3296,unix/debian:/tmp/.ICE-unix/3296
                            23 -  MAIL=/var/mail/root
                            24 - DESKTOP_SESSION=default
                            25 - PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
                            26 - PWD=/home/rohit4/c/hacking/ch1
                            27 - KDE_SESSION_UID=1000
                            28 -           LANG=en_IN
                            29 - KONSOLE_DBUS_SESSION=/Sessions/1
                            30 -           HOME=/root
                            31 -       COLORFGBG=0;15
                            32 -              SHLVL=2
                            33 -    LANGUAGE=en_IN:en
                            34 - KDE_SESSION_VERSION=4
                            35 - XCURSOR_THEME=default
                            36 -         LOGNAME=root
                            37 - XDG_DATA_DIRS=/usr/share:/usr/share:/usr/local/share
                            38 - DBUS_SESSION_BUS_ADDRESS=unix:abstract=/tmp/dbus-cClxvVDGKQ,guid=a2b6f6edd34413d16bfb79d253bd6b1e
                            39 -         WINDOWPATH=7
                            40 -         PROFILEHOME=
                            41 -           DISPLAY=:0
                            42 - QT_PLUGIN_PATH=/home/rohit4/.kde/lib/kde4/plugins/:/usr/lib/kde4/plugins/
                            43 -          _=./convert
                            44 -               (null)
Segmentation fault

*/

/*
 * 
 * Segmentation fault from inside GDB
 * ==============================================
 * root@debian:/home/rohit4/c/hacking/ch1# gcc -g -o convert convert.c 
convert.c: In function ‘usage’:
convert.c:5:2: warning: incompatible implicit declaration of built-in function ‘exit’ [enabled by default]
root@debian:/home/rohit4/c/hacking/ch1# gdb -q ./convert
Reading symbols from /home/rohit4/c/hacking/ch1/convert...done.
(gdb) run "Hello World" 30000
Starting program: /home/rohit4/c/hacking/ch1/convert "Hello World" 30000
/home/rohit4/c/hacking/ch1/convert Hello World 30000 (null) 

Repeating 30000 times..
                             0 - /home/rohit4/c/hacking/ch1/convert
                             1 -          Hello World
                             2 -                30000
                             3 -               (null)
                             4 -   SSH_AGENT_PID=3213
                             5 -  KDE_MULTIHEAD=false
                             6 - DM_CONTROL=/var/run/xdmctl
                             7 -           TERM=xterm
                             8 -      SHELL=/bin/bash
                             9 - XDM_MANAGED=method=classic
                            10 - XDG_SESSION_COOKIE=5579209dc20a00bbc295e2c253521fbe-1404922732.471771-1675173998
                            11 - GTK2_RC_FILES=/etc/gtk-2.0/gtkrc:/home/rohit4/.gtkrc-2.0:/home/rohit4/.kde/share/config/gtkrc-2.0
                            12 - KONSOLE_DBUS_SERVICE=:1.43
                            13 - GS_LIB=/home/rohit4/.fonts
                            14 - GTK_RC_FILES=/etc/gtk/gtkrc:/home/rohit4/.gtkrc:/home/rohit4/.kde/share/config/gtkrc
                            15 -    WINDOWID=67108882
                            16 - SHELL_SESSION_ID=8dae78b982774000a6e699e54f332699
                            17 - KDE_FULL_SESSION=true
                            18 -            USER=root
                            19 - LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:
                            20 - SSH_AUTH_SOCK=/tmp/ssh-WdzHpVAra80W/agent.3132
                            21 - SESSION_MANAGER=local/debian:@/tmp/.ICE-unix/3296,unix/debian:/tmp/.ICE-unix/3296
                            22 -          COLUMNS=225
                            23 - PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
                            24 - DESKTOP_SESSION=default
                            25 -  MAIL=/var/mail/root
                            26 -       _=/usr/bin/gdb
                            27 - PWD=/home/rohit4/c/hacking/ch1
                            28 -           LANG=en_IN
                            29 - KDE_SESSION_UID=1000
                            30 -             LINES=54
                            31 - KONSOLE_DBUS_SESSION=/Sessions/1
                            32 -              SHLVL=2
                            33 -       COLORFGBG=0;15
                            34 -           HOME=/root
                            35 - KDE_SESSION_VERSION=4
                            36 -    LANGUAGE=en_IN:en
                            37 - XCURSOR_THEME=default
                            38 -         LOGNAME=root
                            39 - DBUS_SESSION_BUS_ADDRESS=unix:abstract=/tmp/dbus-cClxvVDGKQ,guid=a2b6f6edd34413d16bfb79d253bd6b1e
                            40 - XDG_DATA_DIRS=/usr/share:/usr/share:/usr/local/share
                            41 -         WINDOWPATH=7
                            42 -           DISPLAY=:0
                            43 -         PROFILEHOME=
                            44 - QT_PLUGIN_PATH=/home/rohit4/.kde/lib/kde4/plugins/:/usr/lib/kde4/plugins/
                            45 -               (null)

Program received signal SIGSEGV, Segmentation fault.
0xb7ea92f4 in vfprintf () from /lib/i386-linux-gnu/i686/cmov/libc.so.6
(gdb) 
* 
* (gdb) where
#0  0xb7ea92f4 in vfprintf () from /lib/i386-linux-gnu/i686/cmov/libc.so.6
#1  0xb7eaee90 in printf () from /lib/i386-linux-gnu/i686/cmov/libc.so.6
#2  0x08048553 in main (argc=3, argv=0xbffff404) at convert.c:22

gdb) break main
Breakpoint 1 at 0x80484ab: file convert.c, line 12.
(gdb) run "Hello World" 30000
The program being debugged has been started already.
Start it from the beginning? (y or n) y
Starting program: /home/rohit4/c/hacking/ch1/convert "Hello World" 30000

Breakpoint 1, main (argc=3, argv=0xbffff404) at convert.c:12
warning: Source file is more recent than executable.
12              printf("%s %s %s %s \n\n", argv[0], argv[1], argv[2], argv[3] );
(gdb) 

(gdb) x/47xw 0xbffff404
0xbffff404:     0xbffff583      0xbffff5a6      0xbffff5b2      0x00000000
0xbffff414:     0xbffff5b8      0xbffff5cb      0xbffff5df      0xbffff5fa
0xbffff424:     0xbffff605      0xbffff615      0xbffff630      0xbffff681
0xbffff434:     0xbffff6e3      0xbffff6fe      0xbffff719      0xbffff76e
0xbffff444:     0xbffff780      0xbffff7b2      0xbffff7c8      0xbffff7d2
0xbffff454:     0xbffffcf3      0xbffffd22      0xbffffd74      0xbffffd80
0xbffff464:     0xbffffdc2      0xbffffdda      0xbffffdee      0xbffffdfd
0xbffff474:     0xbffffe1c      0xbffffe27      0xbffffe3c      0xbffffe45
0xbffff484:     0xbffffe66      0xbffffe6e      0xbffffe7d      0xbffffe88
0xbffff494:     0xbffffe9e      0xbffffeb0      0xbffffec6      0xbffffed3
0xbffff4a4:     0xbfffff35      0xbfffff6a      0xbfffff77      0xbfffff82
0xbffff4b4:     0xbfffff8f      0x00000000      0x00000020
(gdb) 
* (gdb) x/s 0x00000000
0x0:     <Address 0x0 out of bounds>
(gdb) 



*/



