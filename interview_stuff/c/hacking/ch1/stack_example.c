void test_function(int a, int b, int c, int d) {
	int flag;
	char buffer[10];
	flag = 100;
	buffer[0] = 'A';
}

int main() {
	test_function(1, 2, 3, 4);
}

/*
(gdb) disass main
Dump of assembler code for function main:
   0x080483ef <+0>:     push   ebp
   0x080483f0 <+1>:     mov    ebp,esp
   0x080483f2 <+3>:     sub    esp,0x10
=> 0x080483f5 <+6>:     mov    DWORD PTR [esp+0xc],0x4
   0x080483fd <+14>:    mov    DWORD PTR [esp+0x8],0x3
   0x08048405 <+22>:    mov    DWORD PTR [esp+0x4],0x2
   0x0804840d <+30>:    mov    DWORD PTR [esp],0x1
   0x08048414 <+37>:    call   0x80483dc <test_function>
   0x08048419 <+42>:    leave  
   0x0804841a <+43>:    ret    
End of assembler dump.
(gdb) i r eip esp ebp
eip            0x80483f5        0x80483f5 <main+6>
esp            0xbffff358       0xbffff358
ebp            0xbffff368       0xbffff368
(gdb) cont
Continuing.

Breakpoint 2, test_function (a=1, b=2, c=3, d=4) at stack_example.c:4
4               flag = 31337;
(gdb) i r eip esp ebp
eip            0x80483e2        0x80483e2 <test_function+6>
esp            0xbffff340       0xbffff340
ebp            0xbffff350       0xbffff350   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> I THINK FROM 350 TO 358 , RETURN ADDRESS OF MAIN IS STORED
(gdb) disass test_function
Dump of assembler code for function test_function:
   0x080483dc <+0>:     push   ebp							>>>>>>>>>>>>>>>>>> MOVES THE LAST STACK POINTER .I.E STACK PTR FOR MAIN INTO THE STACK
   0x080483dd <+1>:     mov    ebp,esp						>>>>>>>>>>>>>>>>>> MOVES THE CURRENT STACK POINTER INTO THE EBP
   0x080483df <+3>:     sub    esp,0x10
=> 0x080483e2 <+6>:     mov    DWORD PTR [ebp-0x4],0x7a69
   0x080483e9 <+13>:    mov    BYTE PTR [ebp-0xe],0x41
   0x080483ed <+17>:    leave  
   0x080483ee <+18>:    ret    
End of assembler dump.

root@debian:/home/rohit4/c/hacking/ch1# gdb -q ./stack_examlple 
Reading symbols from /home/rohit4/c/hacking/ch1/stack_examlple...done.
(gdb) disass main
Dump of assembler code for function main:
   0x080483ef <+0>:     push   ebp
   0x080483f0 <+1>:     mov    ebp,esp
   0x080483f2 <+3>:     sub    esp,0x10
   0x080483f5 <+6>:     mov    DWORD PTR [esp+0xc],0x4
   0x080483fd <+14>:    mov    DWORD PTR [esp+0x8],0x3
   0x08048405 <+22>:    mov    DWORD PTR [esp+0x4],0x2
   0x0804840d <+30>:    mov    DWORD PTR [esp],0x1
   0x08048414 <+37>:    call   0x80483dc <test_function>
   0x08048419 <+42>:    leave  
   0x0804841a <+43>:    ret    
End of assembler dump.
(gdb) break 9
Breakpoint 1 at 0x80483f5: file stack_example.c, line 9.
(gdb) break test_function
Breakpoint 2 at 0x80483e2: file stack_example.c, line 4.
(gdb) disass main
Dump of assembler code for function main:
   0x080483ef <+0>:     push   ebp
   0x080483f0 <+1>:     mov    ebp,esp
   0x080483f2 <+3>:     sub    esp,0x10
   0x080483f5 <+6>:     mov    DWORD PTR [esp+0xc],0x4
   0x080483fd <+14>:    mov    DWORD PTR [esp+0x8],0x3
   0x08048405 <+22>:    mov    DWORD PTR [esp+0x4],0x2
   0x0804840d <+30>:    mov    DWORD PTR [esp],0x1
   0x08048414 <+37>:    call   0x80483dc <test_function>
   0x08048419 <+42>:    leave  
   0x0804841a <+43>:    ret    
End of assembler dump.
(gdb) run
Starting program: /home/rohit4/c/hacking/ch1/stack_examlple 

Breakpoint 1, main () at stack_example.c:9
warning: Source file is more recent than executable.
9               test_function(1, 2, 3, 4);
(gdb) disass main
Dump of assembler code for function main:
   0x080483ef <+0>:     push   ebp
   0x080483f0 <+1>:     mov    ebp,esp
   0x080483f2 <+3>:     sub    esp,0x10
=> 0x080483f5 <+6>:     mov    DWORD PTR [esp+0xc],0x4
   0x080483fd <+14>:    mov    DWORD PTR [esp+0x8],0x3
   0x08048405 <+22>:    mov    DWORD PTR [esp+0x4],0x2
   0x0804840d <+30>:    mov    DWORD PTR [esp],0x1
   0x08048414 <+37>:    call   0x80483dc <test_function>
   0x08048419 <+42>:    leave  
   0x0804841a <+43>:    ret    
End of assembler dump.

(root@debian:/home/rohit4/c/hacking/ch1# gdb -q ./stack_example
Reading symbols from /home/rohit4/c/hacking/ch1/stack_example...done.
(gdb) disass main
Dump of assembler code for function main:
   0x080483ef <+0>:     push   ebp
   0x080483f0 <+1>:     mov    ebp,esp
   0x080483f2 <+3>:     sub    esp,0x10
   0x080483f5 <+6>:     mov    DWORD PTR [esp+0xc],0x4
   0x080483fd <+14>:    mov    DWORD PTR [esp+0x8],0x3
   0x08048405 <+22>:    mov    DWORD PTR [esp+0x4],0x2
   0x0804840d <+30>:    mov    DWORD PTR [esp],0x1
   0x08048414 <+37>:    call   0x80483dc <test_function>
   0x08048419 <+42>:    leave  
   0x0804841a <+43>:    ret    
End of assembler dump.
(gdb) break 9
Breakpoint 1 at 0x80483f5: file stack_example.c, line 9.
(gdb) break test_function
Breakpoint 2 at 0x80483e2: file stack_example.c, line 4.
(gdb) list main
3               char buffer[10];
4               flag = 31337;
5               buffer[0] = 'A';
6       }
7
8       int main() {
9               test_function(1, 2, 3, 4);
10      }
11
12      /*
(gdb) disass main
Dump of assembler code for function main:
   0x080483ef <+0>:     push   ebp
   0x080483f0 <+1>:     mov    ebp,esp
   0x080483f2 <+3>:     sub    esp,0x10
   0x080483f5 <+6>:     mov    DWORD PTR [esp+0xc],0x4
   0x080483fd <+14>:    mov    DWORD PTR [esp+0x8],0x3
   0x08048405 <+22>:    mov    DWORD PTR [esp+0x4],0x2
   0x0804840d <+30>:    mov    DWORD PTR [esp],0x1
   0x08048414 <+37>:    call   0x80483dc <test_function>
   0x08048419 <+42>:    leave  
   0x0804841a <+43>:    ret    
End of assembler dump.
(gdb) run
Starting program: /home/rohit4/c/hacking/ch1/stack_example 

Breakpoint 1, main () at stack_example.c:9
9               test_function(1, 2, 3, 4);
(gdb) disass main
Dump of assembler code for function main:
   0x080483ef <+0>:     push   ebp
   0x080483f0 <+1>:     mov    ebp,esp
   0x080483f2 <+3>:     sub    esp,0x10
=> 0x080483f5 <+6>:     mov    DWORD PTR [esp+0xc],0x4
   0x080483fd <+14>:    mov    DWORD PTR [esp+0x8],0x3
   0x08048405 <+22>:    mov    DWORD PTR [esp+0x4],0x2
   0x0804840d <+30>:    mov    DWORD PTR [esp],0x1
   0x08048414 <+37>:    call   0x80483dc <test_function>
   0x08048419 <+42>:    leave  
   0x0804841a <+43>:    ret    
End of assembler dump.
(gdb) i r eip esp ebp
eip            0x80483f5        0x80483f5 <main+6>
esp            0xbffff358       0xbffff358
ebp            0xbffff368       0xbffff368
(gdb) cont
Continuing.

Breakpoint 2, test_function (a=1, b=2, c=3, d=4) at stack_example.c:4
4               flag = 31337;
(gdb) i r eip esp ebp
eip            0x80483e2        0x80483e2 <test_function+6>
esp            0xbffff340       0xbffff340
ebp            0xbffff350       0xbffff350
(gdb) x/w $esp+24
0xbffff358:     1
(gdb) x/w $esp+28
0xbffff35c:     2
(gdb) x/w $esp+32
0xbffff360:     3
(gdb) x/w $esp+36
0xbffff364:     4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NOTE : 58+C=64 , SO 4 IS STORED THERE
(gdb) 
(gdb) x/16xw $esp+4
0xbffff344:     0xbffff414      0xbffff41c      0xbffff368      0xbffff368 >>>>>>>>>>>>> this 68 is the EBP which got pushed into the stack
0xbffff354:     0x08048419      0x00000001      0x00000002      0x00000003 >>>>>>>>>>> (0x08048419 54 to 57), (0x00000001 58 to 5b) (5c to 5f) (60 to 63) >>> 54 to 57 is the return address
0xbffff364:     0x00000004      0xbffff3e8      0xb7e7be46      0x00000001 >>>>>>>>>>>>>>>> (64 to 67)
0xbffff374:     0xbffff414      0xbffff41c      0xb7fe0860      0xb7ff6821
(gdb) 

(gdb) x/xw 0xbffff350-0x04  >>>>>>>>>>>>>>>>> 350 is the value of the EBP , minus 4 will give the location where flag is stored
0xbffff34c:     0xbffff368
(gdb) 

*/
