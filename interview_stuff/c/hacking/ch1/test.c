#include <stdio.h>

void usage(char *program_name) {
	printf("Usage: %s <message> <# of times to repeat>\n", program_name);
	exit(1);
}

int main(int argc, char *argv[]) {
	
	unsigned int score = 2147483647;
	score = 4294967295;
	
	printf("Repeating %u times..\n", score);
	
	score++;
	
	printf("Repeating %u times..\n", score);
	

}

/*
 
 Output
root@debian:/home/rohit4/c/hacking/ch1# make test
cc     test.c   -o test
test.c: In function ‘usage’:
test.c:5:2: warning: incompatible implicit declaration of built-in function ‘exit’ [enabled by default]
test.c: In function ‘main’:
test.c:12:2: warning: this decimal constant is unsigned only in ISO C90 [enabled by default]
root@debian:/home/rohit4/c/hacking/ch1# ./test
Repeating -1 times..
root@debian:/home/rohit4/c/hacking/ch1# 


*/



