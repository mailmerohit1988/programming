#include <stdio.h>

int main(int argc, char* argv[]) {

	int areas[] = {10,12,14,16,18};
	char name[] = "Zed";
	char full_name[] = {
		'Z', 'E', 'D', 
		' ', 'A' ,' ', 
		'S', 'H', 'A', 'W'};
	printf("The size of an int: %ld\n", sizeof(int));
	printf("The size of areas is %ld\n", sizeof(areas));
 	printf("The size of a char: %ld\n", sizeof(char));
	printf("The size of name (char[]): %ld\n",sizeof(name));
	printf("The size of full_name (char[]): %ld\n",sizeof(full_name));
        printf("name=\"%s\" and full_name=\"%c\"\n",name, full_name[0]);
	printf("areas = %d",areas[4]);	
	return 0;
}	
