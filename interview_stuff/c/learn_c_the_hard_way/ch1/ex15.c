#include <stdio.h>

int main(int argc, char* argv[]) {

	int ages[] = {10, 20, 30, 40, 50};
	char* names[] = {
			"Rohit",
			"Pallavi",
			"Shivanshu",
			"Aashir",
			"xyz"
			};
	
	int* cur_age = ages;
	char** cur_names = names;

	printf("current age is %d\n", *(cur_age+1));
	printf("Current name is %s\n", *(cur_names+1));
	printf("Showing the difference is %x\n", (cur_age+5));	
	printf("Showing the difference is %d\n", (cur_names+5)	- (names));
	return 0;
}
