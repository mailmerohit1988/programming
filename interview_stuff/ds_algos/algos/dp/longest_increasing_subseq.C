/*
lets start
Consider 3, 4, -1, 0, 6, 2, 3

Create a temp array which stores the length of longest increasing subsequence
till that index

Initialize that array with 1 because each element in itself is a longest
increasing subsequence.
----------------------------------------
      3   4   -1    0   6   2   3


tmp   1   1    1    1   1   1   1    

idx   0   1    2    3   4   5   6
----------------------------------------

1) Aim: to find longest subsequence till i = 4, i.e till 4.
   start with idx=1, and keep incrementing it till length of array.
   initialize j to be starting from zero for each such iteration of idx.
  
  if a[j]<a[i], then tmp[i] = tmp[j]+1 only if tmp[i] < tmp[j]+1(see step 4 for
this);
once j reaches i on next iteration, increment i and reset j to begin from 0

AT EACH ELEMENT YOU REACH VIA I, THINK THAT BY HOW MUCH THE ELEMENT BEHING ME
WILL INCREASE THE INCREASING SUBSEQUENCE
----------------------------------------
      j   i
      3   4   -1    0   6   2   3


tmp   1   2    1    1   1   1   1    

idx   0   1    2    3   4   5   6
----------------------------------------

2)
----------------------------------------
      j        i
      3   4   -1    0   6   2   3


tmp   1   2    1    1   1   1   1    

idx   0   1    2    3   4   5   6
----------------------------------------

3)
----------------------------------------
      j             i
      3   4   -1    0   6   2   3


tmp   1   2    1    2   1   1   1    

idx   0   1    2    3   4   5   6
----------------------------------------

4) When j reaches -1, tmp[i]=3, when j reaches -1, since -1<6 but
   longestIncrSubSeq[i] will become 2, so we dont update tmp[i]
----------------------------------------
      j                 i 
      3   4   -1    0   6   2   3


tmp   1   2    1    2   3   1   1    

idx   0   1    2    3   4   5   6
----------------------------------------

5)
----------------------------------------
      j                     i 
      3   4   -1    0   6   2   3


tmp   1   2    1    2   3   3   1    

idx   0   1    2    3   4   5   6
----------------------------------------

6)
----------------------------------------
      j                         i 
      3   4   -1    0   6   2   3


tmp   1   2    1    2   3   3   4    

idx   0   1    2    3   4   5   6
----------------------------------------


Above approach is tabulation method of dynamic programming.
Time complexity of above is O(n2).
*/


#include <bits/stdc++.h>
using namespace std;

/*
       0  1  2  3  4  5
arr = {1, 2, 0, 5, 3, 6}

L[i] = 1 + max(L(j)) where j < i and arr[j] < arr[i]
     = 1 otherwise


            LIS(6)
  
        LIS(5)                LIS(4)          LIS(3)          LIS(2)    LIS(1)


LIS(4) LIS(3) LIS(2)       LIS(3) LIS(2)    LIS(2)  LIS(1)  LIS(1)


you can solve lis using DP using same recursion and lookup table i.e
memoization i.e top down like how we did for fibonacci series or we could use tabulation to
solve LIS i.e bottoms up. The below approach which uses bottoms up is not using
recursion.
*/

int lis_max = 1;

int lis_recursive(int arr[], int length) {


    if (length == 1)
        return 1;

    int lis_till_given_length = 1;
  
    for (int len = 1; len <= length-1; len++) {
        int res = lis_recursive(arr, len);
        if ((lis_till_given_length < res+1) && (arr[length-1] > arr[len-1])) {
            lis_till_given_length = 1 + res;
            if (lis_till_given_length > lis_max) {
                lis_max = lis_till_given_length;    
            }
        }

    }

    return lis_till_given_length;
}


int lis(int arr[], int length) {
    int i, j;

    int lis_till_given_idx[length];

    for (i = 0; i < length; i++) {
        lis_till_given_idx[i] = 1; 
    }

    for (i = 1; i < length; i++) {

        for (j = 0 ; j < i; j++) {

            if ((arr[i] > arr[j]) &&
                (lis_till_given_idx[j]+1 > lis_till_given_idx[i] )) {

                lis_till_given_idx[i] += 1;
            } 

      }
    }

    for (i = 0; i < length; i++) {
        cout << ": " << lis_till_given_idx[i]; 
    }

    cout << endl;

    int max = lis_till_given_idx[0];
    for (i = 1; i < length; i++ )
        if (max < lis_till_given_idx[i])
            max = lis_till_given_idx[i];
 
    return max; 

}

int main() {

  int arr[] = {3, 4, -1, 0, 6, 2, 3};

  cout << "Longest Increasing subsequence " << lis(arr, sizeof(arr)/sizeof(arr[0])) << endl;
  lis_recursive(arr, sizeof(arr)/sizeof(arr[0]));
  cout << "Longest Increasing subsequence using recursion: " << lis_max; 
  cout << endl;

  return 0;

}
