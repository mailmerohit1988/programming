/*
You are given n pairs of numbers. In every pair, the first number is always
smaller than the second number. A pair (c, d) can follow another pair (a, b) if
b < c. Chain of pairs can be formed in this fashion

{{5, 24}, {39, 60}, {15, 28}, {27, 40}, {50, 90} },


1) Sort given pairs in increasing order of first element.
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}

2) Now run a modified LIS process where we compare the second element of
   already finalized LIS with the first element of new LIS being constructed.

        i
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       1        1         1

        j         i 
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       1        1         1

        j                 i
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       2        1         1

                 j        i
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       2        1         1

        j                         i
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       2        2         1

                  j                i 
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       2        2         1

                           j      i 
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       2        2         1

       j                                   i 
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       2        2         2

                j                           i 
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       2        2         2

                         j                 i 
      {5,24}, {15,28}, {27,40}, {39,60}, {50,90}
        0        1       2        3         4 
        1        1       2        2         3

Answer is      {5,24}, {27,40}, {50,90}


Had you not sorted answer would have been. 

        i
      {5, 24}, {39, 60}, {15, 28}, {27, 40}, {50, 90}
          0        1       2        3         4 
          1        1       1        1         1

        j        i
      {5, 24}, {39, 60}, {15, 28}, {27, 40}, {50, 90}
          0        1       2        3         4 
          1        2       1        1         1

        j                   i
      {5, 24}, {39, 60}, {15, 28}, {27, 40}, {50, 90}
          0        1       2        3         4 
          1        2       1        1         1

        j                             i 
      {5, 24}, {39, 60}, {15, 28}, {27, 40}, {50, 90}
          0        1       2        3         4 
          1        2       1        2         1

        j                                       i 
      {5, 24}, {39, 60}, {15, 28}, {27, 40}, {50, 90}
          0        1       2        3         4 
          1        2       1        2         3

{5,24}, {27,40} and {50,90}

If the arrangement is like below, then without sorting answer would be 
        i
      (27,40) ,(15,28) ,(5,24), (39,60), (50,90)
          0        1       2        3         4 
          1        1       1        1         1

        j         i 
      (27,40) ,(15,28) ,(5,24), (39,60), (50,90)
          0        1       2        3         4 
          1        1       1        1         1

        j                 i
      (27,40) ,(15,28) ,(5,24), (39,60), (50,90)
          0        1       2        3         4 
          1        1       1        1         1

        j                           i
      (27,40) ,(15,28) ,(5,24), (39,60), (50,90)
          0        1       2        3         4 
          1        1       1        2         1

        j                                    i
      (27,40) ,(15,28) ,(5,24), (39,60), (50,90)
          0        1       2        3         4 
          1        1       1        2         2


{27,40} and {50,90}

If the arrangement is like below, then without sorting answer would be NULL
set. So if you dont sort then the first element of second index would be
smaller than the second element of first index. 
        i
      (61,90) (39,60) (27,40) (15,28) (5,24)
          0       1      2        3     4 
          1       1      1        1     1

        j         i
      (61,90) (39,60) (27,40) (15,28) (5,24)
          0       1      2        3     4 
          1       1      1        1     1

        j                i
      (61,90) (39,60) (27,40) (15,28) (5,24)
          0       1      2        3     4 
          1       1      1        1     1

        j                         i
      (61,90) (39,60) (27,40) (15,28) (5,24)
          0       1      2        3     4 
          1       1      1        1     1

        j                               i
      (61,90) (39,60) (27,40) (15,28) (5,24)
          0       1      2        3     4 
          1       1      1        1     1


For similicty just consider two elements

    (1,2) and (3,4) vs

    (3, 4) and (1,2)
*/
