#include <bits/stdc++.h>
#include <unordered_set>

using namespace std;

bool has_pair (int arr[], int n, int x)
{
        if(n<1 )
        {
                return (x ==0);
        }
        unordered_set<int > mys;

        for (int i =0;i<n;i++)
        {
                unordered_set<int>::const_iterator got = mys.find(x-arr[i]);
                if( got != mys.end())
                {
                        cout<<"The pair found and they are "<<arr[i]<<" and " <<x-arr[i]<<endl;
                        return true;
                }
                mys.insert(arr[i]);
        }
        return false;
}

int main()
{
        int arr[6] = {1, 4, 45, 6, 6, -8};
        cout<<has_pair(arr,6,12)<<endl;
        return 0;
}
