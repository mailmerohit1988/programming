#include <bits/stdc++.h>
using namespace std;


/*
Permutations and combination

6 people 4 chairs. how many ways

   nPk     =        6 * 5 * 4 *3 =    6!
                                   -------   
                                    (6-4)!

But above permutation would count  A B C D and A B D C as 2


If we dont bother about their sitting location

nCk =        nPk                                                        nPk                             because group of 4 people are counted k! times hence divide by k!
        ------------                                    =          -----------
          (number of ways of arranging k people)                        k!

4 persons in a room, count number of handshakes that will happen  = 4C2


cards game
------------


4 suits (diamond, heart, club, spade)
each card numbered 1 to 9

hence total 36 unique cards.

a hand is a collection of 9 cards

how many 9 card hands are possible

 - - - - - - - - - -> 9 places to fill


36C9


*/
 
// Returns value of Binomial Coefficient C(n, k)
float binomialCoeff(unsigned int n, unsigned int k)
{
        float res = 1;
 
        // C(n,k) = C(n, n-k)
        if (k > n-k) {
               //so that you have less to compute, hence replace only if k > n-k;
                k = n-k;
        }

        for (int i = 0; i < k; i++) {
                res = res * (n-i); 
                //res = res/(i+1);
                res = res/(k-i);
        }
 
        return res;
}
 
/*
The Catalan number cn, for n ≥ 0, is given by
cn =               1
                  ----- C(2n,n)
                  n + 1


C(n,k) =   n!
        --------     =    n * n-1 * ... * (n-k)!        n * n-1   * (n-k+1)    -------------------> This assumes that n > k.. n will always be greater than k 
         k! (n-k)!      --------------------------   = ----------------------
                                k! (n-k)!                       k!

k! =  k * (k-1) * ... 1


imagine n = 5; hence C(10,5) is as below if everything is float
res                     
---    then             10    2 * 9     (18/4) * 8              12 * 7          42 * 6
(k-i)                  ----, ------  ,  ----------  ,       ------------,    ------------
                        5       4         3                      2                  1

Final value = (42*6)/6 = 42


imagine n = 5; hence C(10,5) is as below if everything is int
res                     
---    then             10    2 * 9      4 * 8              10 * 7               35  * 6
(k-i)                  ----, ------  ,  ----------  ,       ------------,    ------------
                        5       4         3                      2                  1

Final value = (35*6)/6 = 35

imagine n = 5; hence C(10,5) is as below if everything is int
res                     
---    then             10    10 * 9     (45) * 8             120 * 7          210 * 6
(i+1)                  ----, ------  ,  ----------  ,       ------------,    ------------
                        1       2         3                      4                  5

Final value = (42*6)/6 = 42


THerefore if you replace float with unsigned long int, you would still get the same result


Since C(n, k) = C(n, n-k) 

       n!                              n!
   -------------  =                ----------
      k! (n-k)!                     (n-k)! k!
*/

/*A Binomial coefficient based function to find nth catalan number in O(n) time*/
float catalan(unsigned int n)
{
    float c = binomialCoeff(2*n, n);
 
    return c/(n+1);
}
 
int main()
{
    for (int i = 0; i < 10; i++)
        cout << "catalan: " << catalan(i) << " " << endl;
    return 0;
}
