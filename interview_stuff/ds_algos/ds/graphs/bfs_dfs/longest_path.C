// Link to understand : https://www.youtube.com/watch?v=n0LIuVZmAEU
// above link will make u understand as to why we need to find out longest path
// compute longest path to topologically ordered nodes before computing for other nodes ahead in line.

#include <bits/stdc++.h> 
#include <stack>
using namespace std;

struct AdjListNode
{
    int dest;
    int weight;
    struct AdjListNode* next;
};
 
struct AdjList
{
    struct AdjListNode *head;  // pointer to head node of list
};
 
struct Graph
{
    int V;
    struct AdjList* array;
};
 
struct AdjListNode* newAdjListNode(int dest, int weight)
{
    struct AdjListNode* newNode =
            (struct AdjListNode*) malloc(sizeof(struct AdjListNode));
    newNode->dest = dest;
    newNode->next = NULL;
    newNode->weight = weight;
    return newNode;
}
 
struct Graph* createGraph(int V)
{
    struct Graph* graph = (struct Graph*) malloc(sizeof(struct Graph));
    graph->V = V;
 
    graph->array = (struct AdjList*) malloc(V * sizeof(struct AdjList));
 
    int i;
    for (i = 0; i < V; ++i)
        graph->array[i].head = NULL;
 
    return graph;
}
 
void addEdge(struct Graph* graph, int src, int dest, int w)
{
    struct AdjListNode* newNode = newAdjListNode(dest, w);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;
}
 
void printGraph(struct Graph* graph)
{
    int v;
    for (v = 0; v < graph->V; ++v)
    {
        struct AdjListNode* pCrawl = graph->array[v].head;
        printf("\n Adjacency list of vertex %d\n head ", v);
        while (pCrawl)
        {
            printf("-> %d", pCrawl->dest);
            pCrawl = pCrawl->next;
        }
        printf("\n");
    }
}
 
void dfsUtil(bool* visited, int vertex, struct Graph* graph, stack<int>& s) {
        printf("%d, ", vertex);
        visited[vertex] = true;

        struct AdjListNode* adjacents = graph->array[vertex].head;
        while(adjacents) {
                if(!visited[adjacents->dest])
                        dfsUtil(visited, adjacents->dest, graph, s);
                adjacents = adjacents->next;       
        }
        
        s.push(vertex);

}

void dfs(struct Graph* graph, stack<int>& s) {
        printf("DFS traversal\n");

        bool visited[graph->V];
        for(int i = 0; i < graph->V; i++)
                visited[i] = false;

        for(int i = 0; i < graph->V; i++) {
                if(!visited[i])
                        dfsUtil(visited, i, graph, s);           
        }

        printf("\n");
}

void longestPath(stack<int>& s, struct Graph* graph, int sourceVertex) {
        printf("Longest distances from sourceVertex %d are \n", sourceVertex);
        
        int dist[graph->V];
        
        // mark initial distances
        for(int i = 0 ; i < graph->V; i++) {
                if(i == sourceVertex) {
                        dist[i] = 0;
                } else {
                        dist[i] = INT_MIN;
                }
        }

        while(!s.empty()) {
                int top = s.top();
                s.pop();
                if(dist[top] != INT_MIN) {
                        struct AdjListNode* adjacents = graph->array[top].head;
                        while(adjacents) {
                                if(dist[adjacents->dest] < dist[top] + adjacents->weight)
                                        dist[adjacents->dest] = dist[top] + adjacents->weight;
                                adjacents = adjacents->next;
                         }
                }
        }


        for(int i = 0; i < graph->V; i++)               
                printf("%d, ", dist[i]);
        printf("\n");
}

int main()
{
    // create the graph given in above fugure
    int V = 6;
    struct Graph* graph = createGraph(V);
    addEdge(graph, 0, 1, 5);
    addEdge(graph, 0, 2, 3);
    addEdge(graph, 1, 3, 6);
    addEdge(graph, 1, 2, 2);
    addEdge(graph, 2, 4, 4);
    addEdge(graph, 2, 5, 2);
    addEdge(graph, 2, 3, 7);
    addEdge(graph, 3, 5, 1);
    addEdge(graph, 3, 4, -1);
    addEdge(graph, 4, 5, -2);

    stack<int> s;
    printGraph(graph);
 
    dfs(graph, s);

    /*printf("TS order is\n");
    while(!s.empty()) {
        printf("%d, ", s.top());
        s.pop();
    }
    printf("\n");*/

    longestPath(s, graph, 1);
    return 0;
}
