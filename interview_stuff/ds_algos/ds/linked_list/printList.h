#ifndef PRINTLIST_H
#define PRINTLIST_H
#include "node.h"

void printList(struct node* head) {
        if (head == NULL) 
                return;

        while(head != NULL) {
                printf("%d, ", head->data);
                head = head->next;
        }
}
#endif
