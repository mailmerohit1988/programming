#ifndef ADD_OPERATIONS_H
#define ADD_OPERATIONS_H  
#include "node.h"
#include <stdlib.h>
#include <stdio.h>

void insertAfter(struct node* prev, int value) {
        if (prev == NULL) {
                //it can not be NULL
                printf("The given node can not be NULL");
                return;
        }

        struct node* new_node  = (struct node*)malloc(sizeof(struct node));
        new_node->data = value;
        
        new_node->next = prev->next;
        prev->next     = new_node;
}

void append(struct node** head, int value) {
        struct node* new_node  = (struct node*)malloc(sizeof(struct node));
        new_node->data = value;

        if (*head == NULL) {
                *head = new_node;
                return;
        }

        struct node* last = *head;

        while (last->next != NULL) 
                last = last->next;

        last->next = new_node;
        new_node->next = NULL;
}
 
void push(struct node **head, int value) {
        struct node* new_node  = (struct node*)malloc(sizeof(struct node));
        new_node->data = value;

        new_node->next = *head;
        *head = new_node;
}
#endif                
