#ifndef NODE_H
#define NODE_H
struct node {
        int data;       
        struct node* next;
};
#endif
