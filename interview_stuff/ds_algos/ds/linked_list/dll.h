#ifndef DLL
#define DLL
#include <stdio.h>
#include <stdlib.h>
#include <bits/stdc++.h>

struct dll_node {
        struct dll_node *prev;
        struct dll_node *next;
        int data;
};

void printDLL(struct dll_node *head) {
        printf("\nPrinting in forward direction: ");
        struct dll_node *last;
        while(head) {
                printf("%d ->", head->data);
                last = head;
                head=head->next;
        }
        printf("\n");
        printf("Printing in reverse direction: ");
        while(last) {
                printf("%d ->", last->data);
                last = last->prev;
        }
        printf("\n");
}

void addToBegin(struct dll_node **head, int data) {
        struct dll_node *node = (struct dll_node*)malloc(sizeof(struct dll_node));
        node->data = data;
        node->prev = NULL;
        node->next = (*head);
        if ((*head) != NULL) {
                (*head)->prev = node;
        }
        (*head) = node;
}

void addAfter(struct dll_node* after, int data) {
        if (after==NULL)
                return;
        struct dll_node *node = (struct dll_node*)malloc(sizeof(struct dll_node));
        node->data = data;
        node->next = after->next;
        node->prev = after;
        struct dll_node *after_next = after->next;
        after->next = node;
        if (after_next!=NULL) {
                after_next->prev = node;
        }
}

void addEnd(struct dll_node **head, int data) {
        struct dll_node *node = (struct dll_node*)malloc(sizeof(struct dll_node));
        if ((*head)==NULL) {
                node->data = data;
                node->next = NULL;
                node->prev = NULL;
                *head = node;
        }
        struct dll_node *end = *head;
        for(end=*head; end->next!=NULL; end=end->next);

        node->data = data;
        end->next = node;
        node->next = NULL;
        node->prev = end;
}

void delete_dll(struct dll_node **head, struct dll_node *node) {
        if(*head==NULL || node==NULL)
                return;
        struct dll_node *next = node->next;  
        struct dll_node *prev = node->prev; 

        if (prev) {
                prev->next = node->next;
        } else {
                *head = next;
        }
        if (next)
                next->prev = node->prev;
        free(node); 
}

#endif
