#ifndef util
#define util

#include "node.h"
#include <stdbool.h>
#include <bits/stdc++.h>
// recursive length calculation
int findlength(struct node* node) {

        if (node == NULL) {
                return 0;
        }
        return 1 + findlength(node->next);
}

// recursive
struct node* search(struct node* node, int value) {
        if (node == NULL) {
                return NULL;
        }
        if (node->data == value) {
                return node;
        } 
        return search(node->next ,value);
}

void swap(struct node** head, int d1, int d2){
        struct node *prev_d1, *next_d1, *prev_d2, *next_d2;
        struct node *node1, *node2;
        struct node *current = *head; 
        struct node *prev = NULL;
        while(current) {
                if (current->data == d1) {
                        prev_d1 = prev;
                        next_d1 = current->next;
                        node1 = current;
                } else if (current->data == d2) {
                        prev_d2 = prev;
                        next_d2 = current->next;
                        node2 = current;
                }
                prev = current; 
                current = current->next;
        }

        if (prev_d1 == NULL) {
                // first node
                *head = node2;
        } else {
                prev_d1->next = node2;
        }

        node2->next = next_d1;
        
        if (prev_d2 == NULL) {
                *head = node1;
        } else {
                prev_d2->next = node1;
        }

        node1->next = next_d2;

 
}

int nthfromLast(struct node* head, int pos) {

        struct node *slowPtr = head, *fastPtr = head;
        while(fastPtr && pos) {
                fastPtr = fastPtr->next;        
                pos--;
        }
        
        while (fastPtr != NULL) {
                fastPtr = fastPtr->next;
                slowPtr = slowPtr->next;
        }

        return slowPtr->data;
}

void reverse(struct node** head) {
		if (*head==NULL)
			return;
		struct node* node = *head;
		struct node *next=NULL, *prev=NULL;
		while(node) {
			next = node->next;
			node->next = prev;
			prev = node;
			node = next;
			if (!node) {
				*head=prev;
			}
		}	
		
}

// Each thread's *head is actually pointing to next variable of prvious node
void reverse_recursive(struct node **head) {

        struct node *first;
        struct node *rest;

        if (*head == NULL) {
                return;
        }

        first = *head;
        rest  = first->next; 

        if (rest == NULL) {
                return;
        }

        reverse_recursive(&rest);

        first->next->next = first;
        first->next = NULL;

        printf("data is: %d\n", rest->data);// pichewale node ke rest mein you
//are writing the address.
        *head = rest;
}

struct node* merge_two_sorted_list(struct node* a, struct node* b) {
        if (a==NULL) 
                return b;
        if (b==NULL)
                return a;
        struct node* result = NULL;

        if (a->data < b->data) {
                result = a;
                result->next = merge_two_sorted_list(a->next, b);
        } else {
                result = b;
                result->next = merge_two_sorted_list(a, b->next);
        }
        
        return result;
}

struct node* merge_two_sorted_list_descending(struct node* a, struct node* b) {
        if (a==NULL) 
                return b;
        if (b==NULL)
                return a;
        struct node* result = NULL;

        if (a->data > b->data) {
                result = a;
                result->next = merge_two_sorted_list_descending(a->next, b);
        } else {
                result = b;
                result->next = merge_two_sorted_list_descending(a, b->next);
        }
        
        return result;
}
bool recursivePalindrome(struct node **head, struct node* node) {
        if (*head == NULL) {
                return true;
        }
        if (node == NULL) {
                return true;
        }

        bool result = recursivePalindrome(head, node->next);
        if (result == false) {
                return false;
        }
        
        result = node->data == (*head)->data;
        *head = (*head)->next;
        return result;
}

void removeDuplicatesSorted(struct node **head) {
        struct node *node = *head;
        struct node *prev = NULL;
        while(node && node->next) {
                if (node->data == node->next->data) {
                        struct node* tobeFreed = node;
                        if (prev == NULL) {
                                *head = node->next; 
                                free(node);
                                node = *head;
                        } else { 
                                prev->next = node->next;  
                                free(node);
                                node = prev->next;
                        }             
                } else {
                        prev = node;
                        node = node->next;
                }
        }
}

void removeDuplicatesUnsorted(struct node **head) {
        struct node *node = *head;
        struct node *prev = NULL;
        std::map<int,int> m;
        while(node) {
                if (m[node->data]) {
                        prev->next = node->next;
                        free(node);
                        node = prev->next;
                        continue;
                }          
                m[node->data] =1;
                prev = node;
                node = node->next; 
        }

}

struct node* sortedIntersect(struct node *a, struct node *b) {
        struct node *intersect = NULL;

        if (a == NULL || b == NULL) {
                return NULL;
        }

        if (a->data < b->data) 
                return sortedIntersect(a->next, b);         

        if (a->data > b->data)
                return sortedIntersect(a, b->next);

        struct node *result = (struct node*)malloc(sizeof(struct node));        
        result->data = a->data;
        result->next = sortedIntersect(a->next, b->next);

        return result;
}

void altdelete(struct node *head) {
        struct node *node = head->next;
        struct node *prev = head;
        while(node) {
                prev->next = node->next;
                prev = node->next;
                free(node);
                node = prev->next;
        }
}
                
/*
  For every step taken by slow pointer, fast pointer takes 2 steps
  i.e 1 2
      2 4
      3 6
      4 8
      5 10
*/
void breakIntoTwo(struct node *head, struct node **middle) {
        struct node *slow = head;
        struct node *fast = head;
        struct node *prev = NULL;
        while(fast!=NULL && fast->next!=NULL) {
                prev = slow;
                slow = slow->next;
                fast = fast->next->next;
        }

        *middle = slow;
        if (prev!=NULL) {
                prev->next=NULL;
        }
}

void mergeSort(struct node **head) {
        if ((*head)->next == NULL) {
                // return if there is just one element
                return;
        }
                
        struct node *middle=NULL;
        breakIntoTwo(*head, &middle); 
        
        mergeSort(head);
        mergeSort(&middle);

        struct node *result = merge_two_sorted_list(*head, middle);        
        *head = result;
        return;
}

void mergeSortDes(struct node **head) {
        if ((*head)->next == NULL) {
                // return if there is just one element
                return;
        }
                
        struct node *middle=NULL;
        breakIntoTwo(*head, &middle); 
        
        mergeSortDes(head);
        mergeSortDes(&middle);

        struct node *result = merge_two_sorted_list_descending(*head, middle);        
        *head = result;
        return;
}
void reverse_k(struct node** head, int k) {
    int count = k;
		if (*head==NULL)
			return;

		struct node* node = *head;
		struct node *next=NULL, *prev=NULL;
		while(node && count) {
			next = node->next;
			node->next = prev;
			prev = node;
			node = next;
      count--;
		}	

    if (node != NULL) {
        reverse_k(&node, k);
    }

    (*head)->next = node;
		*head=prev;
    return;
}


void rotate(struct node **head, int k) {
        if (*head == NULL) 
                return;
        struct node *node = *head;
        struct node *prev = NULL;
        struct node *head_ref = *head;
        int count = k;
        while (node) {
                prev = node;
                node = node->next;
                count--;
                if (count == 0) {
                        // make current node as head
                        prev->next = NULL;
                        *head = node;

                }
                if (!node) {
                        // link the last node to head
                        prev->next = head_ref;
                }
        }
}

// retain m nodes, then delete n nodes. do this while you still have nodes in
// linked list.
void deleteMN(struct node *head, int M, int N){
        struct node *prev=NULL;
        struct node *node=head;
        struct node *next=NULL;
        while(node) {
                //skip m nodes
                for(int i=0; i<M && node!=NULL; i++){
                        prev = node; 
                        node = node->next;
                }

                if (node==NULL) {
                        return;
                }

                for(int j=0; j<N && node!=NULL; j++){
                        next = node->next;
                        prev->next = next;
                        free(node);
                        node = next;
                }
                        
        } 
                         
}

void altsplit(struct node *head, struct node **aref, struct node **bref) {
        struct node *node = head;
        if (head->next==NULL) {
                return;
        }
        *aref = head;
        *bref = head->next;
        while(node && node->next) {
                struct node *next = node->next;
                node->next = node->next->next;
                node = next;
        }
} 
#endif
