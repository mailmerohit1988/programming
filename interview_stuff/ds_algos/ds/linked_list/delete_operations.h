#ifndef DELETE_H
#define DELETE_H

void delete_first_occurence(struct node** head, int value) {
        if (*head == NULL) {
                return;
        }
        struct node *node = *head;
        struct node *next, *prev = NULL;
        while (node) {
                next = node->next;
                if (node->data == value) {
                        if (prev != NULL) { 
                                prev->next = next;
                        } else {
                                // this is the first node
                                *head = next;
                        }
                        free(node);
                        return;
                }
                prev = node;
                node = node->next;
        }
} 
                 
void delete_at_position(struct node **head, int pos) {
        int i = 0;
        struct node* node = *head; 
        struct node* prev = NULL;
        while (node) {
                if (i == pos) {
                        if (prev == NULL) {
                                // This is the first node
                                *head = node->next;               
                        } else {                               
                                prev->next = node->next;
                        }
                        free(node);
                        return;
                }
                prev = node;
                node = node->next;
                i++;
        }
}

#endif
