/*
        Imagine words are stored in a balanced BST
        
N words are stored, searching a given word would take O(MLogN) where M is the word length.

                                Jungle
                
                        hello                   kangaroo


                Ele             India


Using tries we can search in O(M) time.

                        root of a trie                                                  -> root is going to store pointers to trieNode. There can be max 26 trieNodes that a given trieNode can point to.

                a               b                       c
        
*/

#include <bits/stdc++.h>
using namespace std;

#define CHAR_TO_INDEX(c) ((int)c - (int)'a')

#define ARRAY_SIZE(a) sizeof(a)/sizeof(a[0])

struct trieNode {
        struct trieNode* children[25];        
        bool isLeaf;
};

struct trieNode* getNode() {
        struct trieNode *pNode = NULL;
 
        pNode = (struct trieNode *)malloc(sizeof(struct trieNode));
 
        if (pNode) {
                int i;
 
                pNode->isLeaf = false;
 
                for (i = 0; i < 26; i++)
                        pNode->children[i] = NULL;
        }

        return pNode;
}

void insert(struct trieNode* root, const char* key) {

        int length = strlen(key);

        for (int i = 0; i < length; i++) {
                if (!root->children[CHAR_TO_INDEX(key[i])]) {
                        // the given char is not present
                        struct trieNode* node = getNode();
                        root->children[CHAR_TO_INDEX(key[i])] = node;
                        root = node;
                } else {
                        root = root->children[CHAR_TO_INDEX(key[i])];
                }        

        }

        root->isLeaf = true;
}

int search(struct trieNode* root, const char* key) {

        int length = strlen(key);

        for (int i = 0; i < length; i++) {
                if (!root->children[CHAR_TO_INDEX(key[i])]) {
                        return 0;
                } else {
                        root = root->children[CHAR_TO_INDEX(key[i])];
                }        

        }
       
        return (root && root->isLeaf) ? 1 : 0; 
}

int isItFreeNode(struct trieNode* pNode){
    int i;
    for(i = 0; i < 26; i++)
    {
        if( pNode->children[i] )
            return 0;
    }
 
    return 1;
}

/*
        1) key is not present
        2) key is present as part of some longer word
        3) part of key is also part of another word
        4) key is present as unique key

        keep going down the word, once u reach last letter, i.e length==level, if at this point
        the letter is a leaf and there are no childrens of it then delete it and also
        mark that node as nonLeaf. But you would have
        to ask the caller to delete because only he can set that node i.e its children to NULL.
        hence return true for that delete case.
*/
bool deleteHelper(struct trieNode* root, const char* key, int level, int length) {
        
        if (root) {   // takes care of case 1
                if (level == length) {
                        // ie last letter in key
        
                        if (root->isLeaf) {

                                root->isLeaf = false; // takes care of case2
                                if (isItFreeNode(root)) {
                                        return true; // takes care of case 2 
                                }
                        }
                        return false;

                }

                if (deleteHelper(root->children[CHAR_TO_INDEX(key[level])], key, level+1, length)) {
                        free (root->children[CHAR_TO_INDEX(key[level])]);
                        root->children[CHAR_TO_INDEX(key[level])] = NULL;

                        if (isItFreeNode(root) && !root->isLeaf/*Imagine two words rohi and rohit and rohit is to be deleted*/) {
                                return true; // takes care of case 3 and 4 
                        }
                }
        }

        return false;
}

void deleteKey(struct trieNode* root, const char* key) {

        int length = strlen(key);

        if (length > 0) {
                deleteHelper(root, key, 0, length);
        }
        
        return;
}

int main()
{

        /*
                keys[8][8] means 8 rows and size of each row is 8

                        keys[0 0]  keys[0 1]  keys[0 2] ...   keys[0 7]
                        keys[1 0]  keys[1 1]  keys[1 2] ...   keys[1 7]
                        ...
                        ...
                        keys[7 0]  keys[7 1]  keys[7 2] ...   keys[7 7]

        */

        // Input keys (use only 'a' through 'z' and lower case)
        char keys[][8] = {"the", "a", "there", "answer", "any",
                             "by", "bye", "their", "rohit", "sharma", "pallavi", "rohik", "roho"};
 
        char output[][32] = {"Not present in trie", "Present in trie"};
 
 
        struct trieNode *root = getNode();
 
        // Construct trie
        int i;
        for (i = 0; i < ARRAY_SIZE(keys); i++)
                insert(root, keys[i]);
 
        // Search for different keys
        printf("%s --- %s\n", "the", output[search(root, "the")] );
        printf("%s --- %s\n", "these", output[search(root, "these")] );
        printf("%s --- %s\n", "their", output[search(root, "their")] );
        printf("%s --- %s\n", "thaw", output[search(root, "thaw")] );
        printf("%s --- %s\n", "rohi", output[search(root, "rohi")] );
        printf("%s --- %s\n", "rohit", output[search(root, "rohit")] );
        printf("%s --- %s\n", "roho", output[search(root, "roho")] );
        printf("%s --- %s\n", "rohik", output[search(root, "rohik")] );
 
        deleteKey(root, "rohit");
        printf("%s --- %s\n", "After deleting rohit", output[search(root, "rohit")] );
        printf("%s --- %s\n", "roho", output[search(root, "roho")] );
        printf("%s --- %s\n", "rohik", output[search(root, "rohik")] );

        return 0;
}
