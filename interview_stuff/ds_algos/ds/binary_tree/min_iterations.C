#include<iostream>
#include<list>
#include<cmath>
#include <stdlib.h>
using namespace std;
  
class NAryTree
{
    int N;    // No. of nodes in Tree
  
    list<int> *adj;
  
    void getMinIterUtil(int v, int minItr[]);
public:
    NAryTree(int N);   // Constructor
  
    void addChild(int v, int w);
  
    int getMinIter();
 
    static int compare(const void * a, const void * b);
};
  
NAryTree::NAryTree(int N)
{
    this->N = N;
    adj = new list<int>[N];
}
  
void NAryTree::addChild(int v, int w)
{
    adj[v].push_back(w); // Add w to v’s list.
}
  
void NAryTree::getMinIterUtil(int u, int minItr[])
{
    int noOfChild = adj[u].size();
    minItr[u] = noOfChild;

    if(noOfChild == 0)
        return;

    int *minItrTemp = new int[noOfChild];
    int k = 0, tmp = 0;

    list<int>::iterator i;
    for (i = adj[u].begin(); i!= adj[u].end(); ++i)
    {
        // iterate over all the children, get thier min iterations.
        getMinIterUtil(*i, minItr);
        minItrTemp[k++] = minItr[*i];
    }

    qsort(minItrTemp, minItr[u], sizeof (int), compare);

    for (k = 0; k < noOfChild; k++)
    {
        tmp = minItrTemp[k] + k + 1;
        minItr[u] = max(minItr[u], tmp);
    }
    delete[] minItrTemp;
}
  
int NAryTree::getMinIter()
{
    int *minItr = new int[N];
    int res = -1;
    for (int i = 0; i < N; i++)
        minItr[i] = 0;
  
    getMinIterUtil(0, minItr);
    res = minItr[0];
    delete[] minItr;
    return res;
}
 
int NAryTree::compare(const void * a, const void * b)
{
        return ( *(int*)b - *(int*)a );
}
  
int main()
{
    // TestCase 1
    NAryTree tree1(17);
    tree1.addChild(0, 1);
    tree1.addChild(0, 2);
    tree1.addChild(0, 3);
    tree1.addChild(0, 4);
    tree1.addChild(0, 5);
    tree1.addChild(0, 6);
  
    tree1.addChild(1, 7);
    tree1.addChild(1, 8);
    tree1.addChild(1, 9);
  
    tree1.addChild(4, 10);
    tree1.addChild(4, 11);
  
    tree1.addChild(6, 12);
  
    tree1.addChild(7, 13);
    tree1.addChild(7, 14);
    tree1.addChild(10, 15);
    tree1.addChild(11, 16);
  
    cout << "TestCase 1 - Minimum Iteration: "
         << tree1.getMinIter() << endl;

    return 0;
}
