// C++ program to remove nodes on root to leaf paths of length < K
#include <bits/stdc++.h>
#include<iostream>
using namespace std;
 
struct Node
{
    int data;
    Node *left, *right;
};
 
//New node of a tree
Node *newNode(int data)
{
    Node *node = new Node;
    node->data = data;
    node->left = node->right = NULL;
    return node;
}
 
Node *removeShortPathNodesUtil(Node *root, int level, int k)
{
        if(root==NULL)
                return NULL;

        root->left = removeShortPathNodesUtil(root->left,level+1,k);
        root->right = removeShortPathNodesUtil(root->right,level+1,k);

        if(!root->left && !root->right && (level<k)) {
                free(root);
                return NULL;
        }
        return root;
}
 
Node *removeShortPathNodes(Node *root, int k) {
        int x = 1;
        return removeShortPathNodesUtil(root, x, k);

}
 
//Method to print the tree in inorder fashion.
void printInorder(Node *root)
{
    if (root)
    {
        printInorder(root->left);
        cout << root->data << " ";
        printInorder(root->right);
    }
}
 
// Driver method.
int main()
{
    int k = 4;
    Node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->left->left->left = newNode(7);
    root->right->right = newNode(6);
    root->right->right->left = newNode(8);
    cout << "Inorder Traversal of Original tree" << endl;
    printInorder(root);
    cout << endl;
    cout << "Inorder Traversal of Modified tree" << endl;
    Node *res = removeShortPathNodes(root, k);
    printInorder(res);
    return 0;
}
