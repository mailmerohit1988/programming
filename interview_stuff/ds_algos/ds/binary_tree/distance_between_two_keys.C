/* Program to find LCA of n1 and n2 using one traversal of Binary Tree.
   It handles all cases even when n1 or n2 is not there in Binary Tree */
#include <iostream>
#include <bits/stdc++.h>
using namespace std;
 
// A Binary Tree Node
struct Node
{
    struct Node *left, *right;
    int key;
};
 
// Utility function to create a new tree Node
Node* newNode(int key)
{
    Node *temp = new Node;
    temp->key = key;
    temp->left = temp->right = NULL;
    return temp;
}
 

 
// This function returns LCA of n1 and n2 only if both n1 and n2 are present
// in tree, otherwise returns NULL;
Node* findLCA(Node *root, int n1, int n2, int* sawN1, int* sawN2, int* levelN1, int* levelN2, int currentLevel, int* levelLCA)
{
        if(!root)
                return NULL;

        if(n1==root->key){
                *sawN1 = true;
                *levelN1 = currentLevel;
                return root;
        }
        if(n2==root->key){
                *sawN2 = true;
                *levelN2 = currentLevel;
                return root;
        }
        struct Node* node1 = findLCA(root->left,n1,n2,sawN1,sawN2,levelN1,levelN2,currentLevel+1,levelLCA);      
        struct Node* node2 = findLCA(root->right,n1,n2,sawN1,sawN2,levelN1,levelN2,currentLevel+1,levelLCA);      
        
        if(node1&&node2) {
                *levelLCA = currentLevel;
                return root;
        }

        if(node1)
                return node1;
        if(node2)
                return node2;
        
        return NULL;
}
 
int find(struct Node* root, int key, int* levelNode, int level) {
        // returns true if key exists in a tree rooted at root
        if(!root)
                return 0;
        if(root->key == key){
                *levelNode = level;
                return 1;   
        }
        return find(root->left,key,levelNode,level+1) || find(root->right,key,levelNode,level+1);        
}
 
int wrapper(Node* root, int n1, int n2) {
        int sawN1=0, sawN2 =0;
        int levelN1=0, levelN2=0, levelLCA=0;
        struct Node* lca = findLCA(root, n1, n2, &sawN1, &sawN2, &levelN1, &levelN2, 1, &levelLCA);

        //if you saw one of them, this could be because
        //1. the other one does not exist
        //2. the other one is either left or right child of your lca . In this case you can return lca as the lca
        if( (sawN1==1 && sawN2 ==1)) {
                return levelN1+levelN2-(2*levelLCA);
        }

        if(sawN1 && find(lca,n2,&levelN2,1)){
                return levelN2-1; 

        } 
        if(sawN2 && find(lca,n1,&levelN1,1)){
                return levelN1-1;
        }

        return -1;
}

// Driver program to test above functions
int main()
{
    // Let us create binary tree given in the above example
    Node * root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    int dis =  wrapper(root, 4, 5);
    printf("Distance between 4,5 is %d\n", dis);

    dis =  wrapper(root, 4, 7);
    printf("Distance between 4,7 is %d\n", dis);

    dis =  wrapper(root, 4, 3);
    printf("Distance between 4,3 is %d\n", dis);

    dis =  wrapper(root, 4, 2);
    printf("Distance between 4,2 is %d\n", dis);

    dis =  wrapper(root, 3, 7);
    printf("Distance between 3,7 is %d\n", dis);

    dis =  wrapper(root, 4, 20);
    printf("Distance between 4,20 is %d\n", dis);

    dis =  wrapper(root, 40, 20);
    printf("Distance between 40,20 is %d\n", dis);

    return 0;
}
