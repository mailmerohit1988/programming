#include<bits/stdc++.h>
using namespace std;
 
bool canRepresentBST(int pre[], int n)
{
    stack<int> s;
    s.push(pre[0]);
 
    int root = INT_MIN;
 
    for (int i=1; i<n; i++)
    {
        if (pre[i] < root)
            return false;

        while (!s.empty() && s.top()<pre[i])
        {
            root = s.top();
            s.pop();
        }
 
        s.push(pre[i]);
    }
    return true;
}
 
// Driver program
int main()
{
    int pre1[] = {40, 30, 35, 80, 100};
    int n = sizeof(pre1)/sizeof(pre1[0]);
    canRepresentBST(pre1, n)? cout << "true\n":
                              cout << "false\n";
 
    // When 35 removes 30, Tune greater than 30 dekh liya hai i.e you moved on to right side of 30, so abhi <30 nahi dikhna chahiye
    int pre2[] = {40, 30, 35, 20, 80, 100};
    n = sizeof(pre2)/sizeof(pre2[0]);
    canRepresentBST(pre2, n)? cout << "true\n":
                              cout << "false\n";
 
    return 0;
}
