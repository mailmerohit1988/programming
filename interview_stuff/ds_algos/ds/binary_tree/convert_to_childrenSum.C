#include <bits/stdc++.h>

// convert to children sum tree, with the condition that you can only
// increment node values, decrement is not allowed.
struct b_node {
        int data;
        struct b_node* left;
        struct b_node* right;
};

struct b_node* newNode(int data) {
        struct b_node* node = (struct b_node*)malloc(sizeof(struct b_node));
        node->data = data;
        node->left = node->right = NULL;
        return node;
}

// cant apply children sum to leaf nodes, hence return as soon as you hit leaf.
// since children sum , hence we have to do post-order.
// every node should hold children sum property
void postorderCheck(struct b_node* root) {
        if(!root || (root->left == NULL && root->right == NULL))
                return;
        postorderCheck(root->left);
        postorderCheck(root->right); 
        
        int res1 = root->left ? root->left->data: 0;
        int res2 = root->right ? root->right->data : 0;
        int diff = (res1+res2) - root->data;
        if (diff>0) {
                root->data = root->data+diff;
        } else if (diff<0) {
                // what if node->left is NULL, this program needs correction, do it.
                root->left->data = root->left->data - diff;
                postorderCheck(root->left);
        } else {
                
        } 
}

void inorder(struct b_node* root) {
        if(!root)
                return;
        inorder(root->left);
        printf(" %d, ", root->data);
        inorder(root->right); 
}

int main() {
        struct b_node* root = newNode(50);
        root->left = newNode(7);
        root->right = newNode(2);
        root->left->left = newNode(3);
        root->left->right = newNode(5);
        root->right->left = newNode(1);
        root->right->right = newNode(30);
        inorder(root);
        printf("\n");
        postorderCheck(root);
        inorder(root);
        printf("\n");
        return 0;
}
